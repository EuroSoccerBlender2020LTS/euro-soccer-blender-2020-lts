import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
from .calc_dimencsions import *
from .group_phase import Group


class GroupMatchButton(Rect):

    def __init__(self, x, y):
        self.group_dimensions_dict = calc_groups_positions()
        self.x = x
        self.y = y
        self.cell =  self.group_dimensions_dict['cell_width']
        self.column_ACE = self.group_dimensions_dict['column_group_A']
        self.column_BDF = self.group_dimensions_dict['column_group_B']
        self.row_AB = self.group_dimensions_dict['row_group_A']
        self.row_CD = self.group_dimensions_dict['row_group_C']
        self.row_EF = self.group_dimensions_dict['row_group_E']
        self.row = -100
        self.column = -100
        self.group_target = 'NOT IN'
        self.group_position = -1
        self.width = 1.6 * self.cell
        self.height = self.cell
        self.pressed = False
        self.enabled = True
        self.activated = False
        self.locked = False
        self.set_color([0.3, 0.7, 0.7, 0.5])
        self.update(self.x, self.y)
        self.neighbours = []

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        if not self.is_on_target(x, y):
            self.x = -100
            self.y = -100
        else:
            self.x = self.column + 2.4 * self.cell
            self.y = self.row + (5 - self.group_position) * self.cell
        # bottom left, top, left, top,right, bottom right
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def is_on_target(self, x, y):
        # Groups A, C, E
        if self.column_ACE + 2.4 * self.cell < x < self.column_ACE + 4 * self.cell:
            self.column =self.column_ACE
            # Group A
            if self.row_AB < y < self.row_AB + 6 * self.cell:
                self.row = self.row_AB
                self.group_target = 'Group A'
                self.group_position = 5 - int((y - self.row_AB) / self.cell)
                return True
            # Group C
            if self.row_CD < y < self.row_CD + 6 * self.cell:
                self.row = self.row_CD
                self.group_target = 'Group C'
                self.group_position = 5 - int((y - self.row_CD) / self.cell)
                return True
            # Group E
            if self.row_EF < y < self.row_EF + 6 * self.cell:
                self.row = self.row_EF
                self.group_target = 'Group E'
                self.group_position = 5 - int((y - self.row_EF) / self.cell)
                return True
        # Groups B, D, F
        if self.column_BDF + 2.4 * self.cell < x < self.column_BDF + 4 * self.cell:
            self.column = self.column_BDF
            # Group A
            if self.row_AB < y < self.row_AB + 6 * self.cell:
                self.row = self.row_AB
                self.group_target = 'Group B'
                self.group_position = 5 - int((y - self.row_AB) / self.cell)
                return True
            # Group C
            if self.row_CD < y < self.row_CD + 6 * self.cell:
                self.row = self.row_CD
                self.group_target = 'Group D'
                self.group_position = 5 - int((y - self.row_CD) / self.cell)
                return True
            # Group E
            if self.row_EF < y < self.row_EF + 6 * self.cell:
                self.row = self.row_EF
                self.group_target = 'Group F'
                self.group_position = 5 - int((y - self.row_EF) / self.cell)
                return True
        self.group_position = -1
        self.group_target = 'NOT IN'
        return False

    def set_pressed_function(self, some_function):
        self.pressed_function = some_function

    def left_mouse_down(self, x, y):
        if not Group.popup_activated:
            if self.is_on_target(x, y):
                self.pressed = True
                self.color = self.pressed_color
                try:
                    self.pressed_function(self)
                except:
                    print("No Function for button")
                return True
        else:
            if self.is_in_rect(x, y):
                self.pressed = True
                self.color = self.pressed_color
                try:
                    self.pressed_function(self)
                except:
                    print("No Function for button")
                return True
        return False

    def mouse_move(self, x, y):
        if not Group.popup_activated:
            if self.is_on_target(x, y):
                self.color = self.hover_on_color
                self.update(x, y)
            else:
                self.color = self.default_color
                self.update(-100, -100)
        else:
            if self.is_in_rect(x, y):
                self.color = self.hover_on_color
            else:
                self.color = self.default_color

    def left_mouse_up(self, x, y):
        self.pressed = False
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color
