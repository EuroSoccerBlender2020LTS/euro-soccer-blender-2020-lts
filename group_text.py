import bgl
import blf



class GroupText:

    def __init__(self, group):
        # games_order defines order of games in group - list of 6 lists of 2 teams
        self.group = group
        self.text = ''
        self.score_font_size = self.group.score_font_size
        self.stats_font_size = self.group.stats_font_size
        self.stats_font_size2 = self.group.stats_font_size2
        self.stats_font_dimension = self.group.stats_font_dimension
        self.stats_font_dimension2 = self.group.stats_font_dimension2
        self.set_color((0.388, 0.388, 0.388, 1.0))
        self.enabled = True
        self.set_shadow_color((0.0, 0.0, 0.0, 0.2))
        self.activated = False
        self.score_text = '0 : 0'
        self.score_font_size = self.group.score_font_size
        self.score_names_font_size, self.score_names_dimention = self.group.score_names_font_size, self.group.score_names_dimention
        self.matches = [[i, [0, 0], m] for i, m in enumerate(self.group.match_scores)]
        self.score_positions, self.stats_positions, self.score_names1, self.score_names2 = self.calc_positions()
        self.score_short_names_dimentions_x = self.group.score_short_names_dimentions_x

    def set_color(self, color):
        self.color = color

    def set_shadow_color(self, color):
        self.shadow_color = color

    def set_text(self, text):
        self.text = text

    def calc_positions(self):
        cell = self.group.cell
        score_x = self.group.group_rect.x
        score_y = self.group.group_rect.y
        score_dimension = self.group.score_font_dimensions
        lst = []
        lst2 = []
        lst3 = []
        lst4 = []
        for i in range(6):
            lst.append((score_x + 2.4 * cell + (1.6 * cell - score_dimension[0]) / 2,
                       score_y + (cell - score_dimension[1]) / 2 + (5 - i) * cell))
            lst3.append(((score_x + (1.4 * cell - self.score_names_dimention[0]) / 2),
                        score_y + (cell - self.score_names_dimention[1]) / 2 + (5 - i) * cell))
            lst4.append(((score_x + 4.8 * cell + (1.6 * cell - self.score_names_dimention[0]) / 2),
            score_y + (cell - self.score_names_dimention[1]) / 2 + (5 - i) * cell))
        stat_dimension = self.stats_font_dimension
        stat_dimension2 = self.stats_font_dimension2
        for i in range(4):
            x = self.group.drag_rect_limit_high_x + (cell - stat_dimension2[0] * 0.25) / 2
            y = self.group.drag_rect_limit_low_y + (1.5 * cell - stat_dimension2[1]) / 2
            x1 = self.group.drag_rect_limit_high_x + (cell - stat_dimension[0]) / 2
            y1 = self.group.drag_rect_limit_low_y + (1.5 * cell - stat_dimension[1]) / 2
            lst2.append(((x, y + cell * (3 - i) * 1.5), (x + cell, y + cell * (3 - i) * 1.5), (x1 + 2 * cell , y1 + cell * (3 - i) * 1.5)))
        return lst, lst2, lst3, lst4

    def get_symbol_space_or_star(self, number):
        if number == 1:
            return '*'
        else:
            return ' '

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, self.shadow_color[0], self.shadow_color[1], self.shadow_color[2], self.shadow_color[3])
        blf.shadow_offset(font_id, int(self.stats_font_size2 / 10), -int(self.stats_font_size2/10))
        blf.color(font_id, self.color[0], self.color[1], self.color[2], self.color[3])
        matches = self.group.match_scores
        for i in range(6):
            blf.size(font_id, self.score_font_size, 72)
            blf.position(font_id, int(self.score_positions[i][0]), int(self.score_positions[i][1]), 0)
            match_text = str(matches[i][2][0]) + self.get_symbol_space_or_star(
                matches[i][3][0]) + ':' + self.get_symbol_space_or_star(matches[i][3][1]) + str(matches[i][2][1])
            blf.draw(font_id, match_text)
            blf.size(font_id, self.score_names_font_size, 72)
            txt1 = self.group.team_names_short[self.group.match_scores[i][1][0]]
            txt2 = self.group.team_names_short[self.group.match_scores[i][1][1]]
            group_x = self.group.x
            group_y = self.group.y
            cell = self.group.cell
            name1_pos =((group_x + (1.4 * cell - self.score_short_names_dimentions_x[txt1]) / 2),
                         group_y + (cell - self.score_names_dimention[1]) / 2 + (5 - i) * cell)
            name2_pos = ((group_x + 5 * cell + (1.4 * cell - self.score_short_names_dimentions_x[txt2]) / 2),
                         group_y + (cell - self.score_names_dimention[1]) / 2 + (5 - i) * cell)
            blf.position(font_id, int(name1_pos[0]), int(name1_pos[1]), 0)
            blf.draw(font_id, txt1)
            blf.position(font_id, int(name2_pos[0]), int(name2_pos[1]), 0)
            blf.draw(font_id, txt2)
        blf.size(font_id, int(self.stats_font_size), 72)
        for i, team in enumerate(self.group.get_rated_teams()):
            blf.size(font_id, int(self.stats_font_size2), 72)
            if team.goal_score > 9:
                dx = (self.stats_font_dimension2[0] - self.stats_font_dimension[0]) * 0.4
            else:
                dx = 0
            blf.position(font_id, int(self.stats_positions[i][0][0] - dx), int(self.stats_positions[i][0][1]), 0)
            blf.draw(font_id, str(team.goal_score))
            if team.goal_diff > 9 or team.goal_diff < 0:
                dx = (self.stats_font_dimension2[0] - self.stats_font_dimension[0]) * 0.4
            else:
                dx = 0
            blf.position(font_id, int(self.stats_positions[i][1][0] - dx), int(self.stats_positions[i][1][1]), 0)
            blf.draw(font_id, str(team.goal_diff))
            blf.size(font_id, int(self.stats_font_size), 72)
            blf.position(font_id, int(self.stats_positions[i][2][0]), int(self.stats_positions[i][2][1]), 0)
            blf.draw(font_id, str(team.points))
        cell = self.group.cell
        x = self.group.drag_rect_limit_high_x + (cell - self.stats_font_dimension2[0]) / 2
        y = self.stats_positions[0][2][1] + cell * 1.5
        blf.size(font_id, int(self.stats_font_size), 72)
        blf.position(font_id, int(self.stats_positions[0][2][0]), int(y), 0)
        blf.draw(font_id, 'P')
        blf.size(font_id, int(self.stats_font_size2), 72)
        blf.position(font_id, int(x), int(y), 0)
        blf.draw(font_id, 'GF')
        blf.position(font_id, int(x + cell), int(y), 0)
        blf.draw(font_id, 'GD')
        blf.size(font_id, int(self.stats_font_size * 1.5) , 72)
        blf.position(font_id, self.score_positions[0][0] - cell, self.score_positions[0][1] + 1.2 * cell, 0)
        blf.draw(font_id, self.group.name)
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def handle_event(self, event):
        return False