import bpy
from .drag_rect import *
from .group_rect import *
from .group_text import *
import collections
import random
import math
from .group_generate_button import GroupGenerateButton
from .button_rect import ButtonRect
from .calc_dimencsions import *
from .group_points_probs import group_points_probs



class Team:

    def __init__(self, name, short_name, group):
        self.groups_dict = calc_groups_positions()
        self.name = name
        self.short_name = short_name
        self.level = self.groups_dict['level_stats'][self.name][0]
        self.attack = self.groups_dict['level_stats'][self.name][1]
        self.defence = self.groups_dict['level_stats'][self.name][2]
        self.group = group
        self.goal_score = 0
        self.goal_diff = 0
        self.points = 0
        self.wins = 0
        self.place = group.team_names.index(self.name)
        d = self.group.group_pos_dict
        self.EQ_ranking = d['EQ_ranking'][self.name]
        self.drag_rect_x_spacing = x_spacing = 0.15
        self.drag_rect_y_spacing = y_spacing = 0.15
        self.drag_rect = DragRect(self.group.drag_rect_limit_low_x + x_spacing * d['cell_width'],
                                  self.group.drag_rect_limit_low_y + (y_spacing + 4.5 - 1.5 * self.place) * d[
                                      'cell_width'],
                                  (d['column_team_width_cells_count'] - 2 * x_spacing) * d['cell_width'],
                                  (1.5 - 2 * y_spacing) * d['cell_width'], self)
        self.drag_rect.text = self.name
        text_font_size, text_dimension = self.group.team_font_size, self.group.team_font_dimension
        coef = 0.4
        self.drag_rect.text_y = ((1.5 - 2 * y_spacing) * d['cell_width'] - (old_calc_text_dimension('TPO', text_font_size)[1] - old_calc_text_dimension('qpg', text_font_size)[1] +
                                                          old_calc_text_dimension('amo', text_font_size)[1])) * coef
        self.drag_rect.text_x = self.drag_rect.text_y + d['cell_width']
        filepath = Group.dirpath + '/flags/' + self.name.replace(' ', '_') + '.png'
        self.drag_rect.load_img(filepath)

    def __repr__(self):
        return self.name

    def get_team_name_index(self):
        return self.group.team_names.index(self.name)

    def get_place_prob(self, place):
        d = group_rankings_probabilities[self.group.name]
        return sum([val[1] for val in [an_val for an_val in d if an_val[0][place] == self.name]])


class Group:
    all_groups = {}
    popup_activated = False
    group_pos_dict = None
    dirpath = ''

    def __init__(self, name='Some Group'):
        self.name = name
        Group.group_pos_dict = calc_groups_positions()
        Group.all_groups[self.name] = self
        self.group_points_probs = group_points_probs[self.name]
        self.cell = self.group_pos_dict['cell_width']
        self.score_font_size, self.score_font_dimensions = self.group_pos_dict['score_font_size'], self.group_pos_dict[
            'score_font_dimensions']
        self.score_names_dimention = self.group_pos_dict['score_names_dimention']
        self.score_names_font_size = self.group_pos_dict['score_names_font_size']
        self.score_short_names_dimentions_x = self.group_pos_dict['score_short_names_dimentions_x']
        self.team_font_size, self.team_font_dimension = self.group_pos_dict['team_font_size'], self.group_pos_dict[
            'team_font_dimension']
        self.stats_font_size, self.stats_font_dimension = self.group_pos_dict['stats_font_size'], self.group_pos_dict[
            'stats_font_dimension']
        self.stats_font_size2, self.stats_font_dimension2 = self.group_pos_dict['stats_font_size2'], \
                                                            self.group_pos_dict['stats_font_dimension2']
        self.team_names = ('Team', 'Team', 'Team', 'Team')
        self.team_names_short = ('TM', 'TM', 'TM', 'TM')
        self.match_scores = ((1, (0, 0), [0, 0]), (2, (0, 0), [0, 0]), (13, (0, 0), [0, 0]),
                             (14, (0, 0), [0, 0]), (25, (0, 0), [0, 0]), (26, (0, 0), [0, 0]))
        self.width = self.group_pos_dict['column_group_width']
        self.height = self.group_pos_dict['row_group_height']
        self.init_teams()
        self.drag_rect_limit_low_x = self.x + 6.4 * self.cell
        self.drag_rect_limit_low_y = self.y
        self.drag_rect_limit_high_x = self.x + (6.4 + self.group_pos_dict['column_team_width_cells_count']) * self.cell
        self.drag_rect_limit_high_y = self.y + 6 * self.cell
        self.group_rect = GroupRect(self, self.x, self.y, self.width, self.height)
        self.teams = [Team(name, self.team_names_short[self.team_names.index(name)], self) for name in self.team_names]
        self.calc_neighbours()
        self.places = dict(zip(self.team_names, [0, 1, 2, 3]))
        self.group_text = GroupText(self)
        self.group_generate_button = GroupGenerateButton(self)
        self.group_generate_button.set_pressed_function(self.generate_group_matches)
        self.zero_fill_penalties()
        self.calc_table()
        self.real_sort()


    @classmethod
    def get_all_ranked_thirds(cls):
        ranking_thirds_list = [(Group.all_groups[group_name].get_third().points,
                                Group.all_groups[group_name].get_third().goal_diff,
                                Group.all_groups[group_name].get_third().goal_score,
                                Group.all_groups[group_name].get_third().wins,
                                500 - Group.all_groups[group_name].get_third().EQ_ranking,
                                Group.all_groups[group_name].get_third().name, group_name) for
                               group_name in Group.all_groups]
        ranking_thirds_list.sort(reverse=True)
        return [(r_th[-2], r_th[-1], Group.all_groups[r_th[-1]].get_third()) for r_th in ranking_thirds_list]

    @classmethod
    def get_4_thirds(cls):
        ranking_thirds_list = [(Group.all_groups[group_name].get_third().points,
                                Group.all_groups[group_name].get_third().goal_diff,
                                Group.all_groups[group_name].get_third().goal_score,
                                Group.all_groups[group_name].get_third().wins,
                                500 - Group.all_groups[group_name].get_third().EQ_ranking,
                                Group.all_groups[group_name].get_third().name, group_name) for
                               group_name in Group.all_groups]
        ranking_thirds_list.sort(reverse=True)
        grps = sorted([r_list[6][-1] for r_list in ranking_thirds_list[:4]])
        if grps == ['A', 'B', 'C', 'D']:
            return ((39, Group.all_groups['Group A'].get_third()), (40, Group.all_groups['Group D'].get_third()),
                    (43, Group.all_groups['Group B'].get_third()),(41, Group.all_groups['Group C'].get_third()))
        elif grps == ['A', 'B', 'C', 'E']:
            return ((39, Group.all_groups['Group A'].get_third()), (40, Group.all_groups['Group E'].get_third()),
                    (43, Group.all_groups['Group B'].get_third()),(41, Group.all_groups['Group C'].get_third()))
        elif grps == ['A', 'B', 'C', 'F']:
            return ((39, Group.all_groups['Group A'].get_third()), (40, Group.all_groups['Group F'].get_third()),
                    (43, Group.all_groups['Group B'].get_third()),(41, Group.all_groups['Group C'].get_third()))
        elif grps == ['A', 'B', 'D', 'E']:
            return ((39, Group.all_groups['Group D'].get_third()), (40, Group.all_groups['Group E'].get_third()),
                    (43, Group.all_groups['Group A'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['A', 'B', 'D', 'F']:
            return ((39, Group.all_groups['Group D'].get_third()), (40, Group.all_groups['Group F'].get_third()),
                    (43, Group.all_groups['Group A'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['A', 'B', 'E', 'F']:
            return ((39, Group.all_groups['Group E'].get_third()), (40, Group.all_groups['Group F'].get_third()),
                    (43, Group.all_groups['Group A'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['A', 'C', 'D', 'E']:
            return ((39, Group.all_groups['Group E'].get_third()), (40, Group.all_groups['Group D'].get_third()),
                    (43, Group.all_groups['Group E'].get_third()),(41, Group.all_groups['Group A'].get_third()))
        elif grps == ['A', 'C', 'D', 'F']:
            return ((39, Group.all_groups['Group F'].get_third()), (40, Group.all_groups['Group D'].get_third()),
                    (43, Group.all_groups['Group C'].get_third()),(41, Group.all_groups['Group A'].get_third()))
        elif grps == ['A', 'C', 'E', 'F']:
            return ((39, Group.all_groups['Group E'].get_third()), (40, Group.all_groups['Group F'].get_third()),
                    (43, Group.all_groups['Group C'].get_third()),(41, Group.all_groups['Group A'].get_third()))
        elif grps == ['A', 'D', 'E', 'F']:
            return ((39, Group.all_groups['Group E'].get_third()), (40, Group.all_groups['Group F'].get_third()),
                    (43, Group.all_groups['Group D'].get_third()),(41, Group.all_groups['Group A'].get_third()))
        elif grps == ['B', 'C', 'D', 'E']:
            return ((39, Group.all_groups['Group E'].get_third()), (40, Group.all_groups['Group D'].get_third()),
                    (43, Group.all_groups['Group B'].get_third()),(41, Group.all_groups['Group C'].get_third()))
        elif grps == ['B', 'C', 'D', 'F']:
            return ((39, Group.all_groups['Group F'].get_third()), (40, Group.all_groups['Group D'].get_third()),
                    (43, Group.all_groups['Group C'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['B', 'C', 'E', 'F']:
            return ((39, Group.all_groups['Group F'].get_third()), (40, Group.all_groups['Group E'].get_third()),
                    (43, Group.all_groups['Group C'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['B', 'D', 'E', 'F']:
            return ((39, Group.all_groups['Group F'].get_third()), (40, Group.all_groups['Group E'].get_third()),
                    (43, Group.all_groups['Group D'].get_third()),(41, Group.all_groups['Group B'].get_third()))
        elif grps == ['C', 'D', 'E', 'F']:
            return ((39, Group.all_groups['Group F'].get_third()), (40, Group.all_groups['Group E'].get_third()),
                    (43, Group.all_groups['Group D'].get_third()),(41, Group.all_groups['Group C'].get_third()))
        else:
            print(grps)

    def get_winner(self):
        for team_name in self.places:
            if self.places[team_name] == 0:
                return self.get_team(team_name)

    def get_runner_up(self):
        for team_name in self.places:
            if self.places[team_name] == 1:
                return self.get_team(team_name)

    def get_third(self):
        for team_name in self.places:
            if self.places[team_name] == 2:
                return self.get_team(team_name)

    @classmethod
    def get_group_name_from_team_name(cls, team_name):
        for group_name in Group.all_groups:
            if team_name in Group.all_groups[group_name].team_names:
                return group_name

    @classmethod
    def get_group_from_team_name(cls, team_name):
        for group_name in Group.all_groups:
            if team_name in Group.all_groups[group_name].team_names:
                return Group.all_groups[group_name]

    def get_rating(self):
        lst = []
        for i in range(4):
            for k, v in self.places.items():
                if i == v:
                    lst.append(k)
        return lst

    def get_team(self, team_name):
        for team in self.teams:
            if team_name == team.name:
                return team
        return None

    def get_rated_teams(self):
        lst = []
        for team_name in self.get_rating():
            lst.append(self.get_team(team_name))
        return lst

    def zero_fill_penalties(self):
        for match in self.match_scores:
            match[3][0] = 0
            match[3][1] = 0

    def recalc_places_of_drag_teams(self, drag_rect, place):
        old_rating = self.get_rating()
        rating = old_rating[:]
        rating.remove(drag_rect.team.name)
        rating.insert(place, drag_rect.team.name)
        for i, r in enumerate(rating):
            self.places[r] = i
        for team in self.teams:
            if team.place != self.places[team.name] and team.name != drag_rect.team.name:
                team.drag_rect.set_place_position(self.places[team.name])
        if not drag_rect.is_drag:
            while not self.sort_of_sort(rating):
                self.generate_group_matches_without_sort()
            self.zero_fill_penalties()
            self.calc_table()
            self.real_sort()

    def calc_neighbours(self):
        for team in self.teams[:]:
            team.drag_rect.neighbours = [t.drag_rect for t in self.teams[:]]
            team.drag_rect.neighbours.remove(team.drag_rect)

    def init_teams(self):
        d = self.group_pos_dict
        if self.name == 'Group A':
            self.team_names = ('Turkey', 'Italy', 'Wales', 'Switzerland')
            self.team_names_short = ('TUR', 'ITA', 'WLS', 'CHE')
            self.match_scores = ((1, (0, 1), [0, 0], [0, 0]), (2, (2, 3), [0, 0], [0, 0]), (13, (0, 2), [0, 0], [0, 0]),
                                 (14, (1, 3), [0, 0], [0, 0]), (25, (3, 0), [0, 0], [0, 0]), (26, (1, 2), [0, 0], [0, 0]))
            self.x = d['column_group_A']
            self.y = d['row_group_A']
        elif self.name == 'Group B':
            self.team_names = ('Denmark', 'Finland', 'Belgium', 'Russia')
            self.team_names_short = ('DNK', 'FIN', 'BEL', 'RUS')
            self.match_scores = ((3, (0, 1), [0, 0], [0, 0]), (3, (2, 3), [0, 0], [0, 0]), (15, (1, 3), [0, 0], [0, 0]),
                                 (16, (0, 2), [0, 0], [0, 0]), (27, (3, 0), [0, 0], [0, 0]), (28, (1, 2), [0, 0], [0, 0]))
            self.x = d['column_group_B']
            self.y = d['row_group_A']
        elif self.name == 'Group C':
            self.team_names = ('Netherlands', 'Ukraine', 'Austria', 'North Macedonia')
            self.team_names_short = ('NLD', 'UKR', 'AUT', 'NMK')
            self.match_scores = ((5, (0, 1), [0, 0], [0, 0]), (6, (2, 3), [0, 0], [0, 0]), (17, (0, 2), [0, 0], [0, 0]),
                                 (18, (1, 3), [0, 0], [0, 0]), (29, (3, 0), [0, 0], [0, 0]), (30, (1, 2), [0, 0], [0, 0]))
            self.x = d['column_group_A']
            self.y = d['row_group_C']
        elif self.name == 'Group D':
            self.team_names = ('England', 'Croatia', 'Scotland', 'Czech Republic')
            self.team_names_short = ('ENG', 'HRV', 'SCT', 'CZE')
            self.match_scores = ((7, (0, 1), [0, 0], [0, 0]), (8, (2, 3), [0, 0], [0, 0]), (19, (1, 3), [0, 0], [0, 0]),
                                 (20, (0, 2), [0, 0], [0, 0]), (31, (1, 2), [0, 0], [0, 0]), (32, (3, 0), [0, 0], [0, 0]))
            self.x = d['column_group_B']
            self.y = d['row_group_C']
        elif self.name == 'Group E':
            self.team_names = ('Spain', 'Sweden', 'Poland', 'Slovakia')
            self.team_names_short = ('ESP', 'SWE', 'POL', 'SVK')
            self.match_scores = ((9, (0, 1), [0, 0], [0, 0]), (10, (2, 3), [0, 0], [0, 0]), (21, (1, 3), [0, 0], [0, 0]),
                                 (22, (0, 2), [0, 0], [0, 0]), (33, (3, 0), [0, 0], [0, 0]), (34, (1, 2), [0, 0], [0, 0]))
            self.x = d['column_group_A']
            self.y = d['row_group_E']
        elif self.name == 'Group F':
            self.team_names = ('Hungary', 'Portugal', 'France', 'Germany')
            self.team_names_short = ('HUN', 'PRT', 'FRA', 'DEU')
            self.match_scores = ((11, (0, 1), [0, 0], [0, 0]), (12, (2, 3), [0, 0], [0, 0]), (23, (0, 2), [0, 0], [0, 0]),
                                 (24, (1, 3), [0, 0], [0, 0]), (35, (1, 2), [0, 0], [0, 0]), (36, (3, 0), [0, 0], [0, 0]))
            self.x = d['column_group_B']
            self.y = d['row_group_E']

    def get_points_probs_with_third_points(self, ranking, third_points):
        d = {}
        for grp_points in self.group_points_probs[tuple(ranking)]:
            if grp_points[2] == third_points:
                d[grp_points] = self.group_points_probs[tuple(ranking)][grp_points]
        return d

    def get_points_probs_with_third_points_greater_less(self, ranking, in_out_flag, third_points):
        d = {}
        for grp_points in self.group_points_probs[tuple(ranking)]:
            if (grp_points[2] - third_points) * in_out_flag >= 0:
                d[grp_points] = self.group_points_probs[tuple(ranking)][grp_points]
        return d

    def get_third_porbs_for_all_poins(self, ranking):
        d = {}
        for grp_points in self.group_points_probs[tuple(ranking)]:
            if grp_points[2] in d.keys():
                d[grp_points[2]] += self.group_points_probs[tuple(ranking)][grp_points][0]
            else:
                d[grp_points[2]] = self.group_points_probs[tuple(ranking)][grp_points][0]
        return d

    def get_prob_mid_points_for_third(self, ranking):
        d = self.get_third_porbs_for_all_poins(tuple(ranking))
        return sum([it[0] * it[1] for it in list(d.items())]) / 10000

    def sort_by_stats(self):
        rnd = random.sample([0,1,2,3], 4)
        teams = [[self.teams[i].points, self.teams[i].goal_diff, self.teams[i].goal_score, rnd[i], self.teams[i].name] for i in range(4)]
        teams.sort(reverse=True)
        for i, team in enumerate(teams):
            self.places[team[4]] = i
            self.get_team(team[4]).drag_rect.set_place_position(i)

    def sort_of_sort(self, rating):

        def reorder_by_name_sublist(target_list, name_sublist):
            target_list_copy = target_list[:]  # get list of names
            name_sublist_copy = name_sublist[:]
            for name in name_sublist:
                if name not in target_list_copy:
                    name_sublist_copy.remove(name)
            i = 0
            j = 0
            while i < len(name_sublist_copy) and j < len(target_list):
                if target_list[j] in name_sublist_copy:
                    target_list_copy[j] = name_sublist_copy[i]
                    i += 1
                j += 1
            return target_list_copy

        g = self.teams[:]
        sorted_by_points = sorted(g, key=lambda team: team.points, reverse=True)
        sorted_by_points_names = [team.name for team in sorted_by_points]
        sbs1 = [[team.points, team.goal_diff, team.goal_score, team.wins, 100 - team.EQ_ranking, team.name] for team in g]
        sbs2 = random.sample(sbs1[:], len(sbs1))
        sbs2.sort(reverse=True)
        sorted_by_stats = [(sb[0], sb[1], sb[2], sb[3], sb[5]) for sb in sbs2]
        i = 0
        all_disputes = []
        while i < 3:
            disputed_teams = [sorted_by_points_names[i]]
            j = i + 1
            while j < 4 and sorted_by_points[i].points == sorted_by_points[j].points:
                disputed_teams.append(sorted_by_points_names[j])
                j += 1
            i = j
            if len(disputed_teams) > 1:
                all_disputes.append(disputed_teams)
        new_sorted_by_points_names = sorted_by_points_names[:]
        new_all_disputes = []
        for disp in all_disputes:
            new_disp = self.calc_dispute(disp, sorted_by_stats)
            new_all_disputes.append(new_disp)
        for new_d in new_all_disputes:
            new_sorted_by_points_names = reorder_by_name_sublist(new_sorted_by_points_names, new_d)
        if rating == new_sorted_by_points_names:
            return True
        else:
            return False

    def sort_of_sort_with_third_points(self, rating, in_out_flag, min_limit, max_limit):

        def reorder_by_name_sublist(target_list, name_sublist):
            target_list_copy = target_list[:]  # get list of names
            name_sublist_copy = name_sublist[:]
            for name in name_sublist:
                if name not in target_list_copy:
                    name_sublist_copy.remove(name)
            i = 0
            j = 0
            while i < len(name_sublist_copy) and j < len(target_list):
                if target_list[j] in name_sublist_copy:
                    target_list_copy[j] = name_sublist_copy[i]
                    i += 1
                j += 1
            return target_list_copy

        g = self.teams[:]
        sorted_by_points = sorted(g, key=lambda team: team.points, reverse=True)
        sorted_by_points_names = [team.name for team in sorted_by_points]
        sbs1 = [[team.points, team.goal_diff, team.goal_score, team.wins, 100 - team.EQ_ranking, team.name] for team in g]
        sbs2 = random.sample(sbs1[:], len(sbs1))
        sbs2.sort(reverse=True)
        sorted_by_stats = [(sb[0], sb[1], sb[2], sb[3], sb[5]) for sb in sbs2]
        i = 0
        all_disputes = []
        while i < 3:
            disputed_teams = [sorted_by_points_names[i]]
            j = i + 1
            while j < 4 and sorted_by_points[i].points == sorted_by_points[j].points:
                disputed_teams.append(sorted_by_points_names[j])
                j += 1
            i = j
            if len(disputed_teams) > 1:
                all_disputes.append(disputed_teams)
        new_sorted_by_points_names = sorted_by_points_names[:]
        new_all_disputes = []
        for disp in all_disputes:
            new_disp = self.calc_dispute(disp, sorted_by_stats)
            new_all_disputes.append(new_disp)
        for new_d in new_all_disputes:
            new_sorted_by_points_names = reorder_by_name_sublist(new_sorted_by_points_names, new_d)
        if in_out_flag == -1:
            points, goal_diff, goal_score = max_limit
        else:
            points, goal_diff, goal_score = min_limit
        third = self.get_team(rating[2])
        if tuple(rating) == tuple(new_sorted_by_points_names):
            if (third.points - points) * in_out_flag > 0:
                return True
            elif (third.points - points) * in_out_flag == 0:
                if (third.goal_diff - goal_diff) * in_out_flag > 0:
                    return True
                elif (third.goal_diff - goal_diff) * in_out_flag == 0 and (third.goal_score - goal_score) * in_out_flag > 0:
                    return True
        return False

    def real_sort(self):

        def reorder_by_name_sublist(target_list, name_sublist):
            target_list_copy = target_list[:]  # get list of names
            name_sublist_copy = name_sublist[:]
            for name in name_sublist:
                if name not in target_list_copy:
                    name_sublist_copy.remove(name)
            i = 0
            j = 0
            while i < len(name_sublist_copy) and j < len(target_list):
                if target_list[j] in name_sublist_copy:
                    target_list_copy[j] = name_sublist_copy[i]
                    i += 1
                j += 1
            return target_list_copy

        g = self.teams[:]
        sorted_by_points = sorted(g, key=lambda team: team.points, reverse=True)
        sorted_by_points_names = [team.name for team in sorted_by_points]
        sbs1 = [[team.points, team.goal_diff, team.goal_score, team.wins, 100 - team.EQ_ranking, team.name] for team in g]
        sbs2 = random.sample(sbs1[:], len(sbs1))
        sbs2.sort(reverse=True)
        sorted_by_stats = [(sb[0], sb[1], sb[2], sb[3], sb[5]) for sb in sbs2]
        i = 0
        all_disputes = []
        while i < 3:
            disputed_teams = [sorted_by_points_names[i]]
            j = i + 1
            while j < 4 and sorted_by_points[i].points == sorted_by_points[j].points:
                disputed_teams.append(sorted_by_points_names[j])
                j += 1
            i = j
            if len(disputed_teams) > 1:
                all_disputes.append(disputed_teams)
        new_sorted_by_points_names = sorted_by_points_names[:]
        new_all_disputes = []
        for disp in all_disputes:
            new_disp = self.calc_dispute(disp, sorted_by_stats)
            new_all_disputes.append(new_disp)
        for new_d in new_all_disputes:
            new_sorted_by_points_names = reorder_by_name_sublist(new_sorted_by_points_names, new_d)
        for i, team_name in enumerate(new_sorted_by_points_names):
            self.places[team_name] = i
            self.get_team(team_name).drag_rect.set_place_position(i)


    def recalc_matches_for_disputed_teames(self, disputed_stat_list):

        def get_list_item(list_t, name):
            return [l for l in list_t if l[5] == name][0]
        new_stat_list = []
        eq_dict = Group.group_pos_dict['EQ_ranking']
        for t_name in disputed_stat_list:
            new_stat_list.append([0, 0, 0, 0, 100 - eq_dict[t_name[4]], t_name[4]])
        disputed_teams_names = [t_name[4] for t_name in disputed_stat_list]
        for match in self.match_scores:
            team1_name, team2_name = self.team_names[match[1][0]], self.team_names[match[1][1]]
            score1, score2 = match[2][0], match[2][1]
            if team1_name in disputed_teams_names and team2_name in disputed_teams_names:
                get_list_item(new_stat_list, team1_name)[2] += score1
                get_list_item(new_stat_list, team2_name)[2] += score2
                get_list_item(new_stat_list, team1_name)[1] += score1 - score2
                get_list_item(new_stat_list, team2_name)[1] += score2 - score1
                if score1 > score2:
                    get_list_item(new_stat_list, team1_name)[0] += 3
                    get_list_item(new_stat_list, team1_name)[3] += 1
                elif score1 < score2:
                    get_list_item(new_stat_list, team2_name)[0] += 3
                    get_list_item(new_stat_list, team1_name)[3] += 1
                else:
                    get_list_item(new_stat_list, team1_name)[0] += 1
                    get_list_item(new_stat_list, team2_name)[0] += 1
        new_stat_list.sort(reverse=True)
        return [[nsl[0], nsl[1], nsl[2], nsl[3], nsl[5]] for nsl in new_stat_list]

    def calc_dispute(self, disputed_teams, sorted_by_stats):

        def get_list_item(list_t, name):
            return [l for l in list_t if l[4] == name][0]

        def get_list_of_names(lst):
            return [l[4] for l in lst]

        def reorder_by_name_sublist(target_list, name_sublist):
            target_list_names = get_list_of_names(target_list)
            name_sublist_copy = name_sublist[:]
            for name in name_sublist:
                if name not in target_list_names:
                    name_sublist_copy.remove(name)
            sublist = [get_list_item(target_list, name) for name in name_sublist_copy]
            target_list_copy = target_list[:]
            i = 0
            j = 0
            while i < len(sublist) and j < len(target_list):
                if target_list[j] in sublist:
                    target_list_copy[j] = sublist[i]
                    i += 1
                j += 1
            return target_list_copy

        t_list = [[0, 0, 0, 0, t_name] for t_name in disputed_teams]
        team_list = self.recalc_matches_for_disputed_teames(t_list)
        i = 0
        while i < len(disputed_teams) - 1:
            dispute_list = [team_list[i]]
            j = i + 1
            while j < len(disputed_teams) and team_list[i][0] == team_list[j][0] and team_list[i][1] == team_list[j][
                1] and team_list[i][2] == team_list[j][2] and team_list[i][3] == team_list[j][3]:
                dispute_list.append(team_list[j])
                j += 1
            if len(dispute_list) == 2:
                disp_2 = self.recalc_matches_for_disputed_teames(dispute_list[:])
                if disp_2[0][0] == disp_2[1][0] and disp_2[0][1] == disp_2[1][1] and disp_2[0][2] == disp_2[1][2] and disp_2[0][3] == disp_2[1][3]:
                    disp_2_total_stats_1 = [s for s in sorted_by_stats if s[4] == disp_2[0][4]][0]
                    disp_2_total_stats_2 = [s for s in sorted_by_stats if s[4] == disp_2[1][4]][0]
                    if disp_2_total_stats_1[0] == disp_2_total_stats_2[0] and disp_2_total_stats_1[1] == \
                            disp_2_total_stats_2[1] and disp_2_total_stats_1[2] == disp_2_total_stats_2[2] and \
                            disp_2_total_stats_1[3] == disp_2_total_stats_2[3]:
                        list_of_last_round_matches = (
                        ['Switzerland', 'Turkey'], ['Italy', 'Wales'], ['Russia', 'Denmark'], ['Finland', 'Belgium'],
                        ['North Macedonia', 'Netherlands'], ['Ukraine', 'Austria'], ['Croatia', 'Scotland'],
                        ['Czech Republic', 'England'], ['Slovakia', 'Spain'], ['Sweden', 'Poland'], ['Portugal', 'France'],
                        ['Germany', 'Hungary'])
                        disp_2_names_1 = [disp_2[0][4], disp_2[1][4]]
                        disp_2_names_2 = [disp_2[1][4], disp_2[0][4]]
                        if disp_2_names_1 in list_of_last_round_matches or disp_2_names_2 in list_of_last_round_matches:
                            rnd = random.randint(0, 1)
                            sorted_disp_2 = [disp_2[rnd], disp_2[(rnd + 1) % 2]]
                            group = Group.all_groups[[g for g in Group.all_groups if sorted_disp_2[0][4] in Group.all_groups[g].team_names][0]]
                            index_of_name_penalty_winner = group.team_names.index(sorted_disp_2[0][4])
                            if index_of_name_penalty_winner in group.match_scores[4][1]:
                                index_of_penalty_winner = group.match_scores[4][1].index(index_of_name_penalty_winner)
                                group.match_scores[4][3][index_of_penalty_winner] = 1
                            elif index_of_name_penalty_winner in group.match_scores[5][1]:
                                index_of_penalty_winner = group.match_scores[5][1].index(index_of_name_penalty_winner)
                                group.match_scores[5][3][index_of_penalty_winner] = 1
                            self.calc_table()

                        else:
                            sorted_disp_2 = reorder_by_name_sublist(disp_2, get_list_of_names(sorted_by_stats))
                    else:
                        sorted_disp_2 = reorder_by_name_sublist(disp_2, get_list_of_names(sorted_by_stats))
                    new_dispute_list = reorder_by_name_sublist(dispute_list, [sorted_disp_2[0][4], sorted_disp_2[1][4]])
                    new_team_list = reorder_by_name_sublist(team_list, get_list_of_names(new_dispute_list))
                else:
                    new_dispute_list = reorder_by_name_sublist(dispute_list, get_list_of_names(disp_2))
                    new_team_list = reorder_by_name_sublist(team_list, get_list_of_names(new_dispute_list))
            elif len(dispute_list) > 2:
                if len(dispute_list) == len(team_list):
                    new_team_list = reorder_by_name_sublist(team_list, sorted_by_stats)
                else:
                    new_dispute_list = self.calc_dispute([dispute_list_item[3] for dispute_list_item in dispute_list],
                                                    sorted_by_stats)
                    new_team_list = reorder_by_name_sublist(team_list, new_dispute_list)
            else:
                new_team_list = team_list
            team_list = new_team_list[:]
            i = j

        return [team_list_item[4] for team_list_item in team_list]

    def pdf(self, x, mu, sigma):
        # probability_density_function
        return 1 / (sigma * math.sqrt(2 * math.pi)) * math.exp(-0.5 * ((x - mu) / sigma) ** 2)

    def generate_group_matches_without_sort(self):
        for match in self.match_scores:
            team1_name = self.team_names[match[1][0]]
            team2_name = self.team_names[match[1][1]]
            team1 = self.get_team(team1_name)
            team2 = self.get_team(team2_name)
            if team1.level < team2.level:
                win_draw_lose = list(self.group_pos_dict['match_win_draw_lose'][(team2.level, team1.level)])
                win_draw_lose.reverse()
            else:
                win_draw_lose = self.group_pos_dict['match_win_draw_lose'][(team1.level, team2.level)]
            w_d_l = random.choices('wdl', win_draw_lose)[0]
            a_d_1 = team1.attack - team2.defence
            a_d_2 = team2.attack - team1.defence
            mu1 = (a_d_1 + 10) / 5
            mu2 = (a_d_2 + 10) / 5
            sigma1 = 2 ** (mu1 - 2)
            sigma2 = 2 ** (mu2 - 2)
            team1_score = random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[0]
            team2_score = random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[0]
            if w_d_l == 'w':
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(7)), [self.pdf(i, mu2, sigma2) for i in range(7)])[0]
                    team1_score = max(team2_score + 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu1, sigma1) for i in range(8)])[
                        0])
                else:
                    team1_score = max(1,
                                      random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[
                                          0])
                    team2_score = min(team1_score - 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu2, sigma2) for i in range(8)])[
                        0])
            elif w_d_l == 'l':
                if team1.level >= team2.level:
                    team2_score = max(1,
                                      random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[
                                          0])
                    team1_score = min(team2_score - 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu1, sigma1) for i in range(8)])[
                        0])
                else:
                    team1_score = random.choices(list(range(7)), [self.pdf(i, mu1, sigma1) for i in range(7)])[0]
                    team2_score = max(team1_score + 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu2, sigma2) for i in range(8)])[
                        0])
            else:
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[0]
                    team1_score = team2_score
                else:
                    team1_score = random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[0]
                    team2_score = team1_score
            match[2][0], match[2][1] = team1_score, team2_score
        self.zero_fill_penalties()
        self.calc_table()

    def generate_group_matches(self, widget):
        self.generate_group_matches_without_sort()
        self.real_sort()

    def generate_group_matches_from_letters(self, letters):
        for j, match in enumerate(self.match_scores):
            team1_name = self.team_names[match[1][0]]
            team2_name = self.team_names[match[1][1]]
            team1 = self.get_team(team1_name)
            team2 = self.get_team(team2_name)
            w_d_l = letters[j]
            a_d_1 = team1.attack - team2.defence
            a_d_2 = team2.attack - team1.defence
            mu1 = (a_d_1 + 10) / 5
            mu2 = (a_d_2 + 10) / 5
            sigma1 = 2 ** (mu1 - 2)
            sigma2 = 2 ** (mu2 - 2)
            team1_score = random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[0]
            team2_score = random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[0]
            if w_d_l == 'w':
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(7)), [self.pdf(i, mu2, sigma2) for i in range(7)])[0]
                    team1_score = max(team2_score + 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu1, sigma1) for i in range(8)])[
                        0])
                else:
                    team1_score = max(1,
                                      random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[
                                          0])
                    team2_score = min(team1_score - 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu2, sigma2) for i in range(8)])[
                        0])
            elif w_d_l == 'l':
                if team1.level >= team2.level:
                    team2_score = max(1,
                                      random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[
                                          0])
                    team1_score = min(team2_score - 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu1, sigma1) for i in range(8)])[
                        0])
                else:
                    team1_score = random.choices(list(range(7)), [self.pdf(i, mu1, sigma1) for i in range(7)])[0]
                    team2_score = max(team1_score + 1, random.choices(list(range(8)),
                                                                      [self.pdf(i, mu2, sigma2) for i in range(8)])[
                        0])
            else:
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(8)), [self.pdf(i, mu2, sigma2) for i in range(8)])[0]
                    team1_score = team2_score
                else:
                    team1_score = random.choices(list(range(8)), [self.pdf(i, mu1, sigma1) for i in range(8)])[0]
                    team2_score = team1_score
            match[2][0], match[2][1] = team1_score, team2_score
        self.zero_fill_penalties()
        self.calc_table()

    def generate_group_matches_from_rating_third_points(self, tuple_rating, in_out_flag, min_limit, max_limit):
        tuple_rating = tuple(tuple_rating)
        if in_out_flag == -1:
            points, goal_diff, goal_score = max_limit
        else:
            points, goal_diff, goal_score = min_limit
        d = self.get_points_probs_with_third_points_greater_less(tuple_rating, in_out_flag, points)
        points_list = []
        probs_list = []
        for k, v in d.items():
            points_list.append(k)
            probs_list.append(v[0])
        group_points = random.choices(points_list, probs_list)[0]
        letters_dict = d[group_points][1]
        letters = random.choices(list(letters_dict.keys()), list(letters_dict.values()))[0]
        self.generate_group_matches_from_letters(letters)
        counter = 0
        third = self.get_team(tuple_rating[2])
        str_out =[]
        while not self.sort_of_sort_with_third_points(tuple_rating, in_out_flag, min_limit, max_limit):
            self.generate_group_matches_from_letters(letters)
            str_out += (repr(self) + '\n')
            counter += 1
            if counter > 100:
                break
        third = self.get_team(tuple_rating[2])

    def calc_table(self):
        for team in self.teams:
            team.goal_score = 0
            team.goal_diff = 0
            team.points = 0
            team.wins = 0
        for i, match in enumerate(self.match_scores):
            self.teams[match[1][0]].goal_score += match[2][0]
            self.teams[match[1][1]].goal_score += match[2][1]
            self.teams[match[1][0]].goal_diff += match[2][0] - match[2][1]
            self.teams[match[1][1]].goal_diff += match[2][1] - match[2][0]
            if match[2][1] < match[2][0]:
                self.teams[match[1][0]].points += 3
                self.teams[match[1][0]].wins += 1
            elif match[2][0] < match[2][1]:
                self.teams[match[1][1]].points += 3
                self.teams[match[1][1]].wins += 1
            else:
                self.teams[match[1][0]].points += 1
                self.teams[match[1][1]].points += 1

    def __repr__(self):
        tlst = [(t.name, t.wins, t.EQ_ranking, t.goal_score, t.goal_diff, t.points) for t in self.get_rated_teams()]
        out_str = ''
        for lst in tlst:
            out_str += str(lst[0]) + ' ' + str(lst[1]) + ' ' + str(lst[2]) + ' ' + str(lst[3]) + ' ' + str(lst[4]) + ' ' + str(lst[5])
            out_str += '\n'
        return out_str




