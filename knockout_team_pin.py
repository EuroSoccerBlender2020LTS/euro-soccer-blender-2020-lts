import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
from .calc_dimencsions import *
from .knockout_phase import *


class KnokOutTeamPin(Rect):

    def __init__(self, x, y):
        self.dimensions_dict = calc_knockout_positions()
        self.x = x
        self.y = y
        self.height = self.dimensions_dict['rect_height'] / 2
        self.width = self.dimensions_dict['rect_width'] - self.height * 5 / 3
        self.column_1_x = self.dimensions_dict['column_1_x']
        self.column_2_x = self.dimensions_dict['column_2_x']
        self.column_3_x = self.dimensions_dict['column_3_x']
        self.column_4_x = self.dimensions_dict['column_4_x']
        self.column_5_x = self.dimensions_dict['column_5_x']

        self.row = -500
        self.column = -500
        self.match_id = -1
        self.match_position = -1
        self.pressed = False
        self.enabled = True
        self.activated = False
        self.locked = False
        self.pined = False
        self.set_color([0.3, 0.7, 0.7, 0.5])
        self.update(self.x, self.y)
        self.neighbours = []



    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        if not self.is_on_target(x, y):
            self.x = -100
            self.y = -100
        else:
            self.x = self.column
            self.y = self.row
        width = self.width
        height = self.height
        if self.match_id == 51:
            height = self.height * self.dimensions_dict['final_coef']
            width = self.width * self.dimensions_dict['final_coef']
        vertices = (
            (self.x, self.y),
            (self.x, self.y + height),
            (self.x + width, self.y + height),
            (self.x + width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def is_on_target(self, x, y):
        # Round of 16 column_1
        final_coef = self.dimensions_dict['final_coef']
        height = self.dimensions_dict['rect_height']
        width = self.dimensions_dict['rect_width'] - 5 * height / 6
        column_1_x = self.dimensions_dict['column_1_x'] + height / 2
        column_2_x = self.dimensions_dict['column_2_x'] + height / 2
        column_3_x = self.dimensions_dict['column_3_x'] + height / 2
        column_4_x = self.dimensions_dict['column_4_x'] + height / 2
        column_5_x = self.dimensions_dict['column_5_x'] + final_coef * height / 2
        if column_1_x  < x < column_1_x + width:
            self.column = column_1_x
            # Match 39
            if self.dimensions_dict['column_1_1_y'] < y < self.dimensions_dict['column_1_1_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_1_y']
                self.match_id = 39
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_1_y'] + height / 2 < y < self.dimensions_dict['column_1_1_y'] + height:
                self.row = self.dimensions_dict['column_1_1_y'] + height / 2
                self.match_id = 39
                self.match_position = 0
                return True
            # Match 42
            elif self.dimensions_dict['column_1_2_y'] < y < self.dimensions_dict['column_1_2_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_2_y']
                self.match_id = 42
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_2_y'] + height / 2< y < self.dimensions_dict['column_1_2_y'] + height:
                self.row = self.dimensions_dict['column_1_2_y'] + height / 2
                self.match_id = 42
                self.match_position = 0
                return True
            # Match 43
            elif self.dimensions_dict['column_1_3_y'] < y < self.dimensions_dict['column_1_3_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_3_y']
                self.match_id = 43
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_3_y'] + height / 2 < y < self.dimensions_dict['column_1_3_y'] + height:
                self.row = self.dimensions_dict['column_1_3_y'] + height / 2
                self.match_id = 43
                self.match_position = 0
                return True
            # Match 38
            elif self.dimensions_dict['column_1_4_y'] < y < self.dimensions_dict['column_1_4_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_4_y']
                self.match_id = 38
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_4_y'] + height / 2 < y < self.dimensions_dict['column_1_4_y'] + height:
                self.row = self.dimensions_dict['column_1_4_y'] + height / 2
                self.match_id = 38
                self.match_position = 0
                return True
        if column_2_x  < x < column_2_x + width:
            self.column = column_2_x
            # Match 37
            if self.dimensions_dict['column_2_1_y'] < y < self.dimensions_dict['column_2_1_y'] + height / 2:
                self.row = self.dimensions_dict['column_2_1_y']
                self.match_id = 37
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_2_1_y'] + height / 2 < y < self.dimensions_dict['column_2_1_y'] + height:
                self.row = self.dimensions_dict['column_2_1_y'] + height / 2
                self.match_id = 37
                self.match_position = 0
                return True
            # Match 41
            elif self.dimensions_dict['column_2_2_y'] < y < self.dimensions_dict['column_2_2_y'] + height / 2:
                self.row = self.dimensions_dict['column_2_2_y']
                self.match_id = 41
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_2_2_y'] + height / 2 < y < self.dimensions_dict['column_2_2_y'] + height:
                self.row = self.dimensions_dict['column_2_2_y'] + height / 2
                self.match_id = 41
                self.match_position = 0
                return True
            # Match 44
            elif self.dimensions_dict['column_2_3_y'] < y < self.dimensions_dict['column_2_3_y'] + height / 2:
                self.row = self.dimensions_dict['column_2_3_y']
                self.match_id = 44
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_2_3_y'] + height / 2 < y < self.dimensions_dict['column_2_3_y'] + height:
                self.row = self.dimensions_dict['column_2_3_y'] + height / 2
                self.match_id = 44
                self.match_position = 0
                return True
            # Match 40
            elif self.dimensions_dict['column_2_4_y'] < y < self.dimensions_dict['column_2_4_y'] + height / 2:
                self.row = self.dimensions_dict['column_2_4_y']
                self.match_id = 40
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_2_4_y'] + height / 2 < y < self.dimensions_dict['column_2_4_y'] + height:
                self.row = self.dimensions_dict['column_2_4_y'] + height / 2
                self.match_id = 40
                self.match_position = 0
                return True
        if column_3_x  < x < column_3_x + width:
            self.column = column_3_x
            # Match 46
            if self.dimensions_dict['column_1_1_y'] < y < self.dimensions_dict['column_1_1_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_1_y']
                self.match_id = 46
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_1_y'] + height / 2 < y < self.dimensions_dict['column_1_1_y'] + height:
                self.row = self.dimensions_dict['column_1_1_y'] + height / 2
                self.match_id = 46
                self.match_position = 0
                return True
            # Match 45
            elif self.dimensions_dict['column_1_2_y'] < y < self.dimensions_dict['column_1_2_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_2_y']
                self.match_id = 45
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_2_y'] + height / 2 < y < self.dimensions_dict['column_1_2_y'] + height:
                self.row = self.dimensions_dict['column_1_2_y'] + height / 2
                self.match_id = 45
                self.match_position = 0
                return True
            # Match 48
            elif self.dimensions_dict['column_1_3_y'] < y < self.dimensions_dict['column_1_3_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_3_y']
                self.match_id = 48
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_3_y'] + height / 2 < y < self.dimensions_dict['column_1_3_y'] + height:
                self.row = self.dimensions_dict['column_1_3_y'] + height / 2
                self.match_id = 48
                return True
                self.match_position = 0
            # Match 47
            elif self.dimensions_dict['column_1_4_y'] < y < self.dimensions_dict['column_1_4_y'] + height / 2:
                self.row = self.dimensions_dict['column_1_4_y']
                self.match_id = 47
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_1_4_y'] + height / 2 < y < self.dimensions_dict['column_1_4_y'] + height:
                self.row = self.dimensions_dict['column_1_4_y'] + height / 2
                self.match_id = 47
                self.match_position = 0
                return True
        if column_4_x  < x < column_4_x + width:
            self.column = column_4_x
            # Match 49
            if self.dimensions_dict['column_4_1_y'] < y < self.dimensions_dict['column_4_1_y'] + height / 2:
                self.row = self.dimensions_dict['column_4_1_y']
                self.match_id = 49
                self.match_position = 1
                return True
            if self.dimensions_dict['column_4_1_y'] + height / 2 < y < self.dimensions_dict['column_4_1_y'] + height:
                self.row = self.dimensions_dict['column_4_1_y'] + height / 2
                self.match_id = 49
                self.match_position = 0
                return True
            # Match 50
            elif self.dimensions_dict['column_4_2_y'] < y < self.dimensions_dict['column_4_2_y'] + height / 2:
                self.row = self.dimensions_dict['column_4_2_y']
                self.match_id = 50
                self.match_position = 1
                return True
            elif self.dimensions_dict['column_4_2_y'] + height / 2 < y < self.dimensions_dict['column_4_2_y'] + height:
                self.row = self.dimensions_dict['column_4_2_y'] + height / 2
                self.match_id = 50
                self.match_position = 0
                return True
        if column_5_x  < x < column_5_x + width * final_coef:
            self.column = column_5_x
            # Match 51 - final
            if self.dimensions_dict['column_5_y'] < y < self.dimensions_dict['column_5_y'] + height * final_coef / 2:
                self.row = self.dimensions_dict['column_5_y']
                self.match_id = 51
                self.match_position = 1
                return True
            if self.dimensions_dict['column_5_y'] + height * final_coef / 2 < y < self.dimensions_dict['column_5_y'] + height * final_coef:
                self.row = self.dimensions_dict['column_5_y'] + height * final_coef / 2
                self.match_id = 51
                self.match_position = 0
                return True

        self.match_id = -1
        return False

    def handle_event(self, event):
        if (event.type == "RIGHTMOUSE"):
            if (event.value == "PRESS"):
                return self.right_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.right_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

