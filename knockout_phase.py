from .calc_dimencsions import *
import bpy
import math
import random
from .knockout_rect import *
from .thirds_drag_rect import *
import itertools

class MatchTeam:

    def __init__(self, name, id, position, str_position, image, x, y, width, score, dummy_team=None):
        self.name = name
        self.team = dummy_team
        self.id = id
        self.position = position
        self.str_position = str_position
        self.image = image
        self.x = x
        self.y = y
        self.width = width
        self.score = score
        self.pinned_team = False
        self.all_available_teams = ()
        self.available_teams = list(self.all_available_teams)
        self.dirpath = KnockOutPhase.dirpath
        self.children = []
        self.parents = []

    def set_match_team(self, team):
        if team == None:
            self.team = None
            self.name = 'None'
        else:
            self.team = team
            self.name = team.name

    def get_place_from_groups(self):
        if self.team == None:
            return ('None', 999)
        else:
            if self.id > 44:
                parent0 = self.get_0_parent()
            else:
                parent0 = self
            if parent0.id == 39 and parent0.position == 0:
                return ('B', 0)
            elif parent0.id == 39 and parent0.position == 1:
                return ('ADEF', 2)
            elif parent0.id == 37 and parent0.position == 0:
                return ('A', 0)
            elif parent0.id == 37 and parent0.position == 1:
                return ('C', 1)
            elif parent0.id == 41 and parent0.position == 0:
                return ('F', 0)
            elif parent0.id == 41 and parent0.position == 1:
                return ('ABC', 2)
            elif parent0.id == 42 and parent0.position == 0:
                return ('D', 1)
            elif parent0.id == 42 and parent0.position == 1:
                return ('E', 1)
            elif parent0.id == 43 and parent0.position == 0:
                return ('E', 0)
            elif parent0.id == 43 and parent0.position == 1:
                return ('ABCD', 2)
            elif parent0.id == 44 and parent0.position == 0:
                return ('D', 0)
            elif parent0.id == 44 and parent0.position == 1:
                return ('F', 1)
            elif parent0.id == 40 and parent0.position == 0:
                return ('C', 0)
            elif parent0.id == 40 and parent0.position == 1:
                return ('DEF', 2)
            elif parent0.id == 38 and parent0.position == 0:
                return ('A', 1)
            elif parent0.id == 38 and parent0.position == 1:
                return ('B', 1)
            else:
                return ('None', 999)

    def get_parents(self):
        if self.id in range(37, 45):
            return []
        elif self.id == 46 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(39, 0), KnockOutPhase.get_match_team_by_id_pos(39, 1)]
        elif self.id == 46 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(37, 0), KnockOutPhase.get_match_team_by_id_pos(37, 1)]
        elif self.id == 45 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(41, 0), KnockOutPhase.get_match_team_by_id_pos(41, 1)]
        elif self.id == 45 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(42, 0), KnockOutPhase.get_match_team_by_id_pos(42, 1)]
        elif self.id == 48 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(43, 0), KnockOutPhase.get_match_team_by_id_pos(43, 1)]
        elif self.id == 48 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(44, 0), KnockOutPhase.get_match_team_by_id_pos(44, 1)]
        elif self.id == 47 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(40, 0), KnockOutPhase.get_match_team_by_id_pos(40, 1)]
        elif self.id == 47 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(38, 0), KnockOutPhase.get_match_team_by_id_pos(38, 1)]
        elif self.id == 49 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(46, 0), KnockOutPhase.get_match_team_by_id_pos(46, 1)]
        elif self.id == 49 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(45, 0), KnockOutPhase.get_match_team_by_id_pos(45, 1)]
        elif self.id == 50 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(48, 0), KnockOutPhase.get_match_team_by_id_pos(48, 1)]
        elif self.id == 50 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(47, 0), KnockOutPhase.get_match_team_by_id_pos(47, 1)]
        elif self.id == 51 and self.position == 0:
            return [KnockOutPhase.get_match_team_by_id_pos(49, 0), KnockOutPhase.get_match_team_by_id_pos(49, 1)]
        elif self.id == 51 and self.position == 1:
            return [KnockOutPhase.get_match_team_by_id_pos(50, 0), KnockOutPhase.get_match_team_by_id_pos(50, 1)]

    def is_parent_of(self, id):
        if self.id <= id or id not in range(37, 51):
            return False
        if self.id == 46 and id in [37, 39]:
            return True
        elif self.id == 45 and id in [41, 42]:
            return True
        elif self.id == 48 and id in [43, 44]:
            return True
        elif self.id == 47 and id in [40, 38]:
            return True
        elif self.id == 49 and id in [39, 37, 41, 42, 46, 45]:
            return True
        elif self.id == 50 and id in [43, 44, 40, 38, 48, 47]:
            return True
        else:
            print("128________What's going on???", self, id)

    def get_child(self):
        if self.id == 39:
            return KnockOutPhase.get_match_team_by_id_pos(46, 0)
        elif self.id == 37:
            return KnockOutPhase.get_match_team_by_id_pos(46, 1)
        elif self.id == 41:
            return KnockOutPhase.get_match_team_by_id_pos(45, 0)
        elif self.id == 42:
            return KnockOutPhase.get_match_team_by_id_pos(45, 1)
        elif self.id == 43:
            return KnockOutPhase.get_match_team_by_id_pos(48, 0)
        elif self.id == 44:
            return KnockOutPhase.get_match_team_by_id_pos(48, 1)
        elif self.id == 40:
            return KnockOutPhase.get_match_team_by_id_pos(47, 0)
        elif self.id == 38:
            return KnockOutPhase.get_match_team_by_id_pos(47, 1)
        elif self.id == 46:
            return KnockOutPhase.get_match_team_by_id_pos(49, 0)
        elif self.id == 45:
            return KnockOutPhase.get_match_team_by_id_pos(49, 1)
        elif self.id == 48:
            return KnockOutPhase.get_match_team_by_id_pos(50, 0)
        elif self.id == 47:
            return KnockOutPhase.get_match_team_by_id_pos(50, 1)
        elif self.id == 49:
            return KnockOutPhase.get_match_team_by_id_pos(51, 0)
        elif self.id == 50:
            return KnockOutPhase.get_match_team_by_id_pos(51, 1)
        # elif self.id == 51:
        #     return []

    def get_all_parents(self):
        lst = self.get_parents()
        for parent in self.get_parents():
            lst.extend((parent.get_all_parents()))
        return lst

    def get_0_parents(self):
        lst = []
        for parent in self.get_all_parents():
            if parent.id in range(37, 45):
                lst.append(parent)
        return lst

    def get_0_parent(self):
        if self.team is not None:
            parents = self.get_parents()
            if not parents:
                return self
            if parents[0].team == self.team:
                parent0 = parents[0]
            else:
                parent0 = parents[1]
            while parent0.id > 44:
                parent0 = parent0.get_0_parent()
            return parent0
        else:
            print('None team has no 0_child')

    def get_all_children(self):
        if self.id == 51:
            return []
        else:
            child = self.get_child()
            lst = [child]
            if child.id != 51:
                lst.extend(child.get_all_children())
        return lst

    def get_opponent(self):
        return KnockOutPhase.get_match_team_by_id_pos(self.id, (self.position + 1) % 2)

    @property
    def is_pinned(self):
        return (self.id, self.position) in KnockOutPhase.pinned

    def __repr__(self):
        return 'MatchTeam' + repr((self.id, self.position, self.name))

class KnockOutPhase:
    popup_activated = False
    match_teams = []
    dimensions_dict = None
    none_image = None
    ranking_of_thirds = None
    forced_4_thirds = 'ABCD'
    tournament_teams = {}
    pinned = []
    bg = None
    drag_rects = []
    thirds_rated_places = {}
    group_rankings_probs = {}
    dirpath = ''

    def __init__(self):
        KnockOutPhase.dimensions_dict = calc_knockout_positions()
        KnockOutPhase.pinned = []
        KnockOutPhase.group_rankings_probs = group_rankings_probabilities
        if KnockOutPhase.none_image == None:
            self.none_image = self.load_none_image()
        KnockOutPhase.none_image = self.none_image
        self.init_all_matches()
        self.bg = KnockOutBG(KnockOutPhase.match_teams)
        KnockOutPhase.bg = self.bg
        KnockOutPhase.ranking_of_thirds = [['None', 'Group _', None] for i in range(6)]
        KnockOutPhase.drag_rects = [ThirdsDragRect(i, KnockOutPhase.ranking_of_thirds[i], self) for i in range(6)]
        KnockOutPhase.calc_neighbours()
        KnockOutPhase.thirds_rated_places = {}
        for d_r in KnockOutPhase.drag_rects:
            KnockOutPhase.thirds_rated_places[d_r.team_name] = d_r.place

    @classmethod
    def calc_neighbours(self):
        for drag_rect in KnockOutPhase.drag_rects[:]:
            drag_rect.neighbours = [drag_rect for drag_rect in KnockOutPhase.drag_rects]
            drag_rect.neighbours.remove(drag_rect)

    def load_none_image(self):
        filepath = self.dirpath + '/flags/None.png'
        if 'None.png' not in bpy.data.images:
            image = bpy.data.images.load(filepath)
        image = bpy.data.images['None.png']
        if image.gl_load():
            raise Exception()
        return image

    @classmethod
    def reload_ranking_of_thirds(cls):
        KnockOutPhase.thirds_rated_places = {}
        old_rated_list = KnockOutPhase.get_rating_of_drag_thirds()
        for i in range(6):
            old_rated_list[i].group_name = KnockOutPhase.ranking_of_thirds[i][1]
            old_rated_list[i].team = KnockOutPhase.ranking_of_thirds[i][2]
            old_rated_list[i].team_name = KnockOutPhase.ranking_of_thirds[i][0]
            old_rated_list[i].set_text()
            old_rated_list[i].place = i
            KnockOutPhase.thirds_rated_places[KnockOutPhase.ranking_of_thirds[i][0]] = i

    @classmethod
    def replace_chosen_thirds(cls, new_letters_of_4_thirds):
        old_rating = KnockOutPhase.get_rating_of_drag_thirds()
        grps_letters = [dr_th.group_name[-1] for dr_th in old_rating]
        if sorted(grps_letters[:4]) == sorted(list(new_letters_of_4_thirds)):
            return
        new_letters_of_thirds_reordered = ''
        tail = ''
        for ltr in grps_letters:
            if ltr in new_letters_of_4_thirds:
                new_letters_of_thirds_reordered += ltr
            else:
                tail += ltr
        new_letters_of_thirds_reordered += tail
        KnockOutPhase.thirds_rated_places = {}
        for i in range(6):
            old_rating[i].group_name = 'Group ' + new_letters_of_thirds_reordered[i]
            old_rating[i].team = KnockOutPhase.get_drag_third_team_by_group_name('Group ' + new_letters_of_thirds_reordered[i])
            old_rating[i].team_name = KnockOutPhase.get_drag_third_team_by_group_name('Group ' + new_letters_of_thirds_reordered[i]).team_name
            old_rating[i].set_text()
            old_rating[i].place = i
            KnockOutPhase.thirds_rated_places[KnockOutPhase.ranking_of_thirds[i][0]] = i


    @classmethod
    def get_rating_of_drag_thirds(cls):
        lst = []
        for i in range(6):
            for d_r in KnockOutPhase.drag_rects:
                if i == d_r.place:
                    lst.append(d_r)
        return lst

    def recalc_places_of_draging_thirds(self, drag_rect, place):
        old_rating = self.get_rating_of_drag_thirds()
        rating = old_rating[:]
        rating.remove(drag_rect)
        rating.insert(place, drag_rect)
        for i, d_r in enumerate(rating):
            KnockOutPhase.thirds_rated_places[d_r.team_name] = i
        for d_r in self.drag_rects:
            if d_r.team_name != drag_rect.team_name and KnockOutPhase.thirds_rated_places[d_r.team_name] != d_r.place:  # !!!!!!!
                d_r.set_place_position(KnockOutPhase.thirds_rated_places[d_r.team_name])
        if not drag_rect.is_drag:
            let4 = ''.join(sorted([r_list.group_name[-1] for r_list in self.get_rating_of_drag_thirds()[:4]]))
            if KnockOutPhase.check_letters_with_pins(let4):
                for th in self.get_4_thirds():
                    if th[1].name != KnockOutPhase.get_match_team_by_id_pos(th[0], 1).name:
                        pins_backup = []
                        for pin in KnockOutPhase.pinned:
                            m_t = KnockOutPhase.get_match_team_by_id_pos(pin[0], pin[1])
                            pins_backup.append((pin, m_t, m_t.team))
                        KnockOutPhase.get_match_team_by_id_pos(th[0], 1).set_match_team(th[1])
                        KnockOutPhase.clear_score_included(th[0])
                        for pin_b in pins_backup:
                            pin_way = KnockOutPhase.choose_way_to_point(pin_b[2], pin_b[0][0], pin_b[0][1])
                            KnockOutPhase.find_winners_for(pin_b[0][0], pin_b[0][1], pin_way, pin_b[2])
                for d_r in self.drag_rects:
                    d_r.bckp_place = d_r.place
            else:
                # restore backup
                for d_r in self.drag_rects:
                    d_r.set_place_position(d_r.bckp_place)


    @classmethod
    def get_available_teams_at(cls, id, pos):
        if id == 39:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Denmark', 'Finland', 'Belgium', 'Russia']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Turkey', 'Italy', 'Wales', 'Switzerland', 'England', 'Croatia', 'Scotland', 'Czech Republic', 'Spain', 'Sweden', 'Poland', 'Slovakia', 'Hungary', 'Portugal', 'France', 'Germany']]
        elif id == 37:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Turkey', 'Italy', 'Wales', 'Switzerland']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Netherlands', 'Ukraine', 'Austria', 'North Macedonia']]
        elif id == 41:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Hungary', 'Portugal', 'France', 'Germany']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Turkey', 'Italy', 'Wales', 'Switzerland', 'Denmark', 'Finland', 'Belgium', 'Russia', 'Netherlands', 'Ukraine', 'Austria', 'North Macedonia']]
        elif id == 42:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['England', 'Croatia', 'Scotland', 'Czech Republic']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Spain', 'Sweden', 'Poland', 'Slovakia']]
        elif id == 43:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Spain', 'Sweden', 'Poland', 'Slovakia']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Turkey', 'Italy', 'Wales', 'Switzerland', 'Denmark', 'Finland', 'Belgium', 'Russia', 'Netherlands', 'Ukraine', 'Austria', 'North Macedonia', 'England', 'Croatia', 'Scotland', 'Czech Republic']]
        elif id == 44:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['England', 'Croatia', 'Scotland', 'Czech Republic']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Hungary', 'Portugal', 'France', 'Germany']]
        elif id == 40:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Netherlands', 'Ukraine', 'Austria', 'North Macedonia']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['England', 'Croatia', 'Scotland', 'Czech Republic', 'Spain', 'Sweden', 'Poland', 'Slovakia', 'Hungary', 'Portugal', 'France', 'Germany']]
        elif id == 38:
            if pos == 0:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Turkey', 'Italy', 'Wales', 'Switzerland']]
            else:
                return [KnockOutPhase.tournament_teams[team_name] for team_name in ['Denmark', 'Finland', 'Belgium', 'Russia']]
        elif id == 46:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(39, 0) + KnockOutPhase.get_available_teams_at(39, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(37, 0) + KnockOutPhase.get_available_teams_at(37, 1)))
        elif id == 45:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(41, 0) + KnockOutPhase.get_available_teams_at(41, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(42, 0) + KnockOutPhase.get_available_teams_at(42, 1)))
        elif id == 48:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(43, 0) + KnockOutPhase.get_available_teams_at(43, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(44, 0) + KnockOutPhase.get_available_teams_at(44, 1)))
        elif id == 47:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(40, 0) + KnockOutPhase.get_available_teams_at(40, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(38, 0) + KnockOutPhase.get_available_teams_at(38, 1)))
        elif id == 49:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(46, 0) + KnockOutPhase.get_available_teams_at(46, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(45, 0) + KnockOutPhase.get_available_teams_at(45, 1)))
        elif id == 50:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(48, 0) + KnockOutPhase.get_available_teams_at(48, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(47, 0) + KnockOutPhase.get_available_teams_at(47, 1)))
        elif id == 51:
            if pos == 0:
                return list(
                    set(KnockOutPhase.get_available_teams_at(49, 0) + KnockOutPhase.get_available_teams_at(49, 1)))
            else:
                return list(
                    set(KnockOutPhase.get_available_teams_at(50, 0) + KnockOutPhase.get_available_teams_at(50, 1)))

    @classmethod
    def init_available_teams(cls):
        for m_t in KnockOutPhase.match_teams:
            if m_t.id in list(range(37, 45)):
                m_t.all_available_teams = tuple(KnockOutPhase.get_available_teams_at(m_t.id, m_t.position))
                m_t.available_teams = list(m_t.all_available_teams)
            else:
                m_t.available_teams = list(set(m_t.get_parents()[0].available_teams + m_t.get_parents()[1].available_teams))

    @classmethod
    def clear_pins(cls):
        for pin in KnockOutPhase.pinned[:]:
            KnockOutPhase.unpin_team(pin[0], pin[1])

    @classmethod
    def clear_pins_besides_round_of_16(cls):
        for pin in KnockOutPhase.pinned[:]:
            if pin[0] > 44:
                KnockOutPhase.unpin_team(pin[0], pin[1])
        KnockOutPhase.recalc_pinned_match_teams()

    @classmethod
    def clear_pins_from_id_pos(cls, id, pos):
        match_team = KnockOutPhase.get_match_team_by_id_pos(id, pos)
        changes = False
        way = match_team.get_all_children()
        for m_t in way:
            if (m_t.id, m_t.position) in KnockOutPhase.pinned:
                changes = True
                KnockOutPhase.unpin_team(m_t.id, m_t.position)
        if (id, pos) in KnockOutPhase.pinned:
            changes = True
            KnockOutPhase.unpin_team(id, pos)
        if changes:
            KnockOutPhase.recalc_pinned_match_teams()

    @classmethod
    def pin_team(cls, id, pos):
        KnockOutPhase.pinned.append((id, pos))
        KnockOutPhase.bg.pins[(id, pos)][1] = 1

    @classmethod
    def unpin_team(cls, id, pos):
        KnockOutPhase.pinned.remove((id, pos))
        KnockOutPhase.bg.pins[(id, pos)][1] = 0

    @classmethod
    def recalc_pinned_match_teams(cls):

        def remove_through(match_teams_lst, team):
            for m_t in match_teams_lst:
                if team in m_t.available_teams:
                    m_t.available_teams.remove(team)

        match_teams_of_thirds = [KnockOutPhase.get_match_team_by_id_pos(39, 1),
                                 KnockOutPhase.get_match_team_by_id_pos(41, 1),
                                 KnockOutPhase.get_match_team_by_id_pos(43, 1),
                                 KnockOutPhase.get_match_team_by_id_pos(40, 1)]

        def thirds_remove_through(team, id):
            groups_4_letters = KnockOutPhase.find_all_4_thirds_group_letters(team, id)
            available_groups_39 = KnockOutPhase.get_available_groups_list_for_thirds_from_letters_list(groups_4_letters, 39)
            available_groups_41 = KnockOutPhase.get_available_groups_list_for_thirds_from_letters_list(groups_4_letters, 41)
            available_groups_43 = KnockOutPhase.get_available_groups_list_for_thirds_from_letters_list(groups_4_letters, 43)
            available_groups_40 = KnockOutPhase.get_available_groups_list_for_thirds_from_letters_list(groups_4_letters, 40)
            for t_m in match_teams_of_thirds[0].available_teams[:]:
                if t_m.group.name not in available_groups_39:
                    match_teams_of_thirds[0].available_teams.remove(t_m)
            for t_m in match_teams_of_thirds[1].available_teams[:]:
                if t_m.group.name not in available_groups_41:
                    match_teams_of_thirds[1].available_teams.remove(t_m)
            for t_m in match_teams_of_thirds[2].available_teams[:]:
                if t_m.group.name not in available_groups_43:
                    match_teams_of_thirds[2].available_teams.remove(t_m)
            for t_m in match_teams_of_thirds[3].available_teams[:]:
                if t_m.group.name not in available_groups_40:
                    match_teams_of_thirds[3].available_teams.remove(t_m)

        round0 = [m_t for m_t in  KnockOutPhase.match_teams if m_t.id in range(37, 45)]  # round of 16
        round1 = [m_t for m_t in  KnockOutPhase.match_teams if m_t.id in range(45, 49)]  # quarter-finals
        round2 = [m_t for m_t in  KnockOutPhase.match_teams if m_t.id in [49, 50]]  # semi-finals
        round3 = [KnockOutPhase.get_match_team_by_id_pos(51, 0), KnockOutPhase.get_match_team_by_id_pos(51, 1)]  # final
        # round of 16
        # 39 37 41 42   43 44 40 38
        for match_team in round0:
            match_team.available_teams = list(match_team.all_available_teams)
        for match_team in round0:
            if match_team.is_pinned:
                if match_team in match_teams_of_thirds:
                    thirds_remove_through(match_team.team, match_team.id)
                remove_through(round0, match_team.team)
                match_team.available_teams = [match_team.team]
        # quarter-finals
        # 46 45  48 47
        for match_team in round1:
            match_team_parents = match_team.get_parents()
            match_team.available_teams = list(
                set(match_team_parents[0].available_teams + match_team_parents[1].available_teams))
        for match_team in round1:
            if match_team.is_pinned:
                parent1, parent2 = match_team.get_parents()
                if match_team.team in parent1.available_teams:
                    # 39 41  43 40
                    if parent1 in match_teams_of_thirds:
                        thirds_remove_through(match_team.team, parent1.id)
                    remove_through(round0, match_team.team)
                    remove_through(round1, match_team.team)
                    parent1.available_teams = [match_team.team]
                    match_team.available_teams = [match_team.team]
                else:
                    # 37 42  44 38
                    if parent2 in match_teams_of_thirds:
                        thirds_remove_through(match_team.team, parent2.id)
                    remove_through(round0, match_team.team)
                    remove_through(round1, match_team.team)
                    parent2.available_teams = [match_team.team]
                    match_team.available_teams = [match_team.team]
        # semi-finals
        # 49 50
        for match_team in round2:
            match_team_parents = match_team.get_parents()
            match_team.available_teams = list(
                set(match_team_parents[0].available_teams + match_team_parents[1].available_teams))
        for match_team in round2:
            if match_team.is_pinned:
                parent1, parent2 = match_team.get_parents()
                if match_team.team in parent1.available_teams and match_team.team not in parent2.available_teams:
                    # 46  48
                    parent1_1, parent1_2 = parent1.get_parents()
                    if match_team.team in parent1_1.available_teams:
                        # 39  43
                        if parent1_1 in match_teams_of_thirds:
                            thirds_remove_through(match_team.team, parent1_1.id)
                        remove_through(round0, match_team.team)
                        remove_through(round1, match_team.team)
                        remove_through(round2, match_team.team)
                        parent1_1.available_teams = [match_team.team]
                        parent1.available_teams = [match_team.team]
                        match_team.available_teams = [match_team.team]
                    else:
                        # 37  44
                        if parent1_2 in match_teams_of_thirds:
                            thirds_remove_through(match_team.team, parent1_2.id)
                        remove_through(round0, match_team.team)
                        remove_through(round1, match_team.team)
                        remove_through(round2, match_team.team)
                        parent1_2.available_teams = [match_team.team]
                        parent1.available_teams = [match_team.team]
                        match_team.available_teams = [match_team.team]
                elif match_team.team in parent2.available_teams and match_team.team not in parent1.available_teams:
                    # 45  47
                    parent2_1, parent2_2 = parent2.get_parents()
                    if match_team.team in parent2_1.available_teams:
                        # 41  40
                        if parent2_1 in match_teams_of_thirds:
                            thirds_remove_through(match_team.team, parent2_1.id)
                        remove_through(round0, match_team.team)
                        remove_through(round1, match_team.team)
                        remove_through(round2, match_team.team)
                        parent2_1.available_teams = [match_team.team]
                        parent2.available_teams = [match_team.team]
                        match_team.available_teams = [match_team.team]
                    else:
                        # 42  38
                        if parent2_2 in match_teams_of_thirds:
                            thirds_remove_through(match_team.team, parent2_2.id)
                        remove_through(round0, match_team.team)
                        remove_through(round1, match_team.team)
                        remove_through(round2, match_team.team)
                        parent2_2.available_teams = [match_team.team]
                        parent2.available_teams = [match_team.team]
                        match_team.available_teams = [match_team.team]
                else:
                    # 45 46  48 47
                    # remove from opo's children and find the way to generate in the proper way
                    opo = match_team.get_opponent()
                    remove_through(opo.get_all_parents(), match_team.team)
                    if match_team.team in opo.available_teams:
                        opo.available_teams.remove(match_team.team)
                    # Remove from other semi-final branch
                    child_opo = opo.get_child().get_opponent()
                    remove_through(child_opo.get_all_parents(), match_team.team)
                    if match_team.team in child_opo.available_teams:
                        child_opo.available_teams.remove(match_team.team)
                    match_team.available_teams = [match_team.team]
        # final
        for match_team in round3:
            match_team_parents = match_team.get_parents()
            match_team.available_teams = list(
                set(match_team_parents[0].available_teams + match_team_parents[1].available_teams))
        for match_team in round3:
            if match_team.is_pinned:
                parent1, parent2 = match_team.get_parents()
                if match_team.team in parent1.available_teams and match_team.team not in parent2.available_teams:
                    parent1_1, parent1_2 = parent1.get_parents()
                    if match_team.team in parent1_1.available_teams and match_team.team not in parent1_2.available_teams:
                        parent1_1_1, parent1_1_2 = parent1_1.get_parents()
                        if match_team.team in parent1_1_1.available_teams:
                            if parent1_1_1 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent1_1_1.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent1_1_1.available_teams = [match_team.team]
                            parent1_1.available_teams = [match_team.team]
                            parent1.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                        else:
                            if parent1_1_2 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent1_1_2.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent1_1_2.available_teams = [match_team.team]
                            parent1_1.available_teams = [match_team.team]
                            parent1.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                    elif match_team.team in parent1_2.available_teams and match_team.team not in parent1_1.available_teams:
                        parent1_2_1, parent1_2_2 = parent1_2.get_parents()
                        if match_team.team in parent1_2_1.available_teams:
                            if parent1_2_1 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent1_2_1.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent1_2_1.available_teams = [match_team.team]
                            parent1_2.available_teams = [match_team.team]
                            parent1.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                        else:
                            if parent1_2_2 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent1_2_2.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent1_2_2.available_teams = [match_team.team]
                            parent1_2.available_teams = [match_team.team]
                            parent1.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                elif match_team.team in parent2.available_teams and match_team.team not in parent1.available_teams:
                    parent2_1, parent2_2 = parent2.get_parents()
                    if match_team.team in parent2_1.available_teams and match_team.team not in parent2_2.available_teams:
                        parent2_1_1, parent2_1_2 = parent2_1.get_parents()
                        if match_team.team in parent2_1_1.available_teams:
                            if parent2_1_1 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent2_1_1.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent2_1_1.available_teams = [match_team.team]
                            parent2_1.available_teams = [match_team.team]
                            parent2.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                        else:
                            if parent2_1_2 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent2_1_2.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent2_1_2.available_teams = [match_team.team]
                            parent2_1.available_teams = [match_team.team]
                            parent2.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                    elif match_team.team in parent2_2.available_teams and match_team.team not in parent2_1.available_teams:
                        parent2_2_1, parent2_2_2 = parent2_2.get_parents()
                        if match_team.team in parent2_2_1.available_teams:
                            if parent2_2_1 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent2_2_1.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent2_2_1.available_teams = [match_team.team]
                            parent2_2.available_teams = [match_team.team]
                            parent2.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                        else:
                            if parent2_2_2 in match_teams_of_thirds:
                                thirds_remove_through(match_team.team, parent2_2_2.id)
                            remove_through(round0, match_team.team)
                            remove_through(round1, match_team.team)
                            remove_through(round2, match_team.team)
                            parent2_2_2.available_teams = [match_team.team]
                            parent2_2.available_teams = [match_team.team]
                            parent2.available_teams = [match_team.team]
                            match_team.available_teams = [match_team.team]
                else:
                    opo = KnockOutPhase.get_match_team_by_id_pos(match_team.id, (match_team.position + 1) % 2)
                    remove_through(opo.get_all_parents(), match_team.team)
                    if match_team.team in opo.available_teams:
                        opo.available_teams.remove(match_team.team)

    @classmethod
    def choose_way_to_point(cls, team, id, pos, not_this_match_team=None):
        match_team = KnockOutPhase.get_match_team_by_id_pos(id, pos)
        round0 = [m for m in match_team.get_0_parents() if team in m.available_teams]
        for m_t in round0[:]:
            way_to_final = m_t.get_all_children()
            if not_this_match_team in way_to_final:
                round0.remove(m_t)
                break
            for m_t_wtf in way_to_final:
                if m_t_wtf.id < id and (m_t_wtf.id, m_t_wtf.position) in KnockOutPhase.pinned and m_t_wtf.team != team:
                    round0.remove(m_t)
                    break
        for m_t in round0[:]:
            if m_t.team == team:
                round0 = [m_t]
                break
        if len(round0) <= 1:
            way = round0[:]
        else:
            probs = []
            for m_t in round0:
                group_letter, place = m_t.get_place_from_groups()
                opo = m_t.get_opponent()
                if team.level < opo.team.level:
                    win_draw_lose = list(KnockOutPhase.dimensions_dict['match_win_draw_lose'][(opo.team.level, team.level)])
                    win_draw_lose.reverse()
                else:
                    win_draw_lose = KnockOutPhase.dimensions_dict['match_win_draw_lose'][(team.level, opo.team.level)]
                coef = 1
                if place == 2:
                    coef = 0.32
                probs.append(team.get_place_prob(place) * coef * (win_draw_lose[0] + 0.5 * win_draw_lose[1]))
            way = [random.choices(round0, probs)[0]]
        if way:
            child = way[0].get_child()
        else:
            return way
        while child != match_team and child is not None:
            way.append(child)
            child = child.get_child()
        return way

    @classmethod
    def switch_teams(cls, match_team1, team2, reccurent=''):

        def reorder_letters(word1, word2):
            # word1 - 6 letters, word4 - 4 letters, wordout - 6 letters
            wordout = ['_' for j in range(6)]
            for i in range(4):
                if word1[i] in word2:
                    wordout[i] = word1[i]
            i = 0
            while '_' in wordout:
                if wordout[i] == '_':
                    for let in word2:
                        if let not in wordout:
                            wordout[i] = let
                    for let in word1:
                        if let not in wordout and wordout[i] == '_':
                            wordout[i] = let
                i += 1
            return wordout
        # Pins backup
        pins_backup = []
        for pin in KnockOutPhase.pinned:
            m_t = KnockOutPhase.get_match_team_by_id_pos(pin[0], pin[1])
            pins_backup.append((pin, m_t, m_t.team))
        pins_backup.sort()

        team1 = match_team1.team
        match_team2 = None
        # The way new team2 will go to match_team1
        way = KnockOutPhase.choose_way_to_point(team2, match_team1.id, match_team1.position)
        if match_team1.id in range(37, 45):
            child0 = match_team1
        else:
            child0 = way[0]
        for m_t in KnockOutPhase.match_teams:
            if m_t.team == team2 and m_t.id < 45:
                match_team2 = m_t
        group1 = child0.team.group
        group2 = team2.group
        if match_team2 != None:
            available_groups_at_place, team2_group_place = match_team2.get_place_from_groups()
        else:
            if KnockOutPhase.get_drag_third_team_by_group_name(group2.name) == team2:
                team2_group_place = 2
            else:
                team2_group_place = 3
        team2_group_letter = group2.name[-1]
        chlid0_available_groups_at_place, child0_group_place = child0.get_place_from_groups()
        for pin_b in pins_backup:
            pin_way = KnockOutPhase.choose_way_to_point(pin_b[2], pin_b[0][0], pin_b[0][1])
            if match_team1 in pin_way and team2 != pin_b[2]:
                for p_w in pin_way:
                    if pin_b[2] in p_w.available_teams and pin_b[2] in p_w.get_opponent().available_teams:
                        KnockOutPhase.switch_teams(p_w.get_opponent(), pin_b[2], " recurrent")
        if group1 != group2:
            if team2_group_place == 1 or team2_group_place == 0:
                team2_child0 = KnockOutPhase.get_0_match_team_by_team(team2)
                old_third_team = KnockOutPhase.get_drag_third_by_group_name(group2.name).team
                old_letters_of_4 = [KnockOutPhase.get_match_team_by_id_pos(j, 1).team.group.name[-1] for j in
                                    [39, 41, 43, 40]]
                new_letters_of_4 = KnockOutPhase.find_best_4_new_thirds(old_letters_of_4, team2, child0.id)
                old_thirds_rating = KnockOutPhase.get_rating_of_drag_thirds()
                new_6_letters = reorder_letters([dr_th.group_name[-1] for dr_th in old_thirds_rating], new_letters_of_4)
                th_pl_lst_matches = [39, 41, 43, 40]
                for i, letter in enumerate(new_6_letters):
                    dr_th = KnockOutPhase.get_drag_third_by_group_name('Group ' + letter)
                    KnockOutPhase.thirds_rated_places[dr_th.team_name] = i
                    if letter == team2_group_letter:
                        dr_th.group_name = 'Group ' + letter
                        dr_th.team = team2
                        dr_th.team_name = team2.name
                        dr_th.set_text()
                    dr_th.set_place_position(i)
                    if letter in new_letters_of_4:
                        KnockOutPhase.get_match_team_by_id_pos(th_pl_lst_matches[new_letters_of_4.index(letter)],
                                                               1).set_match_team(dr_th.team)
                team2_child0.set_match_team(old_third_team)
                KnockOutPhase.clear_score_included(team2_child0.id)
            elif team2_group_place == 2:
                # castling of thirds
                old_letters_of_4 = [KnockOutPhase.get_match_team_by_id_pos(j, 1).team.group.name[-1] for j in [39, 41, 43, 40]]
                new_letters_of_4 = KnockOutPhase.find_best_4_new_thirds(old_letters_of_4, team2, child0.id)
                old_thirds_rating = KnockOutPhase.get_rating_of_drag_thirds()
                new_6_letters = reorder_letters([dr_th.group_name[-1] for dr_th in old_thirds_rating], new_letters_of_4)
                team2_child0 = KnockOutPhase.get_0_match_team_by_team(team2)
                th_pl_lst_matches = [39, 41, 43, 40]
                for i, letter in enumerate(new_6_letters):
                    dr_th = KnockOutPhase.get_drag_third_by_group_name('Group ' + letter)
                    KnockOutPhase.thirds_rated_places[dr_th.team_name] = i
                    dr_th.set_place_position(i)
                    if letter in new_letters_of_4:
                        KnockOutPhase.get_match_team_by_id_pos(th_pl_lst_matches[new_letters_of_4.index(letter)],
                                                               1).set_match_team(dr_th.team)
                        KnockOutPhase.clear_score_included(th_pl_lst_matches[new_letters_of_4.index(letter))
            elif team2_group_place == 3:
                team2_drag = KnockOutPhase.get_drag_third_by_group_name(group2.name)
                old_letters_of_4 = [KnockOutPhase.get_match_team_by_id_pos(j, 1).team.group.name[-1] for j in
                                    [39, 41, 43, 40]]
                new_letters_of_4 = KnockOutPhase.find_best_4_new_thirds(old_letters_of_4, team2, child0.id)
                odl_drag_third = {}
                lst_letters = []
                old_thirds_rating = KnockOutPhase.get_rating_of_drag_thirds()
                new_6_letters = reorder_letters([dr_th.group_name[-1] for dr_th in old_thirds_rating], new_letters_of_4)
                th_pl_lst_matches = [39, 41, 43, 40]
                for i, letter in enumerate(new_6_letters):
                    dr_th = KnockOutPhase.get_drag_third_by_group_name('Group ' + letter)
                    KnockOutPhase.thirds_rated_places[dr_th.team_name] = i
                    if letter == team2_group_letter:
                        dr_th.group_name = 'Group ' + letter
                        dr_th.team = team2
                        dr_th.team_name = team2.name
                        dr_th.set_text()
                    dr_th.set_place_position(i)
                    if letter in new_letters_of_4:
                        KnockOutPhase.get_match_team_by_id_pos(th_pl_lst_matches[new_letters_of_4.index(letter)],
                                                               1).set_match_team(dr_th.team)
        # end if group1 != group2:
        else:
            if team2_group_place != 2 and child0_group_place == 2:
                dr_th = KnockOutPhase.get_drag_third_by_group_name(group2.name)
                dr_th.team = team2
                dr_th.team_name = team2.name
                dr_th.set_text()
            elif team2_group_place == 2 and child0_group_place != 2:
                dr_th = KnockOutPhase.get_drag_third_by_group_name(group2.name)
                dr_th.team = child0.team
                dr_th.team_name = child0.team.name
                dr_th.set_text()
            elif team2_group_place != 2 and child0_group_place != 2:
                print('No thirds')
        if match_team2 == None:
            match_team1.set_match_team(team2)
            KnockOutPhase.find_winners_for(match_team1.id, match_team1.position, way, team2)
            KnockOutPhase.clear_score_included(match_team1.id)
        else:
            if child0.team == team2:
                KnockOutPhase.find_winners_for(match_team1.id, match_team1.position, way, team2)
                match_team1.set_match_team(team2)
                KnockOutPhase.clear_score_included(match_team1.id)
            else:
                match_team2.set_match_team(child0.team)
                match_team1.set_match_team(team2)
                KnockOutPhase.find_winners_for(match_team1.id, match_team1.position, way, team2)
                KnockOutPhase.clear_score_included(match_team1.id)
                KnockOutPhase.clear_score_included(match_team2.id)
        for pin_b in pins_backup:
            pin_way = KnockOutPhase.choose_way_to_point(pin_b[2], pin_b[0][0], pin_b[0][1])
            if match_team1 in pin_way and team2 != pin_b[2]:
                pin_way = KnockOutPhase.choose_way_to_point(pin_b[2], pin_b[0][0], pin_b[0][1], match_team1)
            elif match_team1 not in pin_way and team2 == pin_b[2]:
                pin_way = KnockOutPhase.choose_way_to_point(pin_b[2], pin_b[0][0], pin_b[0][1], match_team1.get_opponent())
            KnockOutPhase.find_winners_for(pin_b[0][0], pin_b[0][1], pin_way, pin_b[2])

    @classmethod
    def get_match_teams_and_drag_third_of_group(cls, group_name):
        lst = []
        for match_team in KnockOutPhase.match_teams:
            if match_team.id < 45 and match_team.team.group.name == group_name and \
                    match_team.get_place_from_groups()[0] == group_name[-1] and match_team.get_place_from_groups()[1] == 0:
                lst.insert(0, match_team)
            if match_team.id < 45 and match_team.team.group.name == group_name and \
                    match_team.get_place_from_groups()[0] == group_name[-1] and match_team.get_place_from_groups()[1] == 1:
                lst.append(match_team)
        lst.append(KnockOutPhase.get_drag_third_team_by_group_name(group_name))
        return lst

    @classmethod
    def set_winner(cls, team, id):
        if id == 39:
            KnockOutPhase.get_match_team_by_id_pos(46, 0).set_match_team(team)
        elif id == 37:
            KnockOutPhase.get_match_team_by_id_pos(46, 1).set_match_team(team)
        elif id == 41:
            KnockOutPhase.get_match_team_by_id_pos(45, 0).set_match_team(team)
        elif id == 42:
            KnockOutPhase.get_match_team_by_id_pos(45, 1).set_match_team(team)
        elif id == 43:
            KnockOutPhase.get_match_team_by_id_pos(48, 0).set_match_team(team)
        elif id == 44:
            KnockOutPhase.get_match_team_by_id_pos(48, 1).set_match_team(team)
        elif id == 40:
            KnockOutPhase.get_match_team_by_id_pos(47, 0).set_match_team(team)
        elif id == 38:
            KnockOutPhase.get_match_team_by_id_pos(47, 1).set_match_team(team)
        elif id == 46:
            KnockOutPhase.get_match_team_by_id_pos(49, 0).set_match_team(team)
        elif id == 45:
            KnockOutPhase.get_match_team_by_id_pos(49, 1).set_match_team(team)
        elif id == 48:
            KnockOutPhase.get_match_team_by_id_pos(50, 0).set_match_team(team)
        elif id == 47:
            KnockOutPhase.get_match_team_by_id_pos(50, 1).set_match_team(team)
        elif id == 49:
            KnockOutPhase.get_match_team_by_id_pos(51, 0).set_match_team(team)
        elif id == 50:
            KnockOutPhase.get_match_team_by_id_pos(51, 1).set_match_team(team)

    @classmethod
    def find_winners_for(cls, id, pos, way, child_win=None):
        # To fill match wins loses through the way
        def pdf(x, mu, sigma):
            # probability_density_function
            return 1 / (sigma * math.sqrt(2 * math.pi)) * math.exp(-0.5 * ((x - mu) / sigma) ** 2)
        match_team = KnockOutPhase.get_match_team_by_id_pos(id, pos)
        if id < 45:
            return
        if child_win is not None:
            # The way new team2 will go to match_team1
            for m_t in way:
                if m_t.team != child_win:
                    m_t.set_match_team(child_win)
                    KnockOutPhase.clear_score_included(m_t.id)
        parent1, parent2 = match_team.get_parents()
        if parent2.team is None:
            KnockOutPhase.find_winners_for(parent2.id, parent2.position, way, None)
        elif parent2.team is child_win and parent2 in way:
            KnockOutPhase.find_winners_for(parent2.id, parent2.position, way, child_win)
        if parent1.team is None:
            KnockOutPhase.find_winners_for(parent1.id, parent1.position, way, None)
        elif parent1.team is child_win and parent1 in way:
            KnockOutPhase.find_winners_for(parent1.id, parent1.position, way, child_win)
        if parent1.team is not None and parent2.team is not None:
            team1 = parent1.team
            team2 = parent2.team
            a_d_1 = team1.attack - team2.defence
            a_d_2 = team2.attack - team1.defence
            mu1 = (a_d_1 + 10) / 8
            mu2 = (a_d_2 + 10) / 6
            sigma1 = 2 ** ((mu1 - 1) / 2)
            sigma2 = 2 ** ((mu2 - 1) / 2)
            if child_win == team1:
                w_d_l = 'w'
            elif child_win == team2:
                w_d_l = 'l'
            elif child_win == None:
                if team1.level < team2.level:
                    win_draw_lose = list(KnockOutPhase.dimensions_dict['match_win_draw_lose'][(team2.level, team1.level)])
                    win_draw_lose.reverse()
                else:
                    win_draw_lose = KnockOutPhase.dimensions_dict['match_win_draw_lose'][(team1.level, team2.level)]
                w_d_l = random.choices('wdl', win_draw_lose)[0]
            else:
                print(f'1015___WHY???? match_team: {match_team}, child_win: {child_win}, parents: ({team1}, {team2})')
            if w_d_l == 'w':
                winner = team1
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(7)), [pdf(i, mu2, sigma2) for i in range(7)])[0]
                    team1_score = max(team2_score, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                else:
                    team1_score = max(1, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                    team2_score = min(team1_score, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
            elif w_d_l == 'l':
                winner = team2
                if team1.level >= team2.level:
                    team2_score = max(1, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
                    team1_score = min(team2_score, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                else:
                    team1_score = random.choices(list(range(7)), [pdf(i, mu1, sigma1) for i in range(7)])[0]
                    team2_score = max(team1_score, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
            else:
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0]
                    team1_score = team2_score
                else:
                    team1_score = random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0]
                    team2_score = team1_score
            if w_d_l == 'w' and team1_score == team2_score:
                team1_score = str(team1_score) + '*'
            elif w_d_l == 'l' and team1_score == team2_score:
                team2_score = str(team2_score) + '*'
            elif w_d_l == 'd':
                rnd = random.randint(0, 1)
                if rnd == 0:
                    winner = team1
                    team1_score = str(team1_score) + '*'
                else:
                    winner = team2
                    team2_score = str(team2_score) + '*'
            KnockOutPhase.get_match_team_by_id_pos(parent1.id, 0).score = team1_score
            KnockOutPhase.get_match_team_by_id_pos(parent1.id, 1).score = team2_score
            KnockOutPhase.set_winner(winner, parent1.id)

    @classmethod
    def clear_score(cls, id):
        if id == 39:
            KnockOutPhase.set_winner(None, 46)
            KnockOutPhase.clear_score(46)
            KnockOutPhase.get_match_team_by_id_pos(46, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 1).score = ''
        elif id == 37:
            KnockOutPhase.set_winner(None, 46)
            KnockOutPhase.clear_score(46)
            KnockOutPhase.get_match_team_by_id_pos(46, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 1).score = ''
        elif id == 41:
            KnockOutPhase.set_winner(None, 45)
            KnockOutPhase.clear_score(45)
            KnockOutPhase.get_match_team_by_id_pos(45, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 1).score = ''
        elif id == 42:
            KnockOutPhase.set_winner(None, 45)
            KnockOutPhase.clear_score(45)
            KnockOutPhase.get_match_team_by_id_pos(45, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 1).score = ''
        elif id == 43:
            KnockOutPhase.set_winner(None, 48)
            KnockOutPhase.clear_score(48)
            KnockOutPhase.get_match_team_by_id_pos(48, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 1).score = ''
        elif id == 44:
            KnockOutPhase.set_winner(None, 48)
            KnockOutPhase.clear_score(48)
            KnockOutPhase.get_match_team_by_id_pos(48, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 1).score = ''
        elif id == 40:
            KnockOutPhase.set_winner(None, 47)
            KnockOutPhase.clear_score(47)
            KnockOutPhase.get_match_team_by_id_pos(47, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 1).score = ''
        elif id == 38:
            KnockOutPhase.set_winner(None, 47)
            KnockOutPhase.clear_score(47)
            KnockOutPhase.get_match_team_by_id_pos(47, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 1).score = ''
        elif id == 46:
            KnockOutPhase.set_winner(None, 49)
            KnockOutPhase.clear_score(49)
            KnockOutPhase.get_match_team_by_id_pos(49, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 1).score = ''
        elif id == 45:
            KnockOutPhase.set_winner(None, 49)
            KnockOutPhase.clear_score(49)
            KnockOutPhase.get_match_team_by_id_pos(49, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 1).score = ''
        elif id == 48:
            KnockOutPhase.set_winner(None, 50)
            KnockOutPhase.clear_score(50)
            KnockOutPhase.get_match_team_by_id_pos(50, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 1).score = ''
        elif id == 47:
            KnockOutPhase.set_winner(None, 50)
            KnockOutPhase.clear_score(50)
            KnockOutPhase.get_match_team_by_id_pos(50, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 1).score = ''
        elif id == 49:
            KnockOutPhase.get_match_team_by_id_pos(51, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(51, 1).score = ''
        elif id == 50:
            KnockOutPhase.get_match_team_by_id_pos(51, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(51, 1).score = ''

    @classmethod
    def clear_score_included(cls, id):
        if id == 39:
            KnockOutPhase.set_winner(None, 46)
            KnockOutPhase.clear_score(46)
            KnockOutPhase.get_match_team_by_id_pos(46, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(39, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(39, 1).score = ''
            KnockOutPhase.set_winner(None, 39)
        elif id == 37:
            KnockOutPhase.set_winner(None, 46)
            KnockOutPhase.clear_score(46)
            KnockOutPhase.get_match_team_by_id_pos(46, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(37, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(37, 1).score = ''
            KnockOutPhase.set_winner(None, 37)
        elif id == 41:
            KnockOutPhase.set_winner(None, 45)
            KnockOutPhase.clear_score(45)
            KnockOutPhase.get_match_team_by_id_pos(45, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(41, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(41, 1).score = ''
            KnockOutPhase.set_winner(None, 41)
        elif id == 42:
            KnockOutPhase.set_winner(None, 45)
            KnockOutPhase.clear_score(45)
            KnockOutPhase.get_match_team_by_id_pos(45, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(42, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(42, 1).score = ''
            KnockOutPhase.set_winner(None, 42)
        elif id == 43:
            KnockOutPhase.set_winner(None, 48)
            KnockOutPhase.clear_score(48)
            KnockOutPhase.get_match_team_by_id_pos(48, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(43, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(43, 1).score = ''
            KnockOutPhase.set_winner(None, 43)
        elif id == 44:
            KnockOutPhase.set_winner(None, 48)
            KnockOutPhase.clear_score(48)
            KnockOutPhase.get_match_team_by_id_pos(48, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(44, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(44, 1).score = ''
            KnockOutPhase.set_winner(None, 44)
        elif id == 40:
            KnockOutPhase.set_winner(None, 47)
            KnockOutPhase.clear_score(47)
            KnockOutPhase.get_match_team_by_id_pos(47, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(40, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(40, 1).score = ''
            KnockOutPhase.set_winner(None, 40)
        elif id == 38:
            KnockOutPhase.set_winner(None, 47)
            KnockOutPhase.clear_score(47)
            KnockOutPhase.get_match_team_by_id_pos(47, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(38, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(38, 1).score = ''
            KnockOutPhase.set_winner(None, 38)
        elif id == 46:
            KnockOutPhase.set_winner(None, 49)
            KnockOutPhase.clear_score(49)
            KnockOutPhase.get_match_team_by_id_pos(49, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(46, 1).score = ''
            KnockOutPhase.set_winner(None, 46)
        elif id == 45:
            KnockOutPhase.set_winner(None, 49)
            KnockOutPhase.clear_score(49)
            KnockOutPhase.get_match_team_by_id_pos(49, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(45, 1).score = ''
            KnockOutPhase.set_winner(None, 45)
        elif id == 48:
            KnockOutPhase.set_winner(None, 50)
            KnockOutPhase.clear_score(50)
            KnockOutPhase.get_match_team_by_id_pos(50, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(48, 1).score = ''
            KnockOutPhase.set_winner(None, 48)
        elif id == 47:
            KnockOutPhase.set_winner(None, 50)
            KnockOutPhase.clear_score(50)
            KnockOutPhase.get_match_team_by_id_pos(50, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(47, 1).score = ''
            KnockOutPhase.set_winner(None, 47)
        elif id == 49:
            KnockOutPhase.get_match_team_by_id_pos(51, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(51, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(49, 1).score = ''
            KnockOutPhase.set_winner(None, 49)
        elif id == 50:
            KnockOutPhase.get_match_team_by_id_pos(51, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(51, 1).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(50, 1).score = ''
            KnockOutPhase.set_winner(None, 50)

    @classmethod
    def clear_1_16_match(cls):
        for id in range(37, 45):
            KnockOutPhase.get_match_team_by_id_pos(id, 0).score = ''
            KnockOutPhase.get_match_team_by_id_pos(id, 1).score = ''
            KnockOutPhase.clear_score(id)
        for id in range(45, 49):
            KnockOutPhase.get_match_team_by_id_pos(id, 0).set_match_team(None)
            KnockOutPhase.get_match_team_by_id_pos(id, 1).set_match_team(None)

    @classmethod
    def get_match_team_by_id_pos(cls, id, pos):
        for match_team in cls.match_teams:
            if match_team.id == id and match_team.position == pos:
                return match_team
        return None

    @classmethod
    def get_0_match_team_by_team(cls, team):
        for match_team in cls.match_teams:
            if match_team.team == team:
                return match_team.get_0_parent()
        return None

    @classmethod
    def generate_matches(cls):
        def pdf(x, mu, sigma):
            # probability_density_function
            return 1 / (sigma * math.sqrt(2 * math.pi)) * math.exp(-0.5 * ((x - mu) / sigma) ** 2)
        # Backup All available teams
        backup_available_teams = {}
        for match_team in KnockOutPhase.match_teams:
            backup_available_teams[(match_team.id, match_team.position)] = match_team.available_teams
        for pin in KnockOutPhase.pinned:
            pinned_team = KnockOutPhase.get_match_team_by_id_pos(pin[0], pin[1])
            m_t_pinned = KnockOutPhase.choose_way_to_point(pinned_team.team, pin[0], pin[1])
            for m_t in m_t_pinned:
                m_t.available_teams = [pinned_team.team]
        for id in range(37, 52):
            match_team1 = KnockOutPhase.get_match_team_by_id_pos(id, 0)
            match_team2 = KnockOutPhase.get_match_team_by_id_pos(id, 1)
            team1 = match_team1.team
            team2 = match_team2.team
            a_d_1 = team1.attack - team2.defence
            a_d_2 = team2.attack - team1.defence
            mu1 = (a_d_1 + 10) / 8
            mu2 = (a_d_2 + 10) / 6
            sigma1 = 2 ** ((mu1 - 1) / 2)
            sigma2 = 2 ** ((mu2 - 1) / 2)
            child = match_team1.get_child()
            w_d_l_res = ''
            if child is not None and (child.is_pinned or len(child.available_teams) == 1):
                if child.team == team1:
                    w_d_l_res = 'w'
                else:
                    w_d_l_res = 'l'
            if team1.level < team2.level:
                win_draw_lose = list(KnockOutPhase.dimensions_dict['match_win_draw_lose'][(team2.level, team1.level)])
                win_draw_lose.reverse()
            else:
                win_draw_lose = KnockOutPhase.dimensions_dict['match_win_draw_lose'][(team1.level, team2.level)]
            w_d_l = random.choices('wdl', win_draw_lose)[0]
            while w_d_l_res != '' and (w_d_l != w_d_l_res and w_d_l != 'd'):
                w_d_l = random.choices('wdl', win_draw_lose)[0]
            if w_d_l == 'w':
                winner = team1
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(7)), [pdf(i, mu2, sigma2) for i in range(7)])[0]
                    team1_score = max(team2_score + 1, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                else:
                    team1_score = max(1, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                    team2_score = min(team1_score - 1, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
            elif w_d_l == 'l':
                winner = team2
                if team1.level >= team2.level:
                    team2_score = max(1, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
                    team1_score = min(team2_score - 1, random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0])
                else:
                    team1_score = random.choices(list(range(7)), [pdf(i, mu1, sigma1) for i in range(7)])[0]
                    team2_score = max(team1_score + 1, random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0])
            else:
                if team1.level >= team2.level:
                    team2_score = random.choices(list(range(8)), [pdf(i, mu2, sigma2) for i in range(8)])[0]
                    team1_score = team2_score
                else:
                    team1_score = random.choices(list(range(8)), [pdf(i, mu1, sigma1) for i in range(8)])[0]
                    team2_score = team1_score
            if w_d_l == 'd':
                if w_d_l_res == 'w':
                    team1_score = str(team1_score) + '*'
                    winner = team1
                elif w_d_l_res == 'l':
                    team2_score = str(team2_score) + '*'
                    winner = team2
                else:
                    rnd = random.randint(0, 1)
                    if rnd == 0:
                        winner = team1
                        team1_score = str(team1_score) + '*'
                    else:
                        winner = team2
                        team2_score = str(team2_score) + '*'
            KnockOutPhase.get_match_team_by_id_pos(id, 0).score = team1_score
            KnockOutPhase.get_match_team_by_id_pos(id, 1).score = team2_score
            KnockOutPhase.set_winner(winner, id)
        # Restore Backup ALL available teams
        for ba_team in backup_available_teams:
            m_t = KnockOutPhase.get_match_team_by_id_pos(ba_team[0], ba_team[1])
            m_t.available_teams = backup_available_teams[ba_team]


    @classmethod
    def generate_unpinned_group_teams(cls):
        stay_same_list = []
        thirds4 = []
        pins_backup = []
        # Backup pins and define ways for all pins
        for pin in KnockOutPhase.pinned:
            pinned_match_team = KnockOutPhase.get_match_team_by_id_pos(pin[0], pin[1])
            pined_team = pinned_match_team.team
            pins_backup.append((pin, pined_team))
            if pin[0] > 44:
                round0 = [m for m in pinned_match_team.get_0_parents() if pined_team in m.available_teams]
                if len(round0) == 1:
                    way = round0[:]
                elif len(round0) == 0:
                    print('1362___error in generate_unpinned_group_teams')
                else:
                    probs = []
                    for m_t in round0:
                        group_letter, place = m_t.get_place_from_groups()
                        opo = m_t.get_opponent()
                        if pined_team.level < opo.team.level:
                            win_draw_lose = list(
                                KnockOutPhase.dimensions_dict['match_win_draw_lose'][(opo.team.level, pined_team.level)])
                            win_draw_lose.reverse()
                        else:
                            win_draw_lose = KnockOutPhase.dimensions_dict['match_win_draw_lose'][
                                (pined_team.level, opo.team.level)]
                        coef = 1
                        if place == 2:
                            coef = 0.32
                        probs.append(pined_team.get_place_prob(place) * coef * (win_draw_lose[0] + 0.5 * win_draw_lose[1]))
                    way = [random.choices(round0, probs)[0]]
                child0 = way[0]
            else:
                child0 = pinned_match_team
            group_letter, place = child0.get_place_from_groups()
            stay_same_list.append((child0, group_letter, place, pined_team))
            if place == 2 and (child0, group_letter, place, pined_team) not in thirds4:
                thirds4.append((child0, group_letter, place, pined_team))
        pins_backup.sort()
        # end backup pins
        stay_same_teams_list = [m_t[3] for m_t in stay_same_list]
        some_team = KnockOutPhase.get_match_team_by_id_pos(39, 0).team
        all_groups_dict = some_team.group.all_groups
        groups_stay_same_dict = {}
        for group_name in all_groups_dict:
            groups_stay_same_dict[group_name] = []
            for stay_same in stay_same_list:
                if stay_same[3].group.name == group_name:
                    groups_stay_same_dict[group_name].append((stay_same[3].name, stay_same[2]))
        new_group_rankings = {}
        for group_name in all_groups_dict:
            available_group_rankings = []
            probs_for_available_group_rankings = []
            # Generate group rankings
            for rnkgs_probs in KnockOutPhase.group_rankings_probs[group_name]:
                flag = True
                for s_s_team in groups_stay_same_dict[group_name]:
                    if rnkgs_probs[0].index(s_s_team[0]) != s_s_team[1]:
                        flag = False
                        break
                if flag:
                    available_group_rankings.append(rnkgs_probs[0])
                    probs_for_available_group_rankings.append(rnkgs_probs[1])
            rankings = random.choices(available_group_rankings, probs_for_available_group_rankings)[0]
            new_group_rankings[group_name] = rankings
        # Generate 4 thirds
        if len(thirds4) > 0:
            available_groups_for_thirds = KnockOutPhase.find_all_4_thirds_group_letters(thirds4[0][3], thirds4[0][0].id)
            ordered_4thirds_letters = [KnockOutPhase.convert_from_sorted_to_ordered_4_thirds(thds) for thds in available_groups_for_thirds]
            lst = [39, 41, 43, 40]
            for let in ordered_4thirds_letters[:]:
                for i in range(1, len(thirds4)):
                    if let[lst.index(thirds4[i][0].id)] != thirds4[i][3].group.name[-1]:
                        ordered_4thirds_letters.remove(let)
                        break
            new_4thirds_letters = random.choice(ordered_4thirds_letters)
        else:
            dlst = ['ACBD', 'ACBE', 'ACBF', 'DBAE', 'DBAF', 'EABF', 'EACD', 'FACD', 'EACF', 'EADF', 'ECBD', 'FBCD',
                    'FBCE', 'FBDE', 'FCDE']
            new_4thirds_letters = random.choice(dlst)
        # Replace drag_thirds
        letters2 = list(set(random.choice(list(itertools.permutations('ABCDEF')))).difference(set(new_4thirds_letters)))
        tmp_lst = [(new_group_rankings['Group ' + new_4thirds_letters[i]][2], 'Group ' + new_4thirds_letters[i],
                KnockOutPhase.tournament_teams[new_group_rankings['Group ' + new_4thirds_letters[i]][2]]) for i in
               range(4)]
        tmp_lst.append((new_group_rankings['Group ' + letters2[0]][2], 'Group ' + letters2[0],
                    KnockOutPhase.tournament_teams[new_group_rankings['Group ' + letters2[0]][2]]))
        tmp_lst.append((new_group_rankings['Group ' + letters2[1]][2], 'Group ' + letters2[1],
                    KnockOutPhase.tournament_teams[new_group_rankings['Group ' + letters2[1]][2]]))
        KnockOutPhase.ranking_of_thirds = tmp_lst
        KnockOutPhase.reload_ranking_of_thirds()
        # Apply group rankings
        KnockOutPhase.get_match_team_by_id_pos(39, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group ' + new_4thirds_letters[0]][2]])
        KnockOutPhase.get_match_team_by_id_pos(41, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group ' + new_4thirds_letters[1]][2]])
        KnockOutPhase.get_match_team_by_id_pos(43, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group ' + new_4thirds_letters[2]][2]])
        KnockOutPhase.get_match_team_by_id_pos(40, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group ' + new_4thirds_letters[3]][2]])
        KnockOutPhase.get_match_team_by_id_pos(39, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group B'][0]])
        KnockOutPhase.get_match_team_by_id_pos(37, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group A'][0]])
        KnockOutPhase.get_match_team_by_id_pos(37, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group C'][1]])
        KnockOutPhase.get_match_team_by_id_pos(41, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group F'][0]])
        KnockOutPhase.get_match_team_by_id_pos(42, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group D'][1]])
        KnockOutPhase.get_match_team_by_id_pos(42, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group E'][1]])
        KnockOutPhase.get_match_team_by_id_pos(43, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group E'][0]])
        KnockOutPhase.get_match_team_by_id_pos(44, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group D'][0]])
        KnockOutPhase.get_match_team_by_id_pos(44, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group F'][1]])
        KnockOutPhase.get_match_team_by_id_pos(40, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group C'][0]])
        KnockOutPhase.get_match_team_by_id_pos(38, 0).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group A'][1]])
        KnockOutPhase.get_match_team_by_id_pos(38, 1).set_match_team(KnockOutPhase.tournament_teams[new_group_rankings['Group B'][1]])
        # Clear scores
        for match_team in KnockOutPhase.match_teams:
           if match_team.team not in stay_same_teams_list:
               KnockOutPhase.clear_score_included(match_team.id)
        for pin_bp in pins_backup:
            pin = pin_bp[0]
            pinned_team = pin_bp[1]
            pin_way = KnockOutPhase.choose_way_to_point(pinned_team, pin[0], pin[1])
            KnockOutPhase.find_winners_for(pin[0], pin[1], pin_way, pinned_team)

    @classmethod
    def init_all_matches(self):
        rect_height = KnockOutPhase.dimensions_dict['rect_height']
        rect_width = KnockOutPhase.dimensions_dict['rect_width']
        cell = rect_height / 2
        final_coef = KnockOutPhase.dimensions_dict['final_coef']
        column_1_1_y = KnockOutPhase.dimensions_dict['column_1_1_y']
        column_1_2_y = KnockOutPhase.dimensions_dict['column_1_2_y']
        column_1_3_y = KnockOutPhase.dimensions_dict['column_1_3_y']
        column_1_4_y = KnockOutPhase.dimensions_dict['column_1_4_y']
        column_2_1_y = KnockOutPhase.dimensions_dict['column_2_1_y']
        column_2_2_y = KnockOutPhase.dimensions_dict['column_2_2_y']
        column_2_3_y = KnockOutPhase.dimensions_dict['column_2_3_y']
        column_2_4_y = KnockOutPhase.dimensions_dict['column_2_4_y']
        column_4_1_y = KnockOutPhase.dimensions_dict['column_4_1_y']
        column_4_2_y = KnockOutPhase.dimensions_dict['column_4_2_y']
        column_5_y = KnockOutPhase.dimensions_dict['column_5_y']
        column_1_x = KnockOutPhase.dimensions_dict['column_1_x']
        column_2_x = KnockOutPhase.dimensions_dict['column_2_x']
        column_3_x = KnockOutPhase.dimensions_dict['column_3_x']
        column_4_x = KnockOutPhase.dimensions_dict['column_4_x']
        column_5_x = KnockOutPhase.dimensions_dict['column_5_x']
        matches = []
        matches.append(MatchTeam('None', 39, 0, '1_1_1', self.none_image, column_1_x, column_1_1_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 39, 1, '1_1_2', self.none_image, column_1_x, column_1_1_y, cell, ''))
        matches.append(MatchTeam('None', 42, 0, '1_2_1', self.none_image, column_1_x, column_1_2_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 42, 1, '1_2_2', self.none_image, column_1_x, column_1_2_y, cell, ''))
        matches.append(MatchTeam('None', 43, 0, '1_3_1', self.none_image, column_1_x, column_1_3_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 43, 1, '1_3_2', self.none_image, column_1_x, column_1_3_y, cell, ''))
        matches.append(MatchTeam('None', 38, 0, '1_4_1', self.none_image, column_1_x, column_1_4_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 38, 1, '1_4_2', self.none_image, column_1_x, column_1_4_y, cell, ''))
        matches.append(MatchTeam('None', 37, 0, '2_1_1', self.none_image, column_2_x, column_2_1_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 37, 1, '2_1_2', self.none_image, column_2_x, column_2_1_y, cell, ''))
        matches.append(MatchTeam('None', 41, 0, '2_2_1', self.none_image, column_2_x, column_2_2_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 41, 1, '2_2_2', self.none_image, column_2_x, column_2_2_y, cell, ''))
        matches.append(MatchTeam('None', 44, 0, '2_3_1', self.none_image, column_2_x, column_2_3_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 44, 1, '2_3_2', self.none_image, column_2_x, column_2_3_y, cell, ''))
        matches.append(MatchTeam('None', 40, 0, '2_4_1', self.none_image, column_2_x, column_2_4_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 40, 1, '2_4_2', self.none_image, column_2_x, column_2_4_y, cell, ''))
        matches.append(MatchTeam('None', 46, 0, '3_1_1', self.none_image, column_3_x, column_1_1_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 46, 1, '3_1_2', self.none_image, column_3_x, column_1_1_y, cell, ''))
        matches.append(MatchTeam('None', 45, 0, '3_2_1', self.none_image, column_3_x, column_1_2_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 45, 1, '3_2_2', self.none_image, column_3_x, column_1_2_y, cell, ''))
        matches.append(MatchTeam('None', 48, 0, '3_3_1', self.none_image, column_3_x, column_1_3_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 48, 1, '3_3_2', self.none_image, column_3_x, column_1_3_y, cell, ''))
        matches.append(MatchTeam('None', 47, 0, '3_4_1', self.none_image, column_3_x, column_1_4_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 47, 1, '3_4_2', self.none_image, column_3_x, column_1_4_y, cell, ''))
        matches.append(MatchTeam('None', 49, 0, '4_1_1', self.none_image, column_4_x, column_4_1_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 49, 1, '4_1_2', self.none_image, column_4_x, column_4_1_y, cell, ''))
        matches.append(MatchTeam('None', 50, 0, '4_2_1', self.none_image, column_4_x, column_4_2_y + rect_height / 2, cell, ''))
        matches.append(MatchTeam('None', 50, 1, '4_2_2', self.none_image, column_4_x, column_4_2_y, cell, ''))
        matches.append(MatchTeam('None', 51, 0, '5_1', self.none_image, column_5_x, column_5_y + final_coef * rect_height / 2, cell * final_coef, ''))
        matches.append(MatchTeam('None', 51, 1, '5_2', self.none_image, column_5_x, column_5_y, cell * final_coef, ''))
        KnockOutPhase.match_teams = matches

    @classmethod
    def get_drag_third_team_by_group_name(cls, group_name):
        for d_r in KnockOutPhase.drag_rects:
            if d_r.group_name == group_name:
                return d_r.team

    @classmethod
    def get_drag_third_by_group_name(cls, group_name):
        for d_r in KnockOutPhase.drag_rects:
            if d_r.group_name == group_name:
                return d_r

    def get_4_thirds(self):
        grps = sorted([r_list.group_name[-1] for r_list in self.get_rating_of_drag_thirds()[:4]])
        if grps == ['A', 'B', 'C', 'D']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group A')), (40, self.get_drag_third_team_by_group_name('Group D')),
                (43, self.get_drag_third_team_by_group_name('Group B')), (41, self.get_drag_third_team_by_group_name('Group C')))
        elif grps == ['A', 'B', 'C', 'E']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group A')), (40, self.get_drag_third_team_by_group_name('Group E')),
                (43, self.get_drag_third_team_by_group_name('Group B')), (41, self.get_drag_third_team_by_group_name('Group C')))
        elif grps == ['A', 'B', 'C', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group A')), (40, self.get_drag_third_team_by_group_name('Group F')),
                (43, self.get_drag_third_team_by_group_name('Group B')), (41, self.get_drag_third_team_by_group_name('Group C')))
        elif grps == ['A', 'B', 'D', 'E']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group D')), (40, self.get_drag_third_team_by_group_name('Group E')),
                (43, self.get_drag_third_team_by_group_name('Group A')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['A', 'B', 'D', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group D')), (40, self.get_drag_third_team_by_group_name('Group F')),
                (43, self.get_drag_third_team_by_group_name('Group A')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['A', 'B', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group E')), (40, self.get_drag_third_team_by_group_name('Group F')),
                (43, self.get_drag_third_team_by_group_name('Group A')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['A', 'C', 'D', 'E']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group E')), (40, self.get_drag_third_team_by_group_name('Group D')),
                (43, self.get_drag_third_team_by_group_name('Group C')), (41, self.get_drag_third_team_by_group_name('Group A')))
        elif grps == ['A', 'C', 'D', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group F')), (40, self.get_drag_third_team_by_group_name('Group D')),
                (43, self.get_drag_third_team_by_group_name('Group C')), (41, self.get_drag_third_team_by_group_name('Group A')))
        elif grps == ['A', 'C', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group E')), (40, self.get_drag_third_team_by_group_name('Group F')),
                (43, self.get_drag_third_team_by_group_name('Group C')), (41, self.get_drag_third_team_by_group_name('Group A')))
        elif grps == ['A', 'D', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group E')), (40, self.get_drag_third_team_by_group_name('Group F')),
                (43, self.get_drag_third_team_by_group_name('Group D')), (41, self.get_drag_third_team_by_group_name('Group A')))
        elif grps == ['B', 'C', 'D', 'E']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group E')), (40, self.get_drag_third_team_by_group_name('Group D')),
                (43, self.get_drag_third_team_by_group_name('Group B')), (41, self.get_drag_third_team_by_group_name('Group C')))
        elif grps == ['B', 'C', 'D', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group F')), (40, self.get_drag_third_team_by_group_name('Group D')),
                (43, self.get_drag_third_team_by_group_name('Group C')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['B', 'C', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group F')), (40, self.get_drag_third_team_by_group_name('Group E')),
                (43, self.get_drag_third_team_by_group_name('Group C')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['B', 'D', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group F')), (40, self.get_drag_third_team_by_group_name('Group E')),
                (43, self.get_drag_third_team_by_group_name('Group D')), (41, self.get_drag_third_team_by_group_name('Group B')))
        elif grps == ['C', 'D', 'E', 'F']:
            return (
                (39, self.get_drag_third_team_by_group_name('Group F')), (40, self.get_drag_third_team_by_group_name('Group E')),
                (43, self.get_drag_third_team_by_group_name('Group D')), (41, self.get_drag_third_team_by_group_name('Group C')))
        else:
            print('1597__', grps)

    @classmethod
    def find_all_4_thirds_group_letters(cls, team, id):
        if id == 39:
            if team.group.name == 'Group A':
                return ['ABCD', 'ABCE', 'ABCF']
            elif team.group.name == 'Group D':
                return ['ABDE', 'ABDF']
            elif team.group.name == 'Group E':
                return ['ABEF', 'ACDE', 'ACEF', 'ADEF', 'BCDE']
            elif team.group.name == 'Group F':
                return ['ACDF', 'BCDF', 'BCEF', 'BDEF', 'CDEF']
        elif id == 40:
            if team.group.name == 'Group D':
                return ['ABCD', 'ACDE', 'ACDF', 'BCDE', 'BCDF']
            elif team.group.name == 'Group E':
                return ['ABCE', 'ABDE', 'BCEF', 'BDEF', 'CDEF']
            elif team.group.name == 'Group F':
                return ['ABCF', 'ABDF', 'ABEF', 'ACEF', 'ADEF']
        elif id == 41:
            if team.group.name == 'Group A':
                return ['ABEF', 'ACDE', 'ACDF', 'ACEF', 'ADEF']
            elif team.group.name == 'Group B':
                return ['ABDE', 'ABDF', 'BCDF', 'BCEF', 'BDEF']
            elif team.group.name == 'Group C':
                return ['ABCD', 'ABCE', 'ABCF', 'BCDE', 'CDEF']
        elif id == 43:
            if team.group.name == 'Group A':
                return ['ABDE', 'ABDF']
            elif team.group.name == 'Group B':
                return ['ABCD', 'ABCE', 'ABCF', 'ABEF', 'BCDE']
            elif team.group.name == 'Group C':
                return ['ACDE', 'ACDF', 'ACEF', 'BCDF', 'BCEF']
            elif team.group.name == 'Group D':
                return ['ADEF', 'BDEF', 'CDEF']

    @classmethod
    def find_best_4_new_thirds(cls, old_4_letters, team, id):
        d = {'ABCD': 'ACBD', 'ABCE': 'ACBE', 'ABCF': 'ACBF', 'ABDE': 'DBAE', 'ABDF': 'DBAF', 'ABEF': 'EABF',
             'ACDE': 'EACD', 'ACDF': 'FACD', 'ACEF': 'EACF', 'ADEF': 'EADF', 'BCDE': 'ECBD', 'BCDF': 'FBCD',
             'BCEF': 'FBCE', 'BDEF': 'FBDE', 'CDEF': 'FCDE'}
        lst_all_4_thirds = [d[f] for f in KnockOutPhase.find_all_4_thirds_group_letters(team, id)]
        for i, id in enumerate([39, 41, 43, 40]):
            third_match_team = KnockOutPhase.get_match_team_by_id_pos(id, 1)
            if len(third_match_team.available_teams) == 1:
                for l4 in lst_all_4_thirds[:]:
                    if l4[i] != third_match_team.team.group.name[-1]:
                        lst_all_4_thirds.remove(l4)
        changes = 4
        best = None
        if len(lst_all_4_thirds) > 0:
            best = lst_all_4_thirds[0]
        for l4 in lst_all_4_thirds:
            count = 0
            for i in range(4):
                if old_4_letters[i] != l4[i]:
                    count += 1
            if count < changes:
                best = l4
                changes = count
        return best

    @classmethod
    def check_letters_with_pins(cls, letters):
        for pin in KnockOutPhase.pinned:
            match_team = KnockOutPhase.get_match_team_by_id_pos(pin[0], pin[1])
            parent0 = match_team.get_0_parent()
            if parent0.position == 1 and parent0.id in [39, 41, 43, 40]:
                letters_list = KnockOutPhase.find_all_4_thirds_group_letters(parent0.team, parent0.id)
                if letters not in letters_list:
                    return False
        return True

    @classmethod
    def convert_from_sorted_to_ordered_4_thirds(cls, letters):
        d = {'ABCD': 'ACBD', 'ABCE': 'ACBE', 'ABCF': 'ACBF', 'ABDE': 'DBAE', 'ABDF': 'DBAF', 'ABEF': 'EABF',
             'ACDE': 'EACD', 'ACDF': 'FACD', 'ACEF': 'EACF', 'ADEF': 'EADF', 'BCDE': 'ECBD', 'BCDF': 'FBCD',
             'BCEF': 'FBCE', 'BDEF': 'FBDE', 'CDEF': 'FCDE'}
        return d[letters]

    @classmethod
    def get_available_groups_for_thirds_from_letters(cls, letters, id):
        if letters == 'ABCD':
            if id == 39:
                return 'Group A'
            elif id == 40:
                return 'Group D'
            elif id == 43:
                return 'Group B'
            elif id == 41:
                return 'Group C'
        elif letters == 'ABCE':
            if id == 39:
                return 'Group A'
            elif id == 40:
                return 'Group E'
            elif id == 43:
                return 'Group B'
            elif id == 41:
                return 'Group C'
        elif letters == 'ABCF':
            if id == 39:
                return 'Group A'
            elif id == 40:
                return 'Group F'
            elif id == 43:
                return 'Group B'
            elif id == 41:
                return 'Group C'
        elif letters == 'ABDE':
            if id == 39:
                return 'Group D'
            elif id == 40:
                return 'Group E'
            elif id == 43:
                return 'Group A'
            elif id == 41:
                return 'Group B'
        elif letters == 'ABDF':
            if id == 39:
                return 'Group D'
            elif id == 40:
                return 'Group F'
            elif id == 43:
                return 'Group A'
            elif id == 41:
                return 'Group B'
        elif letters == 'ABEF':
            if id == 39:
                return 'Group E'
            elif id == 40:
                return 'Group F'
            elif id == 43:
                return 'Group B'
            elif id == 41:
                return 'Group A'
        elif letters == 'ACDE':
            if id == 39:
                return 'Group E'
            elif id == 40:
                return 'Group D'
            elif id == 43:
                return 'Group C'
            elif id == 41:
                return 'Group A'
        elif letters == 'ACDF':
            if id == 39:
                return 'Group F'
            elif id == 40:
                return 'Group D'
            elif id == 43:
                return 'Group C'
            elif id == 41:
                return 'Group A'
        elif letters == 'ACEF':
            if id == 39:
                return 'Group E'
            elif id == 40:
                return 'Group F'
            elif id == 43:
                return 'Group C'
            elif id == 41:
                return 'Group A'
        elif letters == 'ADEF':
            if id == 39:
                return 'Group E'
            elif id == 40:
                return 'Group F'
            elif id == 43:
                return 'Group D'
            elif id == 41:
                return 'Group A'
        elif letters == 'BCDE':
            if id == 39:
                return 'Group E'
            elif id == 40:
                return 'Group D'
            elif id == 43:
                return 'Group B'
            elif id == 41:
                return 'Group C'
        elif letters == 'BCDF':
            if id == 39:
                return 'Group F'
            elif id == 40:
                return 'Group D'
            elif id == 43:
                return 'Group C'
            elif id == 41:
                return 'Group B'
        elif letters == 'BCEF':
            if id == 39:
                return 'Group F'
            elif id == 40:
                return 'Group E'
            elif id == 43:
                return 'Group C'
            elif id == 41:
                return 'Group B'
        elif letters == 'BDEF':
            if id == 39:
                return 'Group F'
            elif id == 40:
                return 'Group E'
            elif id == 43:
                return 'Group D'
            elif id == 41:
                return 'Group B'
        elif letters == 'CDEF':
            if id == 39:
                return 'Group F'
            elif id == 40:
                return 'Group E'
            elif id == 43:
                return 'Group D'
            elif id == 41:
                return 'Group C'

    @classmethod
    def get_available_groups_list_for_thirds_from_letters_list(cls, letters_list, id):
        groups = set()
        for letters in letters_list:
            groups.update([KnockOutPhase.get_available_groups_for_thirds_from_letters(letters, id)])
        return list(groups)
