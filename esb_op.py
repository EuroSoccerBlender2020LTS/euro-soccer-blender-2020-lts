import bpy
from bpy.types import Operator
from .group_score_popup import *
from .blf_text import *
from .group_phase import *
from .group_match_buttons import *
from .knockout_score_popup_rect import *
from .knockout_score_button import *
from .knockout_team_button import *
from .calc_dimencsions import *
from .knockout_team_popup_rect import *

class ESB_OT_draw_operator(Operator):
    bl_idname = "object.esb_ot_draw_operator"
    bl_label = "Euro Soccer Blender 2020 LTS"
    bl_description = "Euro Soccer Blender"
    bl_options = {'REGISTER'}

    @classmethod
    def poll(cls, context):
        return context.screen.name == 'Layout' and 'VIEW_3D' in [ar.type for ar in context.screen.areas] and context.active_object.mode != 'EDIT'


    def backup_scene_layout_stuff(self):
        # if bpy.context.active_object:
        #     bpy.ops.object.mode_set(mode='OBJECT')
        self.objects_hide_backup = {}
        self.otherstuff_hide_backup ={}
        context = bpy.context
        self.dirpath = context.window_manager.ESB_dirpath
        layer = context.view_layer
        scene = context.scene
        scene_collection = scene.collection
        active_collection = layer.active_layer_collection.collection
        view_layer = context.view_layer
        for ob in view_layer.objects:
            self.objects_hide_backup[ob] = ob.hide_viewport
            ob.hide_set(True)
        for screen in bpy.data.screens:
            if screen.name == 'Layout':
                layout_screen = screen
                break
        for area in layout_screen.areas:
            if area.type == 'VIEW_3D':
                view_3d_area = area
                break
        self.otherstuff_hide_backup = {}
        if bpy.context.active_object:
            bpy.ops.object.mode_set(mode='OBJECT')
        self.otherstuff_hide_backup['show_gizmo'] = view_3d_area.spaces[0].show_gizmo
        self.otherstuff_hide_backup['show_overlays'] = view_3d_area.spaces[0].overlay.show_overlays
        self.otherstuff_hide_backup['show_region_header'] = view_3d_area.spaces[0].show_region_header
        self.otherstuff_hide_backup['show_region_tool_header'] = view_3d_area.spaces[0].show_region_tool_header
        self.otherstuff_hide_backup['show_region_ui'] = view_3d_area.spaces[0].show_region_ui
        self.otherstuff_hide_backup['show_region_toolbar'] = view_3d_area.spaces[0].show_region_toolbar
        self.otherstuff_hide_backup['high_gradient'] = tuple(context.preferences.themes[0].view_3d.space.gradients.high_gradient)
        self.otherstuff_hide_backup['gradient'] = tuple(context.preferences.themes[0].view_3d.space.gradients.gradient)
        if 'blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    self.otherstuff_hide_backup['mouse_right_click'] = k.active
                elif k.id == 62:
                    self.otherstuff_hide_backup['mouse_right_click'] = k.active
        elif 'Blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['Blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    self.otherstuff_hide_backup['mouse_right_click'] = k.active
                elif k.id == 62:
                    self.otherstuff_hide_backup['mouse_right_click'] = k.active

        # Hide everything unnecessary
        view_3d_area.spaces[0].show_gizmo = False
        view_3d_area.spaces[0].overlay.show_overlays = False
        view_3d_area.spaces[0].show_region_header = False
        view_3d_area.spaces[0].show_region_tool_header = False
        view_3d_area.spaces[0].show_region_ui = False
        view_3d_area.spaces[0].show_region_toolbar = False
        # context.preferences.themes[0].view_3d.space.gradients.high_gradient = (0.288, 0.288, 0.288)
        # context.preferences.themes[0].view_3d.space.gradients.gradient = (0.032, 0.032, 0.032)
        if 'blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    k.active = False
                elif k.id == 62:
                    k.active = False
        elif 'Blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['Blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    k.active = False
                elif k.id == 62:
                    k.active = False

    def restore_scene_layout_stuff(self):
        context = bpy.context
        layer = context.view_layer
        scene = context.scene
        scene_collection = scene.collection
        active_colection = layer.active_layer_collection.collection
        view_layer = context.view_layer
        for ob in view_layer.objects:
            ob.hide_set(self.objects_hide_backup[ob])
        for screen in bpy.data.screens:
            if screen.name == 'Layout':
                layout_screen = screen
                break
        for area in layout_screen.areas:
            if area.type == 'VIEW_3D':
                view_3d_area = area
                break
        view_3d_area.spaces[0].show_gizmo = self.otherstuff_hide_backup['show_gizmo']
        view_3d_area.spaces[0].overlay.show_overlays = self.otherstuff_hide_backup['show_overlays']
        view_3d_area.spaces[0].show_region_header = self.otherstuff_hide_backup['show_region_header']
        view_3d_area.spaces[0].show_region_tool_header = self.otherstuff_hide_backup['show_region_tool_header']
        view_3d_area.spaces[0].show_region_ui = self.otherstuff_hide_backup['show_region_ui']
        view_3d_area.spaces[0].show_region_toolbar = self.otherstuff_hide_backup['show_region_toolbar']
        # context.preferences.themes[0].view_3d.space.gradients.high_gradient = tuple(self.otherstuff_hide_backup['high_gradient'])
        # context.preferences.themes[0].view_3d.space.gradients.gradient = tuple(self.otherstuff_hide_backup['gradient'])
        if 'blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    k.active = self.otherstuff_hide_backup['mouse_right_click']
                elif k.id == 62:
                    k.active = self.otherstuff_hide_backup['mouse_right_click']
        elif 'Blender' in bpy.data.window_managers['WinMan'].keyconfigs.keys():
            for k in bpy.data.window_managers['WinMan'].keyconfigs['Blender'].keymaps['Object Mode'].keymap_items:
                if k.id == 61:
                    k.active = self.otherstuff_hide_backup['mouse_right_click']
                elif k.id == 62:
                    k.active = self.otherstuff_hide_backup['mouse_right_click']


    def __init__(self):
        self.draw_handle = None
        self.draw_event = None
        self.phase = 'GROUPS'
        self.knockout_teams = {}
        for i in range(37, 45):
            self.knockout_teams[(i, 0)] = 'None'
            self.knockout_teams[(i, 1)] = 'None'
        for screen in bpy.data.screens:
            if screen.name == 'Layout':
                layout_screen = screen
                break
        for area in layout_screen.areas:
            if area.type == 'VIEW_3D':
                view_3d_area = area
                break

        self.backup_scene_layout_stuff()
        self.dimensions_dict = calc_groups_positions()
        # Calculate Scale, Sizes and Boundaries
        for reg in view_3d_area.regions:
            if reg.type == 'WINDOW':
                self.window_width = reg.width - 1
                self.window_height = reg.height - 1


        d_d = self.dimensions_dict
        # Draw Title
        self.title = Text(d_d['title_x'], d_d['title_y'], d_d['title_font_size'], 'Euro Soccer Blender 2020 LTS')
        # Draw Buttons for Group And Knockout Phases
        self.button_group_phase = ButtonRect(d_d['but_group_phase_x'], d_d['but_row_y'], d_d['main_but_width'],
                                             d_d['main_but_height'], 'Group Phase', d_d['main_butt_font_size'])
        self.button_generate_groups = ButtonRect(d_d['but_group_gen_x'], d_d['but_row_y'], d_d['main_but_width'],
                                                 d_d['main_but_height'], 'Generate Groups Results',
                                                 d_d['main_butt_font_size'])
        self.button_generate_knockout = ButtonRect(d_d['but_knockout_gen_x'], d_d['but_row_y'], d_d['main_but_width'],
                                                   d_d['main_but_height'], 'Generate Knockout Results',
                                                   d_d['main_butt_font_size'])
        self.button_knockout_phase = ButtonRect(d_d['but_knockout_phase_x'], d_d['but_row_y'], d_d['main_but_width'],
                                                d_d['main_but_height'], 'KnockOut Phase', d_d['main_butt_font_size'])
        self.refresh_button = ButtonRect(d_d['window_width'] - d_d['main_but_height'] * 2,
                                         d_d['but_row_y'] - d_d['main_but_height'] * 2, d_d['main_but_height'],
                                         d_d['main_but_height'], 'R', d_d['main_butt_font_size'])
        #           Functionality to those buttons
        self.refresh_button.set_pressed_function(self.refresh_func)
        self.button_group_phase.set_pressed_function(self.group_phase_func)
        self.button_knockout_phase.set_pressed_function(self.knockout_phase_func)
        self.button_generate_groups.set_pressed_function(self.group_generate_function_press)
        self.button_generate_knockout.set_pressed_function(self.knockout_generate_function_press)
        self.button_generate_knockout.inactive = True
        self.button_group_phase.inactive = True
        self.button_generate_knockout.color = self.button_generate_knockout.pressed_color
        self.button_group_phase.color = self.button_generate_knockout.pressed_color
        Group.dirpath = self.dirpath
        self.all_groups = {'Group A': Group('Group A'), 'Group B': Group('Group B'), 'Group C': Group('Group C'), \
                           'Group D': Group('Group D'), 'Group E': Group('Group E'), 'Group F': Group('Group F')}
        self.group_A = self.all_groups['Group A']
        self.group_B = self.all_groups['Group B']
        self.group_C = self.all_groups['Group C']
        self.group_D = self.all_groups['Group D']
        self.group_E = self.all_groups['Group E']
        self.group_F = self.all_groups['Group F']
        self.group_A_panel = self.group_A.group_rect
        self.group_B_panel = self.group_B.group_rect
        self.group_C_panel = self.group_C.group_rect
        self.group_D_panel = self.group_D.group_rect
        self.group_E_panel = self.group_E.group_rect
        self.group_F_panel = self.group_F.group_rect
        self.group_A_text = self.group_A.group_text
        self.group_B_text = self.group_B.group_text
        self.group_C_text = self.group_C.group_text
        self.group_D_text = self.group_D.group_text
        self.group_E_text = self.group_E.group_text
        self.group_F_text = self.group_F.group_text
        self.group_A_button = self.group_A.group_generate_button
        self.group_B_button = self.group_B.group_generate_button
        self.group_C_button = self.group_C.group_generate_button
        self.group_D_button = self.group_D.group_generate_button
        self.group_E_button = self.group_E.group_generate_button
        self.group_F_button = self.group_F.group_generate_button

        self.bg_rect = Rect(d_d['low_border'], d_d['low_border'], d_d['window_width'] - 2 * d_d['low_border'],
                            d_d['window_height'] - 2 * d_d['low_border'])
        self.bg_rect.set_color((0.118, 0.118, 0.118, 1.0))
        self.list_of_panels = []
        for i in range(4):
            self.list_of_panels.extend(
                [self.group_A.teams[i].drag_rect, self.group_B.teams[i].drag_rect, self.group_C.teams[i].drag_rect,
                 self.group_D.teams[i].drag_rect, self.group_E.teams[i].drag_rect, self.group_F.teams[i].drag_rect])
        self.groups_score_button = GroupMatchButton(-500, -500)
        self.groups_score_button.set_pressed_function(self.group_score_button_press)
        self.groups_score_popup = PopUpRect(100, 300, 200, 200, self.groups_score_button)
        self.groups_score_popup.set_alpha(0.0)
        self.all_visible_widgets = [self.bg_rect, self.title, self.button_group_phase, self.button_generate_groups,
                                    self.button_generate_knockout, self.button_knockout_phase, self.refresh_button,
                                    self.group_A_panel,
                                    self.group_B_panel, self.group_C_panel, self.group_D_panel, self.group_E_panel,
                                    self.group_F_panel, self.groups_score_button, self.group_A_text , self.group_B_text,
                                    self.group_C_text, self.group_D_text, self.group_E_text, self.group_F_text,
                                    self.group_A_button, self.group_B_button, self.group_C_button, self.group_D_button,
                                    self.group_E_button, self.group_F_button]
        KnockOutPhase.dirpath = self.dirpath
        self.knockout_phase = KnockOutPhase()
        for group in self.all_groups:
            for team in self.all_groups[group].teams:
                KnockOutPhase.tournament_teams[team.name] = team
        self.knockout_phase.init_available_teams()
        self.knockout_bg = self.knockout_phase.bg
        self.list_of_drag_Knockout = self.knockout_phase.drag_rects
        self.knockout_drag_rect_insert_index = 12
        self.knockout_score_button = KnockOutScoreButton(-300, -300)
        self.knockout_score_button.set_pressed_function(self.knockout_score_button_press)
        self.knockout_team_button = KnokOutTeamButton(-500, -500)
        self.knockout_team_popup = KnockOutTeamPopUpRect(-500, -500, 100, 100, [], self.knockout_team_button)
        self.knockout_team_button.set_pressed_function(self.knockout_team_button_press)
        self.knockout_score_popup = KnockOutScorePopUpRect(-500, -500, 100, 100, self.knockout_score_button)
        self.drag_rect_insert_index = len(self.all_visible_widgets) + len(self.list_of_panels) - 1
        self.all_visible_widgets.extend(self.list_of_panels[:])
        self.list_of_popup_rects = [self.groups_score_popup]
        self.list_for_popups = [self.groups_score_button, self.groups_score_popup, self.knockout_score_button,
                                self.knockout_score_popup, self.knockout_team_popup, self.knockout_team_button]
        self.all_visible_widgets.extend(self.list_of_popup_rects)

    def invoke(self, context, event):
        args = (self, context)
        if (context.window_manager.ESB_started is False):
            context.window_manager.ESB_started = True
            # Register draw callback
            self.register_handlers(args, context)
            context.window_manager.modal_handler_add(self)
            return {"RUNNING_MODAL"}
        else:
            context.window_manager.ESB_started = False
            return {"CANCELLED"}

    def register_handlers(self, args, context):
        self.draw_handle = bpy.types.SpaceView3D.draw_handler_add(self.draw_callback_px, (self, context, self.all_visible_widgets), "WINDOW",
                                                                  "POST_PIXEL")
        self.draw_sequencer_hendler = bpy.types.SpaceSequenceEditor.draw_handler_add(self.draw_callback_px, (self, context, [self.bg_rect]), "WINDOW", "POST_PIXEL")
        self.draw_event = context.window_manager.event_timer_add(20, window=context.window)

    def unregister_handlers(self, context):
        context.window_manager.event_timer_remove(self.draw_event)
        bpy.types.SpaceView3D.draw_handler_remove(self.draw_handle, "WINDOW")
        bpy.types.SpaceSequenceEditor.draw_handler_remove(self.draw_sequencer_hendler, "WINDOW")
        self.draw_event = None
        self.draw_handle = None

    def modal(self, context, event):
        if context.area:
            context.area.tag_redraw()
        if context.area.type == 'VIEW_3D':
            for panel in self.all_visible_widgets:
                if panel.handle_event(event) and panel.enabled:
                    if panel in self.list_of_panels and panel.is_drag:
                        if self.list_of_panels.index(panel) == len(self.list_of_panels) - 1:
                            return {"RUNNING_MODAL"}
                        else:
                            self.list_of_panels.remove(panel)
                            self.list_of_panels.append(panel)
                            self.all_visible_widgets.remove(panel)
                            self.all_visible_widgets.insert(self.drag_rect_insert_index, panel)
                            self.unregister_handlers(context)
                            self.register_handlers((self, context, self.all_visible_widgets), context)
                            return {"RUNNING_MODAL"}
                    elif panel in self.list_of_drag_Knockout and panel.is_drag:
                        if self.list_of_drag_Knockout.index(panel) == len(self.list_of_drag_Knockout) - 1:
                            return {"RUNNING_MODAL"}
                        else:
                            self.list_of_drag_Knockout.remove(panel)
                            self.list_of_drag_Knockout.append(panel)
                            self.all_visible_widgets.remove(panel)
                            self.all_visible_widgets.insert(self.knockout_drag_rect_insert_index, panel)
                            self.unregister_handlers(context)
                            self.register_handlers((self, context, self.all_visible_widgets), context)
                            return {"RUNNING_MODAL"}
                    elif panel in self.list_for_popups and panel.activated:
                        return {"RUNNING_MODAL"}
        if event.type == 'TIMER':
            for image_name in self.knockout_bg.images:
                if self.knockout_bg.images[image_name].gl_load():
                    raise Exception()
        if event.type in {'ESC'}:
            context.window_manager.ESB_started = False
        if not context.window_manager.ESB_started:
            self.restore_scene_layout_stuff()
            self.unregister_handlers(context)
            return {'CANCELLED'}
        return {"PASS_THROUGH"}

    def cancel(self, context):
        if context.window_manager.ESB_started:
            self.unregister_handlers(context)
        return {'CANCELLED'}

    def finish(self, context):
        self.unregister_handlers(context)
        return {'FINISHED'}

    def recalc_for_knockout_team_pos_backup(self):
        thirds = Group.get_4_thirds()
        for th in thirds:
            self.knockout_teams[(th[0], 1)] = th[1].name
        self.knockout_teams[(39, 0)] = Group.all_groups['Group B'].get_winner().name
        self.knockout_teams[(37, 0)] = Group.all_groups['Group A'].get_winner().name
        self.knockout_teams[(37, 1)] = Group.all_groups['Group C'].get_runner_up().name
        self.knockout_teams[(41, 0)] = Group.all_groups['Group F'].get_winner().name
        self.knockout_teams[(42, 0)] = Group.all_groups['Group D'].get_runner_up().name
        self.knockout_teams[(42, 1)] = Group.all_groups['Group E'].get_runner_up().name
        self.knockout_teams[(43, 0)] = Group.all_groups['Group E'].get_winner().name
        self.knockout_teams[(44, 0)] = Group.all_groups['Group D'].get_winner().name
        self.knockout_teams[(44, 1)] = Group.all_groups['Group F'].get_runner_up().name
        self.knockout_teams[(40, 0)] = Group.all_groups['Group C'].get_winner().name
        self.knockout_teams[(38, 0)] = Group.all_groups['Group A'].get_runner_up().name
        self.knockout_teams[(38, 1)] = Group.all_groups['Group B'].get_runner_up().name

    def group_generate_function_press(self, widget):
        if self.phase == "GROUPS":
            for group in self.all_groups:
                self.all_groups[group].generate_group_matches(widget)
        elif self.phase == "KNOCK_OUT":
            print('KNOCK_OUT pressed group_generate_function_press')
            KnockOutPhase.generate_unpinned_group_teams()

    def knockout_generate_function_press(self, widget):
        print('pressed  knockout_generate_function_press')
        if self.phase == "KNOCK_OUT":
            KnockOutPhase.generate_matches()

    def group_score_button_press(self, widget):
        if self.groups_score_popup.activated:
            for rect in self.all_visible_widgets:
                rect.enabled = True
            if self.groups_score_popup.group_name != 'NOT IN' and self.groups_score_popup.group_name == widget.group_target \
                    and self.groups_score_popup.match_position == widget.group_position:
                self.groups_score_popup.update(20000, -100)
                self.groups_score_popup.update_focus()
                Group.popup_activated = False
                self.groups_score_popup.deactivate()
        else:
            for rect in self.all_visible_widgets:
                if rect not in self.list_for_popups:
                    rect.enabled = False
            self.groups_score_popup.group_name = widget.group_target
            self.groups_score_popup.match_position = widget.group_position
            if widget.group_target in ['Group A', 'Group B', 'Group C', 'Group D']:
                self.groups_score_popup.up_down = -1
                self.groups_score_popup.update(widget.x, widget.y - self.groups_score_popup.height)
            elif widget.group_target in ['Group E', 'Group F']:
                self.groups_score_popup.up_down = 1
                self.groups_score_popup.update(widget.x, widget.y + Group.group_pos_dict['cell_width'])
            Group.popup_activated = True
            self.groups_score_popup.activate()


    def knockout_score_button_press(self, widget):
        if self.knockout_score_popup.activated:
            for rect in self.all_visible_widgets:
                rect.enabled = True
            if self.knockout_score_popup.match_id != -1 and self.knockout_score_popup.match_id == widget.match_id:
                self.knockout_score_popup.update(20000, -100)
                self.knockout_score_popup.update_focus()
                KnockOutPhase.popup_activated = False
                self.knockout_score_popup.deactivate()
        else:
            for rect in self.all_visible_widgets:
                if rect not in self.list_for_popups:
                    rect.enabled = False
            self.knockout_score_popup.match_id = widget.match_id
            if widget.match_id in range(37, 45):
                self.knockout_score_popup.set_color((0.135, 0.248, 0.16, 1.0))
                self.knockout_score_popup.left_down = 0
                self.knockout_score_popup.update(widget.x + widget.width, widget.y)
            elif widget.match_id in range(45, 49):
                self.knockout_score_popup.set_color((0.365, 0.529, 0.271, 1.0))
                self.knockout_score_popup.left_down = 0
                self.knockout_score_popup.update(widget.x + widget.width, widget.y)
            elif widget.match_id in range(49, 51):
                self.knockout_score_popup.set_color((0.169, 0.310, 0.20, 1.0))
                self.knockout_score_popup.left_down = 0
                self.knockout_score_popup.update(widget.x + widget.width, widget.y)
            elif widget.match_id == 51:
                self.knockout_score_popup.set_color((0.219, 0.3174, 0.1626, 1.0))
                self.knockout_score_popup.left_down = 1
                self.knockout_score_popup.update(widget.x + widget.width - self.knockout_score_popup.width, widget.y - self.knockout_score_popup.height)
            KnockOutPhase.popup_activated = True
            self.knockout_score_popup.activate()

    def knockout_team_button_press(self, widget):
        if self.knockout_team_popup.activated:
            for rect in self.all_visible_widgets:
                rect.enabled = True
            if self.knockout_team_popup.match_id != -1 and self.knockout_team_popup.match_id == widget.match_id and \
                    self.knockout_team_popup.match_position == widget.match_position:
                self.knockout_team_popup.update(20000, -100)
                self.knockout_team_popup.update_focus()
                KnockOutPhase.popup_activated = False
                self.knockout_team_popup.deactivate()
            elif widget.match_id in list(range(37, 51)):
                self.knockout_team_popup.left_down = 0
                self.knockout_team_popup.update(widget.x + widget.width, widget.y)
        else:
            for rect in self.all_visible_widgets:
                if rect not in self.list_for_popups:
                    rect.enabled = False
            self.knockout_team_popup.match_id = widget.match_id
            if widget.match_id in range(37, 45):
                self.knockout_team_popup.set_color((0.135, 0.248, 0.16, 1.0))
            elif widget.match_id in range(45, 49):
                self.knockout_team_popup.set_color((0.365, 0.529, 0.271, 1.0))
            elif widget.match_id in range(49, 50):
                self.knockout_team_popup.set_color((0.169, 0.310, 0.20, 1.0))
            else:
                self.knockout_team_popup.set_color((0.219, 0.3174, 0.1626, 1.0))
            self.knockout_team_popup.match_position = widget.match_position
            self.knockout_team_popup.set_available_teams(
                KnockOutPhase.get_match_team_by_id_pos(self.knockout_team_popup.match_id,
                                                       self.knockout_team_popup.match_position).available_teams)
            width = widget.width
            height = widget.height
            dict_knockout = calc_knockout_positions()
            if widget.match_id == 51:
                coef = dict_knockout['final_coef']
                dx = coef * dict_knockout['rect_height'] / 2
            else:
                coef = 1
                dx = dict_knockout['border'] + dict_knockout['rect_height'] / 2
            if widget.match_id in [39, 37, 46]:
                self.knockout_team_popup.up_down = -1
                self.knockout_team_popup.update(widget.x - dx, widget.y - self.knockout_team_popup.height)
            elif widget.match_id in [40, 38, 47, 50]:
                self.knockout_team_popup.up_down = 1
                self.knockout_team_popup.update(widget.x - dx, widget.y - self.knockout_team_popup.height + height * coef + self.knockout_team_popup.height)
            else:
                if widget.match_position == 0:
                    self.knockout_team_popup.up_down = 1
                    self.knockout_team_popup.update(widget.x - dx, widget.y - self.knockout_team_popup.height + height * coef + self.knockout_team_popup.height)
                else:
                    self.knockout_team_popup.up_down = -1
                    self.knockout_team_popup.update(widget.x - dx, widget.y - self.knockout_team_popup.height)
            KnockOutPhase.popup_activated = True
            self.knockout_team_popup.activate()


    def refresh_func(self, widget):
        context = bpy.context
        for image_name in self.knockout_bg.images:
            if self.knockout_bg.images[image_name].gl_load():
                raise Exception()


    def group_phase_func(self, widget):
        self.phase = 'GROUPS'
        # print(self.phase)
        self.button_generate_knockout.inactive = True
        self.button_group_phase.inactive = True
        self.button_knockout_phase.inactive = False
        self.button_generate_knockout.color = self.button_generate_knockout.pressed_color
        self.button_group_phase.color = self.button_generate_knockout.pressed_color
        self.button_knockout_phase.color = self.button_generate_knockout.default_color
        from_knokout_rated_teams = {}
        old_thirds = [(th[0], th[1], (th[2].points, th[2].goal_diff, th[2].goal_score)) for th in Group.get_all_ranked_thirds()]
        change_flag = False # Whether third must be changed
        in_out_flag = 0 # 0 -- not defined. -1 -- not in 4 first rated, 1 -- in 4 first rated
        thirds = [[dr_th.team_name, dr_th.group_name, change_flag, in_out_flag] for dr_th in KnockOutPhase.get_rating_of_drag_thirds()]
        for group_name in self.all_groups:
            from_knokout_rated_teams[group_name] = {}
            for team_name in self.all_groups[group_name].team_names:
                from_knokout_rated_teams[group_name][team_name] = 3
            third = KnockOutPhase.get_drag_third_team_by_group_name(group_name)
            from_knokout_rated_teams[group_name][third.name] = 2
        for match_team in KnockOutPhase.match_teams:
            if match_team.id > 44:
                continue
            grps, place = match_team.get_place_from_groups()
            from_knokout_rated_teams[match_team.team.group.name][match_team.name] = place
        rated_team_names_of_group = {}
        old_rated_team_names_of_group = {}
        for group_name in self.all_groups:
            old_rated_team_names_of_group[group_name] = self.all_groups[group_name].get_rating()
            lst = sorted(from_knokout_rated_teams[group_name].items(), key=lambda d: d[1])
            rated_team_names_of_group[group_name] = [t[0] for t in lst]
        points_for_third = {}
        for i, th in enumerate(thirds):
            grp_name = Group.get_group_name_from_team_name(th)
            if old_thirds[i][0] in [th[0] for th in thirds[:4]]:
                points_for_third[grp_name] = old_thirds[i][2]
        old_thirds_4_in = [th[0] for th in old_thirds[:4]]
        old_thirds_2_out = [th[0] for th in old_thirds[4:]]
        rated_list = []
        rest_list = thirds[:4]
        rest_tail = thirds[4:]
        for th in thirds[:4]:
            if th[0] in old_thirds_4_in:
                th[3] = 1
                rated_list.append(th)
                rest_list.remove(th)
        if thirds[4][0] in old_thirds_2_out:
            thirds[4][3] = -1
            rated_list.append(thirds[4])
            rest_tail.remove(thirds[4])
        if thirds[5][0] in old_thirds_2_out:
            thirds[5][3] = -1
            rated_list.append(thirds[5])
            rest_tail.remove(thirds[5])
        thirds_4_in = [th[0] for th in thirds[:4]]
        thirds_2_out = [th[0] for th in thirds[4:]]
        min_limit = (3, -2, 1)
        max_limit = (3, -2, 1)
        for srt in rated_list:
            if srt[0] in old_thirds_4_in:
                for old_th in old_thirds:
                    if old_th[0] == srt[0]:
                        max_limit = min(old_th[2], max_limit)
                        break
            elif srt[0] in old_thirds_2_out:
                for old_th in old_thirds:
                    if old_th[0] == srt[0]:
                        min_limit = max(old_th[2], min_limit)
                        break
        rest_tail_for_sort = [(self.all_groups[r[1]].get_prob_mid_points_for_third(rated_team_names_of_group[r[1]]), r) for r in rest_tail]
        rest_for_sort = [(self.all_groups[r[1]].get_prob_mid_points_for_third(rated_team_names_of_group[r[1]]), r) for r in rest_list]
        rest_for_sort.sort()
        rest_tail_for_sort.sort(reverse=True)
        k = 0
        while k < max(len(rest_for_sort), len(rest_tail_for_sort)):
            if k < len(rest_tail_for_sort):
                rest_tail_for_sort[k][1][3] = -1
                rest_tail_for_sort[k][1][2] = True
                rated_list.append(rest_tail_for_sort[k][1])
            if k < len(rest_for_sort):
                rest_for_sort[k][1][3] = 1
                rest_for_sort[k][1][2] = True
                rated_list.append(rest_for_sort[k][1])
            k += 1
        if len(rated_list) != 6 or (0 in [th[3] for th in rated_list]):
            print('607__ ERROR!!!!!!!!!!!!!!! now enough thirds')
        # Now we have ordered list with thirds and flags [team_name, group_name, change_True_False, in_out_1_-1]
        for th in rated_list:
            group_name = th[1]
            if rated_team_names_of_group[group_name] == old_rated_team_names_of_group[group_name]:
                if th[2]:
                    self.all_groups[group_name].generate_group_matches_from_rating_third_points(
                        rated_team_names_of_group[group_name], th[3], min_limit, max_limit)
                    self.all_groups[group_name].real_sort()
            else:
                self.all_groups[group_name].generate_group_matches_from_rating_third_points(
                    rated_team_names_of_group[group_name], th[3], min_limit, max_limit)
                self.all_groups[group_name].real_sort()
        self.recalc_for_knockout_team_pos_backup()
        context = bpy.context
        self.all_visible_widgets = [self.bg_rect, self.title, self.button_group_phase, self.button_generate_groups,
                                    self.button_generate_knockout, self.button_knockout_phase, self.refresh_button,
                                    self.group_A_panel,
                                    self.group_B_panel, self.group_C_panel, self.group_D_panel, self.group_E_panel,
                                    self.group_F_panel, self.groups_score_button, self.group_A_text, self.group_B_text,
                                    self.group_C_text, self.group_D_text, self.group_E_text, self.group_F_text,
                                    self.group_A_button, self.group_B_button, self.group_C_button, self.group_D_button,
                                    self.group_E_button, self.group_F_button]
        self.all_visible_widgets.extend(self.list_of_panels[:])
        self.all_visible_widgets.extend(self.list_of_popup_rects)
        self.unregister_handlers(context)
        self.register_handlers((self, context, self.all_visible_widgets), context)
        print('Done! In Group Phase')

    def knockout_phase_func(self, widget):
        if self.phase == "KNOCK_OUT":
            return
        self.phase = "KNOCK_OUT"
        self.button_generate_knockout.inactive = False
        self.button_group_phase.inactive = False
        self.button_knockout_phase.inactive = True
        self.button_generate_knockout.color = self.button_generate_knockout.default_color
        self.button_group_phase.color = self.button_generate_knockout.default_color
        self.button_knockout_phase.color = self.button_generate_knockout.pressed_color
        thirds = Group.get_4_thirds()
        all_thirds = Group.get_all_ranked_thirds()
        KnockOutPhase.ranking_of_thirds = all_thirds
        self.knockout_phase.reload_ranking_of_thirds()
        print(self.phase, KnockOutPhase.pinned)
        for th in thirds:
            if th[1].name != self.knockout_teams[(th[0], 1)]:
                self.knockout_teams[(th[0], 1)] = th[1].name
                KnockOutPhase.get_match_team_by_id_pos(th[0], 1).set_match_team(th[1])
                KnockOutPhase.clear_pins_from_id_pos(th[0], 1)
                KnockOutPhase.clear_score_included(th[0])
        if Group.all_groups['Group B'].get_winner().name != self.knockout_teams[(39, 0)]:
            self.knockout_teams[(39, 0)] = Group.all_groups['Group B'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(39, 0).set_match_team(Group.all_groups['Group B'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(39, 0)
            KnockOutPhase.clear_score_included(39)
        if Group.all_groups['Group A'].get_winner().name != self.knockout_teams[(37, 0)]:
            self.knockout_teams[(37, 0)] = Group.all_groups['Group A'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(37, 0).set_match_team(Group.all_groups['Group A'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(37, 0)
            KnockOutPhase.clear_score_included(37)
        if Group.all_groups['Group C'].get_runner_up().name != self.knockout_teams[(37, 1)]:
            self.knockout_teams[(37, 1)] = Group.all_groups['Group C'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(37, 1).set_match_team(Group.all_groups['Group C'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(37, 1)
            KnockOutPhase.clear_score_included(37)
        if Group.all_groups['Group F'].get_winner().name != self.knockout_teams[(41, 0)]:
            self.knockout_teams[(41, 0)] = Group.all_groups['Group F'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(41, 0).set_match_team(Group.all_groups['Group F'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(41, 0)
            KnockOutPhase.clear_score_included(41)
        if Group.all_groups['Group D'].get_runner_up().name != self.knockout_teams[(42, 0)]:
            self.knockout_teams[(42, 0)] = Group.all_groups['Group D'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(42, 0).set_match_team(Group.all_groups['Group D'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(42, 0)
            KnockOutPhase.clear_score_included(42)
        if Group.all_groups['Group E'].get_runner_up().name != self.knockout_teams[(42, 1)]:
            self.knockout_teams[(42, 1)] = Group.all_groups['Group E'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(42, 1).set_match_team(Group.all_groups['Group E'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(42, 1)
            KnockOutPhase.clear_score_included(42)
        if Group.all_groups['Group E'].get_winner().name != self.knockout_teams[(43, 0)]:
            self.knockout_teams[(43, 0)] = Group.all_groups['Group E'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(43, 0).set_match_team(Group.all_groups['Group E'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(43, 0)
            KnockOutPhase.clear_score_included(43)
        if Group.all_groups['Group D'].get_winner().name != self.knockout_teams[(44, 0)]:
            self.knockout_teams[(44, 0)] = Group.all_groups['Group D'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(44, 0).set_match_team(Group.all_groups['Group D'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(44, 0)
            KnockOutPhase.clear_score_included(44)
        if Group.all_groups['Group F'].get_runner_up().name != self.knockout_teams[(44, 1)]:
            self.knockout_teams[(44, 1)] = Group.all_groups['Group F'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(44, 1).set_match_team(Group.all_groups['Group F'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(44, 1)
            KnockOutPhase.clear_score_included(44)
        if Group.all_groups['Group C'].get_winner().name != self.knockout_teams[(40, 0)]:
            self.knockout_teams[(40, 0)] = Group.all_groups['Group C'].get_winner().name
            KnockOutPhase.get_match_team_by_id_pos(40, 0).set_match_team(Group.all_groups['Group C'].get_winner())
            KnockOutPhase.clear_pins_from_id_pos(40, 0)
            KnockOutPhase.clear_score_included(40)
        if Group.all_groups['Group A'].get_runner_up().name != self.knockout_teams[(38, 0)]:
            self.knockout_teams[(38, 0)] = Group.all_groups['Group A'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(38, 0).set_match_team(Group.all_groups['Group A'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(38, 0)
            KnockOutPhase.clear_score_included(38)
        if Group.all_groups['Group B'].get_runner_up().name != self.knockout_teams[(38, 1)]:
            self.knockout_teams[(38, 1)] = Group.all_groups['Group B'].get_runner_up().name
            KnockOutPhase.get_match_team_by_id_pos(38, 1).set_match_team(Group.all_groups['Group B'].get_runner_up())
            KnockOutPhase.clear_pins_from_id_pos(38, 1)
            KnockOutPhase.clear_score_included(38)
        context = bpy.context
        self.all_visible_widgets = [self.bg_rect, self.title, self.button_group_phase, self.button_generate_groups,
                                    self.button_generate_knockout, self.button_knockout_phase, self.refresh_button]
        self.knockout_drag_rect_insert_index = len(self.all_visible_widgets) + len(self.list_of_drag_Knockout) - 1
        self.all_visible_widgets.extend(self.list_of_drag_Knockout)
        self.all_visible_widgets.extend([self.knockout_bg, self.knockout_score_button, self.knockout_team_button,
                                         self.knockout_score_popup, self.knockout_team_popup])
        self.unregister_handlers(context)
        self.register_handlers((self, context, self.all_visible_widgets), context)

    def draw_callback_px(self, context, args, list_of_panels):
        for panel in list_of_panels:
            panel.draw()
