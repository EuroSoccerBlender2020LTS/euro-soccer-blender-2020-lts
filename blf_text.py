import bgl
import blf


class Text:

    def __init__(self, x, y, font_size, text):
        self.x = x
        self.y = y
        self.text = text
        self.font_size = font_size
        self.set_color((1.0, 1.0, 1.0, 1.0))
        self.set_shadow_color((0.0, 0.0, 0.0, 1.0))
        self.activated = False

    def set_color(self, color):
        self.color = color

    def set_shadow_color(self, color):
        self.shadow_color = color

    def set_text(self, text):
        self.text = text

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, self.shadow_color[0], self.shadow_color[1], self.shadow_color[2], self.shadow_color[3])
        blf.shadow_offset(font_id, int(self.font_size / 10), -int(self.font_size / 10))
        blf.position(font_id, self.x, self.y, 0)
        blf.size(font_id, self.font_size, 72)
        blf.color(font_id, self.color[0], self.color[1], self.color[2], self.color[3])
        blf.draw(font_id, self.text)
        bgl.glDisable(bgl.GL_BLEND)

    def handle_event(self, event):
        return False
