import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
import random
from .group_phase import Group
from .knockout_phase import KnockOutPhase
from .calc_dimencsions import *


class KnockOutTeamPopUpRect:

    def __init__(self, x, y, width, height, available_teams, button):
        self.button = button
        self.dimensions_dict = calc_knockout_positions()
        self.images = {}
        self.available_teams = available_teams
        self.load_all_images()
        self.x = x
        self.y = y
        self.width = self.dimensions_dict['popup_team_rect_width']
        self.focus_width = self.dimensions_dict['popup_team_focus_width']
        self.font_size = int(self.dimensions_dict['popup_team_font_size'])
        self.font_dimension = self.dimensions_dict['popup_team_font_dimension']
        coef = 1.8
        self.focus_height = self.dimensions_dict['popup_team_font_dimension'][1] * coef
        self.text = self.dimensions_dict['popup_text']
        self.height = self.focus_height * (int((len(self.available_teams) - 1) / 4) + 1)
        self.focus_x = x
        self.focus_y = y
        self.up_down = -1
        self.index_x = int((x - self.x) / self.focus_width)
        self.index_y = self.index_y = int((y - self.y) / self.focus_height)
        if self.up_down == 1:
            self.index_of_cell = self.index_y * 4 + self.index_x
        else:
            self.index_of_cell = (int((len(self.available_teams) - 1) / 4) - self.index_y) * 4 + self.index_x
        self.activated = False
        self.enabled = True
        self.deactivating = False
        self.set_color([0.5, 0.2, 0.8, 0.7])
        self.text_color = [0.855, 0.855, 0.855, 1.0]
        self.match_position = -1
        self.match_id = -1
        self.update(self.x, self.y)
        self.update_focus()
        self.neighbours = []

    def load_img(self, filepath):
        if str(filepath).split('/')[-1] not in bpy.data.images:
            image = bpy.data.images.load(filepath)
        else:
            image = bpy.data.images[str(filepath).split('/')[-1]]
        if image.gl_load():
            raise Exception()
        return image

    def load_all_images(self):
        all_team_names = list(self.dimensions_dict['EQ_ranking'].keys())
        all_team_names.append('None')
        for name in all_team_names:
            filepath = KnockOutPhase.dirpath + '/flags/' + name.replace(' ', '_') + '.png'
            self.images[name] = self.load_img(filepath)

    def set_alpha(self, alpha):
        self.set_color([self.default_color[0], self.default_color[1], self.default_color[2], alpha])
        self.text_color[3] = alpha

    def set_color(self, color):
        self.color = color
        self.default_color = color
        self.hover_on_color = [c * 308 / 288 for c in color]
        self.hover_on_color[3] = 1
        self.pressed_color = self.hover_on_color
        self.focus_color = [0.976, 0.553, 0.39, 1.0]

    def set_available_teams(self, available_teams):
        self.available_teams = available_teams
        self.height = self.focus_height * (int((len(self.available_teams) - 1) / 4) + 1)

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        self.shader_focus.bind()
        self.shader_focus.uniform_float("color", self.focus_color)
        self.batch_focus.draw(self.shader_focus)
        bgl.glDisable(bgl.GL_BLEND)

        for i, team, batch in self.batches_images:
            bgl.glEnable(bgl.GL_BLEND)
            font_id = 0
            blf.enable(font_id, blf.SHADOW)
            blf.shadow(font_id, 0, 0.0, 0.0, 0.0, self.text_color[3] * 0.5)
            blf.shadow_offset(font_id, int(self.font_size / 10), -int(self.font_size / 10))
            blf.size(font_id, self.font_size, 72)
            blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], self.text_color[3])
            image = self.images[team.name]
            bgl.glActiveTexture(bgl.GL_TEXTURE0)
            bgl.glBindTexture(bgl.GL_TEXTURE_2D, image.bindcode)
            self.shader_images.bind()
            self.shader_images.uniform_int("image", 0)
            batch.draw(self.shader_images)
            text_x = self.x + (i % 4) * self.focus_width + self.focus_height + (self.focus_width - self.font_dimension[0] - self.focus_height) * 0.3
            if self.up_down == 1:
                yy = self.y
            else:
                yy = self.y + self.height - self.focus_height
            text_y = yy + self.up_down * (i // 4) * self.focus_height + (self.focus_height - self.font_dimension[1]) * 0.3
            text = self.dimensions_dict['short_names_dict'][team.name]
            blf.position(font_id, text_x, text_y, 0)
            blf.draw(font_id, text)
            blf.disable(font_id, blf.SHADOW)
            bgl.glDisable(bgl.GL_BLEND)



    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)
        self.shader_images = gpu.shader.from_builtin("2D_IMAGE")
        self.batches_images = []
        if self.up_down == 1:
            yy = self.y
        else:
            yy = self.y + self.height - self.focus_height
        for i, team in enumerate(self.available_teams):
            vertices_img = ((self.x + (i % 4) * self.focus_width, yy + self.up_down * (i // 4) * self.focus_height),
                            (self.x + (i % 4) * self.focus_width,
                             yy + self.up_down * (i // 4) * self.focus_height + self.focus_height),
                            (self.x + (i % 4) * self.focus_width + self.focus_height,
                             yy + self.up_down * (i // 4) * self.focus_height + self.focus_height),
                            (self.x + (i % 4) * self.focus_width + self.focus_height,
                             yy + self.up_down * (i // 4) * self.focus_height),
                            )
            self.batches_images.append((i, team, batch_for_shader(self.shader_images, "TRI_FAN", {"pos": vertices_img,
                                                                                                    "texCoord": (
                                                                                                        (0, 0), (0, 1),
                                                                                                        (1, 1),
                                                                                                        (1, 0)), })))

    def update_focus(self):
        indices = ((0, 1, 2), (0, 2, 3))
        focus_vertices = (
            (self.focus_x, self.focus_y),
            (self.focus_x, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y))
        self.shader_focus = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_focus = batch_for_shader(self.shader_focus, 'TRIS', {"pos": focus_vertices}, indices=indices)


    def handle_event(self, event):
        if (event.type == 'LEFTMOUSE'):
            if (event.value == "PRESS"):
                return self.left_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.left_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

    def is_in_rect(self, x, y):
        return (self.x <= x <= (self.x + self.width)) and (self.y <= y <= (self.y + self.height))

    def left_mouse_down(self, x, y):
        if self.is_in_rect(x, y) and self.activated and self.index_of_cell < len(self.available_teams):
            self.color = self.pressed_color
            self.deactivating = True
            return True
        return False

    def mouse_move(self, x, y):
        if self.is_in_rect(x, y):
            self.index_x = int((x - self.x) / self.focus_width)
            self.index_y = int((y - self.y) / self.focus_height)
            if self.up_down == 1:
                self.index_of_cell = self.index_y * 4 + self.index_x
            else:
                self.index_of_cell = (int((len(self.available_teams) - 1 ) / 4) - self.index_y) * 4 + self.index_x
            if self.index_of_cell < len(self.available_teams):
                self.focus_x = self.x + self.focus_width * self.index_x
                self.focus_y = self.y + self.focus_height * self.index_y
            else:
                self.focus_x = -500
                self.focus_y = -500
            self.color = self.hover_on_color
        else:
            self.focus_x = -500
            self.focus_y = -500
            self.color = self.default_color
        self.update_focus()

    def activate(self):
        self.activated = True
        self.set_alpha(1.0)

    def deactivate(self):
        self.activated = False
        self.deactivating = False
        self.set_alpha(0.0)
        self.match_id = -1
        self.match_position = -1

    def left_mouse_up(self, x, y):
        if self.deactivating:
            KnockOutPhase.popup_activated = False
            chosen_team = self.available_teams[self.index_of_cell]
            target_match_team = KnockOutPhase.get_match_team_by_id_pos(self.match_id, self.match_position)
            KnockOutPhase.switch_teams(target_match_team, chosen_team)
            self.focus_x = -500
            self.focus_y = -500
            self.update(-500, -500)
            self.button.pressed_function(self.button)
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color

