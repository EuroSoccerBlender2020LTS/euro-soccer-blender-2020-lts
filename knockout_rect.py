import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import bpy
import collections
from .calc_dimencsions import *

class KnockOutBG:

    def __init__(self, match_teams):
        self.match_teams = match_teams
        self.pins = {}
        self.dimensions_dict = calc_knockout_positions()
        self.all_teams_names = list(self.dimensions_dict['EQ_ranking'].keys())
        self.all_teams_names.append('None')
        self.lines_color = (0.4, 0.2, 0.6, 1.0)
        self.set_colors((0.169, 0.310, 0.20, 1.0), (0.365, 0.529, 0.271, 1.0))
        self.init_pins([(m.x, m.y, m.id, m.position) for m in self.match_teams])
        self.line_width = self.dimensions_dict['line_width'] * 2
        self.x = self.dimensions_dict['column_3_x']
        self.y = self.dimensions_dict['column_1_1_y']
        self.width = self.dimensions_dict['rect_width']
        self.height = self.dimensions_dict['rect_height']
        self.team_font_size = self.dimensions_dict['team_font_size']
        self.images = {}
        self.enabled = True
        self.dirpath = self.match_teams[0].dirpath
        self.imgs = []
        self.load_all_images()
        self.update()

    def handle_event(self, event):
        return False

    def load_img(self, filepath):
        if str(filepath).split('/')[-1] not in bpy.data.images:
            image = bpy.data.images.load(filepath)
        else:
            image = bpy.data.images[str(filepath).split('/')[-1]]
        if image.gl_load():
            raise Exception()
        return image

    def load_all_images(self):
        for name in self.all_teams_names:
            filepath = self. dirpath + '/flags/' + name.replace(' ', '_') + '.png'
            self.images[name] = self.load_img(filepath)

    def set_colors(self, color_1, color_2):
        color = (0.576, 0.725, 0.235, 1.0)
        self.color3 = list(color_1)
        self.color4 = [cl * 0.6 for cl in color_2]
        self.color2 = list(color_2)
        self.color1 = [cl * 0.8 for cl in color_1]
        self.color1[3] = 1
        self.color3[3] = 1
        self.color2[3] = 1
        self.color4[3] = 1
        self.color2_3 = [(self.color2[i] + self.color3[i]) / 2 for i in range(len(self.color2))]
        self.color3_4 = [(self.color4[i] + self.color3[i]) / 2 for i in range(len(self.color3))]
        self.lines_color = (1.0 - color[0], 1.0 - color[1], 1.0 - color[2], 1.0)
        self.color = color
        self.default_color = color
        self.hover_on_color = [c * 0.6 for c in self.default_color]
        self.hover_on_color[1] += 0.4 * self.hover_on_color[1] / 0.6
        self.hover_on_color[3] += 0.4 * self.hover_on_color[3] / 0.6
        self.pressed_color = self.hover_on_color[:]
        self.pressed_color[3] *= 0.3
        self.lines_color = (0.4, 0.2, 0.6, 1.0)
        self.shadow_color = (0.0, 0.0, 0.0, 0.5)
        self.pinned_color = (0.867, 0.325, 0.0, 1.0)
        self.text_color = (0.855, 0.855, 0.855, 1.0)


    def init_pins(self, list_of_matches):
        width = self.dimensions_dict['rect_width'] - self.dimensions_dict['rect_height'] / 3
        height = self.dimensions_dict['rect_height'] / 2
        keys = [(i, 0) for i in range(37, 52)]
        keys.extend([(i, 1) for i in range(37, 52)])
        self.pins = {}
        for match in list_of_matches:
            x = match[0]
            y = match[1]
            id = match[2]
            if id == 51:
                width = self.dimensions_dict['final_coef'] * (self.dimensions_dict['rect_width'] - self.dimensions_dict['rect_height'] / 3)
                height = self.dimensions_dict['final_coef'] * self.dimensions_dict['rect_height'] / 2
            pos = match[3]
            self.pins[(id, pos)] = [((x, y), (x, y + height), (x + width, y + height), (x + width, y)), 0]


    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader_lines.bind()
        bgl.glLineWidth(int(self.line_width))
        self.batch_lines.draw(self.shader_lines)
        self.shader.bind()
        self.shader.uniform_float("color", self.color1)
        self.batch_round_of_16.draw(self.shader)
        self.shader.uniform_float("color", self.color2)
        self.batch_quarter_finals.draw(self.shader)
        self.shader.uniform_float("color", self.color3)
        self.batch_semi_finals.draw(self.shader)
        self.shader.uniform_float("color", self.color4)
        self.batch_final.draw(self.shader)
        for pin, batch in self.batches_pins:
            self.shader.uniform_float("color", (
            self.pinned_color[0], self.pinned_color[1], self.pinned_color[2], self.pinned_color[3] * self.pins[pin][1]))
            batch.draw(self.shader)
        self.shader.bind()
        bgl.glLineWidth(int(self.line_width / 3))
        self.shader.uniform_float("color", (1,1,1,1))
        self.batch_thirds_line.draw(self.shader)
        bgl.glLineWidth(1)
        self.batch_thin_lines.draw(self.shader)
        bgl.glDisable(bgl.GL_BLEND)
        bgl.glEnable(bgl.GL_BLEND)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, self.shadow_color[0], self.shadow_color[1], self.shadow_color[2], self.shadow_color[3])
        blf.shadow_offset(font_id, int(self.team_font_size / 10), -int(self.team_font_size / 10))
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], 1.0)
        blf.position(font_id, int(self.dimensions_dict['limit_low_x']), int(self.dimensions_dict['limit_low_y']), 0)
        blf.enable(font_id, blf.ROTATION)
        blf.rotation(font_id, 3.14159 / 2)
        font_size, dimensions = calc_font_size('RANKING OF THIRDS', (
                    self.dimensions_dict['limit_high_y'] - self.dimensions_dict['limit_low_y']) + 0.23 *
                                               self.dimensions_dict['thirds_height'], 50)
        blf.size(font_id, int(font_size), 72)
        blf.draw(font_id, 'RANKING OF THIRDS')
        blf.disable(font_id, blf.ROTATION)
        bgl.glDisable(bgl.GL_BLEND)

        for match, batch in self.batches_images:
            bgl.glEnable(bgl.GL_BLEND)
            self.shader_images.bind()
            image = self.images[match.name]
            bgl.glActiveTexture(bgl.GL_TEXTURE0)
            # bgl.glBindTexture(bgl.GL_TEXTURE_2D, match.image.bindcode)
            bgl.glBindTexture(bgl.GL_TEXTURE_2D, image.bindcode)
            self.shader_images.uniform_int("image", 0)
            batch.draw(self.shader_images)
            text_x = match.x + self.dimensions_dict['text_x']
            text_y = match.y + self.dimensions_dict['text_y']
            # text = match.name
            text = match.name
            score_x = match.x + self.dimensions_dict['score_x']
            score_y = match.y + self.dimensions_dict['score_y']
            team_font_size = self.dimensions_dict['team_font_size']
            score_font_size = self.dimensions_dict['score_font_size']
            if match.id == 51:
                text_x = match.x + self.dimensions_dict['final_text_x']
                text_y = match.y + self.dimensions_dict['final_text_y']
                score_x = match.x + self.dimensions_dict['final_score_x']
                score_y = match.y + self.dimensions_dict['final_score_y']
                team_font_size = self.dimensions_dict['final_team_font_size']
                score_font_size = self.dimensions_dict['final_score_font_size']
            font_size = team_font_size
            if text == 'North Macedonia':
                font_size -= 3
            blf.size(font_id, int(font_size), 72)
            blf.position(font_id, int(text_x), int(text_y), 0)
            blf.draw(font_id, text)
            blf.size(font_id, int(score_font_size), 72)
            blf.position(font_id, int(score_x), int(score_y), 0)
            blf.draw(font_id, str(match.score))
            bgl.glDisable(bgl.GL_BLEND)

    def update(self):
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        indices = ((0, 1, 2), (0, 2, 3))
        rect_height = self.dimensions_dict['rect_height']
        rect_width = self.dimensions_dict['rect_width']
        line_width = self.dimensions_dict['line_width']
        cell = rect_height / 2
        final_coef = self.dimensions_dict['final_coef']
        column_1_1_y = self.dimensions_dict['column_1_1_y']
        column_1_2_y = self.dimensions_dict['column_1_2_y']
        column_1_3_y = self.dimensions_dict['column_1_3_y']
        column_1_4_y = self.dimensions_dict['column_1_4_y']
        column_2_1_y = self.dimensions_dict['column_2_1_y']
        column_2_2_y = self.dimensions_dict['column_2_2_y']
        column_2_3_y = self.dimensions_dict['column_2_3_y']
        column_2_4_y = self.dimensions_dict['column_2_4_y']
        column_4_1_y = self.dimensions_dict['column_4_1_y']
        column_4_2_y = self.dimensions_dict['column_4_2_y']
        column_5_y = self.dimensions_dict['column_5_y']
        column_1_x = self.dimensions_dict['column_1_x']
        column_2_x = self.dimensions_dict['column_2_x']
        column_3_x = self.dimensions_dict['column_3_x']
        column_4_x = self.dimensions_dict['column_4_x']
        column_5_x = self.dimensions_dict['column_5_x']
        col_1_x = column_1_x + rect_width
        col_2_x = column_2_x + rect_width
        col_1_x_end = column_3_x + 2
        col_3_x = column_3_x + rect_width
        col_3_x_end = column_4_x + rect_width / 2
        col_4_x = column_4_x + rect_width
        col_4_x_end = column_5_x + final_coef * rect_width / 2
        # vertices_round_of_16
        vertices_round_of_16 = (
            (column_1_x, column_1_1_y),
            (column_1_x, column_1_1_y + rect_height),
            (column_1_x + rect_width, column_1_1_y + rect_height),
            (column_1_x + rect_width, column_1_1_y),
            (column_1_x, column_1_2_y),
            (column_1_x, column_1_2_y + rect_height),
            (column_1_x + rect_width, column_1_2_y + rect_height),
            (column_1_x + rect_width, column_1_2_y),
            (column_1_x, column_1_3_y),
            (column_1_x, column_1_3_y + rect_height),
            (column_1_x + rect_width, column_1_3_y + rect_height),
            (column_1_x + rect_width, column_1_3_y),
            (column_1_x, column_1_4_y),
            (column_1_x, column_1_4_y + rect_height),
            (column_1_x + rect_width, column_1_4_y + rect_height),
            (column_1_x + rect_width, column_1_4_y),
            (column_2_x, column_2_1_y),
            (column_2_x, column_2_1_y + rect_height),
            (column_2_x + rect_width, column_2_1_y + rect_height),
            (column_2_x + rect_width, column_2_1_y),
            (column_2_x, column_2_2_y),
            (column_2_x, column_2_2_y + rect_height),
            (column_2_x + rect_width, column_2_2_y + rect_height),
            (column_2_x + rect_width, column_2_2_y),
            (column_2_x, column_2_3_y),
            (column_2_x, column_2_3_y + rect_height),
            (column_2_x + rect_width, column_2_3_y + rect_height),
            (column_2_x + rect_width, column_2_3_y),
            (column_2_x, column_2_4_y),
            (column_2_x, column_2_4_y + rect_height),
            (column_2_x + rect_width, column_2_4_y + rect_height),
            (column_2_x + rect_width, column_2_4_y),
        )
        indices_round_of_16 = (
            (0, 1, 2), (0, 2, 3), (4, 5, 6), (4, 6, 7), (8, 9, 10), (8, 10, 11), (12, 13, 14), (12, 14, 15), (16, 17, 18),
            (16, 18, 19), (20, 21, 22), (20, 22, 23), (24, 25, 26), (24, 26, 27), (28, 29, 30), (28, 30, 31))
        # vertices_quarter_finals
        vertices_quarter_finals = (
            (column_3_x, column_1_1_y),
            (column_3_x, column_1_1_y + rect_height),
            (column_3_x + rect_width, column_1_1_y + rect_height),
            (column_3_x + rect_width, column_1_1_y),
            (column_3_x, column_1_2_y),
            (column_3_x, column_1_2_y + rect_height),
            (column_3_x + rect_width, column_1_2_y + rect_height),
            (column_3_x + rect_width, column_1_2_y),
            (column_3_x, column_1_3_y),
            (column_3_x, column_1_3_y + rect_height),
            (column_3_x + rect_width, column_1_3_y + rect_height),
            (column_3_x + rect_width, column_1_3_y),
            (column_3_x, column_1_4_y),
            (column_3_x, column_1_4_y + rect_height),
            (column_3_x + rect_width, column_1_4_y + rect_height),
            (column_3_x + rect_width, column_1_4_y),
        )
        indices_quarter_finals = ((0, 1, 2), (0, 2, 3), (4, 5, 6), (4, 6, 7), (8, 9, 10), (8, 10, 11), (12, 13, 14), (12, 14, 15))
        vertices_semi_finals = (
            (column_4_x, column_4_1_y),
            (column_4_x, column_4_1_y + rect_height),
            (column_4_x + rect_width, column_4_1_y + rect_height),
            (column_4_x + rect_width, column_4_1_y),
            (column_4_x, column_4_2_y),
            (column_4_x, column_4_2_y + rect_height),
            (column_4_x + rect_width, column_4_2_y + rect_height),
            (column_4_x + rect_width, column_4_2_y),
        )
        indices_semi_finals = ((0, 1, 2), (0, 2, 3), (4, 5, 6), (4, 6, 7))
        vertices_final = (
            (column_5_x, column_5_y),
            (column_5_x, column_5_y + rect_height * final_coef),
            (column_5_x + rect_width * final_coef, column_5_y + rect_height * final_coef),
            (column_5_x + rect_width * final_coef, column_5_y),
        )
        indices_final = ((0, 1, 2), (0, 2, 3))
        line_vertices = (
            (col_1_x, column_1_1_y + rect_height - line_width),
            (col_1_x_end, column_1_1_y + rect_height - line_width),
            (col_2_x, column_1_1_y + line_width),
            (col_1_x_end, column_1_1_y + line_width),
            (col_2_x, column_1_2_y + rect_height - line_width),
            (col_1_x_end, column_1_2_y + rect_height - line_width),
            (col_1_x, column_1_2_y + line_width),
            (col_1_x_end, column_1_2_y + line_width),
            (col_1_x, column_1_3_y + rect_height - line_width),
            (col_1_x_end, column_1_3_y + rect_height - line_width),
            (col_2_x, column_1_3_y + line_width),
            (col_1_x_end, column_1_3_y + line_width),
            (col_2_x, column_1_4_y + rect_height - line_width),
            (col_1_x_end, column_1_4_y + rect_height - line_width),
            (col_1_x, column_1_4_y + line_width),
            (col_1_x_end, column_1_4_y + line_width),
            # 17 - corner
            (col_3_x, column_1_1_y + rect_height / 2),
            (col_3_x_end, column_1_1_y + rect_height / 2),
            (col_3_x_end, column_4_1_y + rect_height),
            # 20 - corner
            (col_3_x, column_1_2_y + rect_height / 2),
            (col_3_x_end, column_1_2_y + rect_height / 2),
            (col_3_x_end, column_4_1_y),
            # 23 - corner
            (col_3_x, column_1_3_y + rect_height / 2),
            (col_3_x_end, column_1_3_y + rect_height / 2),
            (col_3_x_end, column_4_2_y + rect_height),
            # 26 - corner
            (col_3_x, column_1_4_y + rect_height / 2),
            (col_3_x_end, column_1_4_y + rect_height / 2),
            (col_3_x_end, column_4_2_y),
            # 29 - corner
            (col_4_x, column_4_1_y + rect_height / 2),
            (col_4_x_end, column_4_1_y + rect_height / 2),
            (col_4_x_end, column_5_y + final_coef * rect_height),
            # 32 - corner
            (col_4_x, column_4_2_y + rect_height / 2),
            (col_4_x_end, column_4_2_y + rect_height / 2),
            (col_4_x_end, column_5_y),
        )
        color1_list = [0, 2, 4, 6, 8, 10, 12, 14]
        color2_list = [1, 3, 5, 7, 9, 11, 13, 15, 16, 19, 22, 25]
        color2_3_list = [17, 20, 23, 26]
        color3_list = [18, 21, 24, 27, 28, 31]
        color3_4_list = [29, 32]
        color4_list = [30, 33]
        line_color_list = [[1, 1, 1, 1] for _ in range(len(line_vertices))]
        for index in color1_list:
            line_color_list[index] = self.color1
        for index in color2_list:
            line_color_list[index] = self.color2
        for index in color2_3_list:
            line_color_list[index] = self.color2_3
        for index in color3_list:
            line_color_list[index] = self.color3
        for index in color3_4_list:
            line_color_list[index] = self.color3_4
        for index in color4_list:
            line_color_list[index] = self.color4
        line_indices = (
            (0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11), (12, 13), (14, 15), (16, 17), (17, 18), (19, 20), (20, 21),
            (22, 23), (23, 24), (25, 26), (26, 27), (28, 29), (29, 30), (31, 32), (32, 33))
        thin_line_vertices = (
            (column_1_x + rect_width - 6 * cell / 9, column_1_1_y + rect_height),
            (column_1_x + rect_width - 6 * cell / 9, column_1_1_y),
            (column_1_x, column_1_1_y + rect_height / 2),
            (column_1_x + rect_width, column_1_1_y + rect_height / 2),
            (column_1_x + rect_width - 6 * cell / 9, column_1_2_y + rect_height),
            (column_1_x + rect_width - 6 * cell / 9, column_1_2_y),
            (column_1_x, column_1_2_y + rect_height / 2),
            (column_1_x + rect_width, column_1_2_y + rect_height / 2),
            (column_1_x + rect_width - 6 * cell / 9, column_1_3_y + rect_height),
            (column_1_x + rect_width - 6 * cell / 9, column_1_3_y),
            (column_1_x, column_1_3_y + rect_height / 2),
            (column_1_x + rect_width, column_1_3_y + rect_height / 2),
            (column_1_x + rect_width - 6 * cell / 9, column_1_4_y + rect_height),
            (column_1_x + rect_width - 6 * cell / 9, column_1_4_y),
            (column_1_x, column_1_4_y + rect_height / 2),
            (column_1_x + rect_width, column_1_4_y + rect_height / 2),
            (column_2_x + rect_width - 6 * cell / 9, column_2_1_y + rect_height),
            (column_2_x + rect_width - 6 * cell / 9, column_2_1_y),
            (column_2_x, column_2_1_y + rect_height / 2),
            (column_2_x + rect_width, column_2_1_y + rect_height / 2),
            (column_2_x + rect_width - 6 * cell / 9, column_2_2_y + rect_height),
            (column_2_x + rect_width - 6 * cell / 9, column_2_2_y),
            (column_2_x, column_2_2_y + rect_height / 2),
            (column_2_x + rect_width, column_2_2_y + rect_height / 2),
            (column_2_x + rect_width - 6 * cell / 9, column_2_3_y + rect_height),
            (column_2_x + rect_width - 6 * cell / 9, column_2_3_y),
            (column_2_x, column_2_3_y + rect_height / 2),
            (column_2_x + rect_width, column_2_3_y + rect_height / 2),
            (column_2_x + rect_width - 6 * cell / 9, column_2_4_y + rect_height),
            (column_2_x + rect_width - 6 * cell / 9, column_2_4_y),
            (column_2_x, column_2_4_y + rect_height / 2),
            (column_2_x + rect_width, column_2_4_y + rect_height / 2),
            (column_3_x + rect_width - 6 * cell / 9, column_1_1_y + rect_height),
            (column_3_x + rect_width - 6 * cell / 9, column_1_1_y),
            (column_3_x, column_1_1_y + rect_height / 2),
            (column_3_x + rect_width, column_1_1_y + rect_height / 2),
            (column_3_x + rect_width - 6 * cell / 9, column_1_2_y + rect_height),
            (column_3_x + rect_width - 6 * cell / 9, column_1_2_y),
            (column_3_x, column_1_2_y + rect_height / 2),
            (column_3_x + rect_width, column_1_2_y + rect_height / 2),
            (column_3_x + rect_width - 6 * cell / 9, column_1_3_y + rect_height),
            (column_3_x + rect_width - 6 * cell / 9, column_1_3_y),
            (column_3_x, column_1_3_y + rect_height / 2),
            (column_3_x + rect_width, column_1_3_y + rect_height / 2),
            (column_3_x + rect_width - 6 * cell / 9, column_1_4_y + rect_height),
            (column_3_x + rect_width - 6 * cell / 9, column_1_4_y),
            (column_3_x, column_1_4_y + rect_height / 2),
            (column_3_x + rect_width, column_1_4_y + rect_height / 2),
            (column_4_x + rect_width - 6 * cell / 9, column_4_1_y + rect_height),
            (column_4_x + rect_width - 6 * cell / 9, column_4_1_y),
            (column_4_x, column_4_1_y + rect_height / 2),
            (column_4_x + rect_width, column_4_1_y + rect_height / 2),
            (column_4_x + rect_width - 6 * cell / 9, column_4_2_y + rect_height),
            (column_4_x + rect_width - 6 * cell / 9, column_4_2_y),
            (column_4_x, column_4_2_y + rect_height / 2),
            (column_4_x + rect_width, column_4_2_y + rect_height / 2),
            (column_5_x + rect_width * final_coef  - 6 * final_coef  * cell / 9, column_5_y + rect_height * final_coef ),
            (column_5_x + rect_width * final_coef - 6 * final_coef  * cell / 9, column_5_y),
            (column_5_x, column_5_y + rect_height * final_coef  / 2),
            (column_5_x + rect_width * final_coef , column_5_y + rect_height * final_coef / 2),
        )
        thin_line_indices = (
        (0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11), (12, 13), (14, 15), (16, 17), (18, 19), (20, 21), (22, 23),
        (24, 25), (26, 27), (28, 29), (30, 31), (32, 33), (34, 35), (36, 37), (38, 39), (40, 41), (42, 43), (44, 45),
        (46, 47), (48, 49), (50, 51), (52, 53), (54, 55), (56, 57), (58, 59))
        thirds_line_pos = ((self.dimensions_dict['limit_low_x'] + 0.1 * self.dimensions_dict['thirds_height'],
                            self.dimensions_dict['limit_low_y'] + self.dimensions_dict['thirds_height'] * 2.1),
                           (self.dimensions_dict['limit_low_x'] - 0.1 * self.dimensions_dict['thirds_height'] +
                           self.dimensions_dict['thirds_width'],
                           self.dimensions_dict['limit_low_y'] + self.dimensions_dict['thirds_height'] * 2.1))
        thirds_line_indices = ((0, 1),)
        self.shader_images = gpu.shader.from_builtin("2D_IMAGE")
        self.batches_images = []
        for m in self.match_teams:
            vertices_img = ((m.x, m.y),
                            (m.x, m.y + m.width),
                            (m.x + m.width, m.y + m.width),
                            (m.x + m.width, m.y),
                            )
            self.batches_images.append((m, batch_for_shader(self.shader_images, "TRI_FAN", {"pos": vertices_img,
                                                                                                   "texCoord": (
                                                                                                   (0, 0), (0, 1),
                                                                                                   (1, 1), (1, 0)), })))

        self.shader_lines = gpu.shader.from_builtin("2D_SMOOTH_COLOR")
        self.batch_lines = batch_for_shader(self.shader_lines, 'LINES', {"pos": line_vertices, "color": line_color_list}, indices=line_indices)
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batches_pins = []
        for pin in self.pins:
            self.batches_pins.append((pin, batch_for_shader(self.shader, 'TRIS', {"pos": self.pins[pin][0]}, indices=((0, 1, 2), (0, 2, 3)))))
        self.batch_thin_lines = batch_for_shader(self.shader, 'LINES', {"pos": thin_line_vertices}, indices=thin_line_indices)
        self.batch_thirds_line = batch_for_shader(self.shader, 'LINES', {"pos": thirds_line_pos}, indices=thirds_line_indices)
        self.batch_round_of_16 = batch_for_shader(self.shader, 'TRIS', {"pos": vertices_round_of_16}, indices=indices_round_of_16)
        self.batch_quarter_finals = batch_for_shader(self.shader, 'TRIS', {"pos": vertices_quarter_finals}, indices=indices_quarter_finals)
        self.batch_semi_finals = batch_for_shader(self.shader, 'TRIS', {"pos": vertices_semi_finals}, indices=indices_semi_finals)
        self.batch_final = batch_for_shader(self.shader, 'TRIS', {"pos": vertices_final}, indices=indices_final)

