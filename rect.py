import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf


class Rect:

    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.set_color((1.0, 1.0, 1.0, 1.0))
        self.activated = False
        self.enabled = True
        self.update(self.x, self.y)

    def set_color(self, color):
        self.color = color
        self.default_color = color
        self.hover_on_color = [0.976, 0.553, 0.39, 1.0]
        self.pressed_color = list(self.hover_on_color)
        self.pressed_color[3] *= 0.5
        self.pined_color = (1, 0, 0, 1)

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_rect.draw(self.shader)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_rect = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def handle_event(self, event):
        if (event.type == 'LEFTMOUSE'):
            if (event.value == "PRESS"):
                return self.left_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.left_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

    def is_in_rect(self, x, y):
        return (self.x <= x <= (self.x + self.width)) and (self.y <= y <= (self.y + self.height))

    def left_mouse_down(self, x, y):
        return True

    def left_mouse_up(self, x, y):
        pass

    def right_mouse_down(self, x, y):
        return True

    def right_mouse_up(self, x, y):
        pass

    def mouse_move(self, x, y):
        pass
