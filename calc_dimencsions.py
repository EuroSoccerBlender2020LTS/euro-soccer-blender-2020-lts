import blf
import bpy

all_char_dimensions = {'a': (0.6049804594041875, 0.5742260357733727), 'b': (0.6266352620644378, 0.7739478884285874), 'c': (0.5422231209740039, 0.5742260357733727), 'd': (0.6266352620644378, 0.7739478884285874), 'e': (0.607644851347744, 0.5742260357733727), 'f': (0.32786387057751754, 0.7597803632756941), 'g': (0.6266352620644378, 0.7680696452079366), 'h': (0.6257504440007604, 0.7597803632756941), 'i': (0.27024937205923516, 0.7597803632756941), 'g': (0.6266352620644378, 0.7680696452079366), 'k': (0.5711557622726159, 0.7597803632756941), 'l': (0.27024937205923516, 0.7597803632756941), 'm': (0.9660063206401829, 0.5600585106204796), 'n': (0.6257504440007604, 0.5600585106204796), 'o': (0.6039666432629966, 0.5742260357733727), 'p': (0.6266352620644378, 0.7680696452079366), 'q': (0.6266352620644378, 0.7680696452079366), 'r': (0.4031172951955282, 0.5600585106204796), 's': (0.5129276341471702, 0.5742260357733727), 't': (0.3841541028977438, 0.7021489983372001), 'u': (0.6257504440007604, 0.5610425251528932), 'v': (0.5838867467981468, 0.546875), 'w': (0.8101101061747527, 0.546875), 'x': (0.5838867467981468, 0.546875), 'y': (0.5838867467981468, 0.7548861345874572), 'z': (0.5169134366050091, 0.546875), 'A': (0.6600491840013943, 0.7290110308415529), 'B': (0.6778266819320186, 0.7290110308415529), 'C': (0.690057389522319, 0.756416581098699), 'D': (0.7622354218588656, 0.7290110308415529), 'E': (0.6239261895426831, 0.7290110308415529), 'F': (0.5671018749382984, 0.7290110308415529), 'G': (0.7670532274263303, 0.756416581098699), 'H': (0.744650780178043, 0.7290110308415529), 'I': (0.28691463423661145, 0.7290110308415529), 'J': (0.28691463423661145, 0.9292138885387681), 'K': (0.6478935220945897, 0.7290110308415529), 'L': (0.5492855046766525, 0.7290110308415529), 'M': (0.8548381131678247, 0.7290110308415529), 'N': (0.7395544207287726, 0.7290110308415529), 'I': (0.28691463423661145, 0.7290110308415529), 'O': (0.7790933905586531, 0.756416581098699), 'P': (0.5953630842002295, 0.7290110308415529), 'Q': (0.7790933905586531, 0.8711893615363521), 'R': (0.6867910481689795, 0.7290110308415529), 'S': (0.6266352620644378, 0.756416581098699), 'T': (0.5549330582602024, 0.7290110308415529), 'U': (0.7240952578274918, 0.7431785559944456), 'V': (0.6760705666022011, 0.7290110308415529), 'W': (0.9791744235535154, 0.7290110308415529), 'X': (0.6769506086637415, 0.7290110308415529), 'Y': (0.6029972060626212, 0.7290110308415529), 'Z': (0.6769506086637415, 0.7290110308415529), '-': (0.3527340322496392, 0.08007173152337897), ':': (0.3134592491361742, 0.5170921284792933), ' ': (0.31026428796191713, 0.0), '0': (0.6286184404320007, 0.756416581098699), '1': (0.6286184404320007, 0.7290110308415529), '2': (0.6286184404320007, 0.7422490559458058), '3': (0.6286184404320007, 0.756416581098699), '4': (0.6286184404320007, 0.7290110308415529), '5': (0.6286184404320007, 0.7431785559944456), '6': (0.6286184404320007, 0.756416581098699), '7': (0.6286184404320007, 0.7290110308415529), '8': (0.6286184404320007, 0.756416581098699), '9': (0.6286184404320007, 0.756416581098699)}

group_rankings_probabilities = {'Group A': [(('Italy', 'Switzerland', 'Turkey', 'Wales'), 116273),
                                            (('Italy', 'Switzerland', 'Wales', 'Turkey'), 113369),
                                            (('Italy', 'Turkey', 'Switzerland', 'Wales'), 91744),
                                            (('Italy', 'Wales', 'Switzerland', 'Turkey'), 87972),
                                            (('Italy', 'Turkey', 'Wales', 'Switzerland'), 72574),
                                            (('Italy', 'Wales', 'Turkey', 'Switzerland'), 72052),
                                            (('Switzerland', 'Italy', 'Turkey', 'Wales'), 59845),
                                            (('Switzerland', 'Italy', 'Wales', 'Turkey'), 56955),
                                            (('Turkey', 'Italy', 'Switzerland', 'Wales'), 33138),
                                            (('Wales', 'Italy', 'Switzerland', 'Turkey'), 32026),
                                            (('Switzerland', 'Turkey', 'Italy', 'Wales'), 30310),
                                            (('Switzerland', 'Wales', 'Italy', 'Turkey'), 30307),
                                            (('Turkey', 'Italy', 'Wales', 'Switzerland'), 27095),
                                            (('Wales', 'Italy', 'Turkey', 'Switzerland'), 26433),
                                            (('Turkey', 'Switzerland', 'Italy', 'Wales'), 22184),
                                            (('Wales', 'Switzerland', 'Italy', 'Turkey'), 20490),
                                            (('Turkey', 'Wales', 'Italy', 'Switzerland'), 16728),
                                            (('Switzerland', 'Turkey', 'Wales', 'Italy'), 16406),
                                            (('Switzerland', 'Wales', 'Turkey', 'Italy'), 15956),
                                            (('Wales', 'Turkey', 'Italy', 'Switzerland'), 14490),
                                            (('Turkey', 'Switzerland', 'Wales', 'Italy'), 12417),
                                            (('Wales', 'Switzerland', 'Turkey', 'Italy'), 10808),
                                            (('Turkey', 'Wales', 'Switzerland', 'Italy'), 10739),
                                            (('Wales', 'Turkey', 'Switzerland', 'Italy'), 9689)],
                                'Group B': [(('Belgium', 'Denmark', 'Russia', 'Finland'), 207370),
                                            (('Belgium', 'Russia', 'Denmark', 'Finland'), 195400),
                                            (('Belgium', 'Denmark', 'Finland', 'Russia'), 88471),
                                            (('Belgium', 'Russia', 'Finland', 'Denmark'), 87492),
                                            (('Denmark', 'Belgium', 'Russia', 'Finland'), 71407),
                                            (('Russia', 'Belgium', 'Denmark', 'Finland'), 67232),
                                            (('Belgium', 'Finland', 'Denmark', 'Russia'), 45494),
                                            (('Belgium', 'Finland', 'Russia', 'Denmark'), 45051),
                                            (('Russia', 'Denmark', 'Belgium', 'Finland'), 35993),
                                            (('Denmark', 'Russia', 'Belgium', 'Finland'), 33742),
                                            (('Denmark', 'Belgium', 'Finland', 'Russia'), 31137),
                                            (('Russia', 'Belgium', 'Finland', 'Denmark'), 28948),
                                            (('Finland', 'Belgium', 'Russia', 'Denmark'), 8186),
                                            (('Finland', 'Belgium', 'Denmark', 'Russia'), 8138),
                                            (('Russia', 'Finland', 'Belgium', 'Denmark'), 6623),
                                            (('Denmark', 'Finland', 'Belgium', 'Russia'), 6556),
                                            (('Denmark', 'Russia', 'Finland', 'Belgium'), 6460),
                                            (('Russia', 'Denmark', 'Finland', 'Belgium'), 6241),
                                            (('Finland', 'Denmark', 'Belgium', 'Russia'), 4074),
                                            (('Finland', 'Russia', 'Belgium', 'Denmark'), 4069),
                                            (('Russia', 'Finland', 'Denmark', 'Belgium'), 3876),
                                            (('Denmark', 'Finland', 'Russia', 'Belgium'), 3643),
                                            (('Finland', 'Russia', 'Denmark', 'Belgium'), 2241),
                                            (('Finland', 'Denmark', 'Russia', 'Belgium'), 2156)],
                                'Group C': [(('Netherlands', 'Ukraine', 'Austria', 'North Macedonia'), 206770),
                                            (('Netherlands', 'Austria', 'Ukraine', 'North Macedonia'), 200788),
                                            (('Netherlands', 'Ukraine', 'North Macedonia', 'Austria'), 88191),
                                            (('Netherlands', 'Austria', 'North Macedonia', 'Ukraine'), 84731),
                                            (('Ukraine', 'Netherlands', 'Austria', 'North Macedonia'), 74231),
                                            (('Austria', 'Netherlands', 'Ukraine', 'North Macedonia'), 66846),
                                            (('Netherlands', 'North Macedonia', 'Ukraine', 'Austria'), 45673),
                                            (('Netherlands', 'North Macedonia', 'Austria', 'Ukraine'), 45118),
                                            (('Ukraine', 'Austria', 'Netherlands', 'North Macedonia'), 34075),
                                            (('Austria', 'Ukraine', 'Netherlands', 'North Macedonia'), 32310),
                                            (('Ukraine', 'Netherlands', 'North Macedonia', 'Austria'), 30508),
                                            (('Austria', 'Netherlands', 'North Macedonia', 'Ukraine'), 29082),
                                            (('North Macedonia', 'Netherlands', 'Austria', 'Ukraine'), 8065),
                                            (('North Macedonia', 'Netherlands', 'Ukraine', 'Austria'), 7746),
                                            (('Ukraine', 'North Macedonia', 'Netherlands', 'Austria'), 6750),
                                            (('Ukraine', 'Austria', 'North Macedonia', 'Netherlands'), 6486),
                                            (('Austria', 'North Macedonia', 'Netherlands', 'Ukraine'), 6458),
                                            (('Austria', 'Ukraine', 'North Macedonia', 'Netherlands'), 6205),
                                            (('North Macedonia', 'Ukraine', 'Netherlands', 'Austria'), 4170),
                                            (('North Macedonia', 'Austria', 'Netherlands', 'Ukraine'), 3907),
                                            (('Austria', 'North Macedonia', 'Ukraine', 'Netherlands'), 3752),
                                            (('Ukraine', 'North Macedonia', 'Austria', 'Netherlands'), 3733),
                                            (('North Macedonia', 'Ukraine', 'Austria', 'Netherlands'), 2205),
                                            (('North Macedonia', 'Austria', 'Ukraine', 'Netherlands'), 2200)],
                                'Group D': [(('England', 'Croatia', 'Czech Republic', 'Scotland'), 242594),
                                            (('England', 'Croatia', 'Scotland', 'Czech Republic'), 156042),
                                            (('Croatia', 'England', 'Czech Republic', 'Scotland'), 133388),
                                            (('England', 'Czech Republic', 'Croatia', 'Scotland'), 95783),
                                            (('Croatia', 'England', 'Scotland', 'Czech Republic'), 84681),
                                            (('England', 'Scotland', 'Croatia', 'Czech Republic'), 61848),
                                            (('England', 'Czech Republic', 'Scotland', 'Croatia'), 41295),
                                            (('Croatia', 'Czech Republic', 'England', 'Scotland'), 36627),
                                            (('England', 'Scotland', 'Czech Republic', 'Croatia'), 29773),
                                            (('Czech Republic', 'England', 'Croatia', 'Scotland'), 23368),
                                            (('Czech Republic', 'Croatia', 'England', 'Scotland'), 17598),
                                            (('Croatia', 'Scotland', 'England', 'Czech Republic'), 15850),
                                            (('Scotland', 'England', 'Croatia', 'Czech Republic'), 10430),
                                            (('Czech Republic', 'England', 'Scotland', 'Croatia'), 9784),
                                            (('Croatia', 'Czech Republic', 'Scotland', 'England'), 8236),
                                            (('Scotland', 'Croatia', 'England', 'Czech Republic'), 7193),
                                            (('Croatia', 'Scotland', 'Czech Republic', 'England'), 6543),
                                            (('Scotland', 'England', 'Czech Republic', 'Croatia'), 4619),
                                            (('Czech Republic', 'Croatia', 'Scotland', 'England'), 3341),
                                            (('Scotland', 'Croatia', 'Czech Republic', 'England'), 2822),
                                            (('Czech Republic', 'Scotland', 'England', 'Croatia'), 2469),
                                            (('Czech Republic', 'Scotland', 'Croatia', 'England'), 2203),
                                            (('Scotland', 'Czech Republic', 'England', 'Croatia'), 2110),
                                            (('Scotland', 'Czech Republic', 'Croatia', 'England'), 1403)],
                                'Group E': [(('Spain', 'Poland', 'Sweden', 'Slovakia'), 214450),
                                            (('Spain', 'Sweden', 'Poland', 'Slovakia'), 166553),
                                            (('Spain', 'Poland', 'Slovakia', 'Sweden'), 136270),
                                            (('Spain', 'Sweden', 'Slovakia', 'Poland'), 82225),
                                            (('Poland', 'Spain', 'Sweden', 'Slovakia'), 71885),
                                            (('Spain', 'Slovakia', 'Poland', 'Sweden'), 68834),
                                            (('Spain', 'Slovakia', 'Sweden', 'Poland'), 55553),
                                            (('Poland', 'Spain', 'Slovakia', 'Sweden'), 43587),
                                            (('Sweden', 'Spain', 'Poland', 'Slovakia'), 37617),
                                            (('Poland', 'Sweden', 'Spain', 'Slovakia'), 23322),
                                            (('Sweden', 'Poland', 'Spain', 'Slovakia'), 18898),
                                            (('Sweden', 'Spain', 'Slovakia', 'Poland'), 17457),
                                            (('Slovakia', 'Spain', 'Poland', 'Sweden'), 10806),
                                            (('Poland', 'Slovakia', 'Spain', 'Sweden'), 9839),
                                            (('Slovakia', 'Spain', 'Sweden', 'Poland'), 8881),
                                            (('Slovakia', 'Poland', 'Spain', 'Sweden'), 5553),
                                            (('Poland', 'Sweden', 'Slovakia', 'Spain'), 5413),
                                            (('Sweden', 'Slovakia', 'Spain', 'Poland'), 4798),
                                            (('Sweden', 'Poland', 'Slovakia', 'Spain'), 4102),
                                            (('Poland', 'Slovakia', 'Sweden', 'Spain'), 4014),
                                            (('Slovakia', 'Sweden', 'Spain', 'Poland'), 3345),
                                            (('Sweden', 'Slovakia', 'Poland', 'Spain'), 2579),
                                            (('Slovakia', 'Poland', 'Sweden', 'Spain'), 2211),
                                            (('Slovakia', 'Sweden', 'Poland', 'Spain'), 1808)],
                                'Group F': [(('Germany', 'France', 'Portugal', 'Hungary'), 193535),
                                            (('France', 'Germany', 'Portugal', 'Hungary'), 187739),
                                            (('Germany', 'Portugal', 'France', 'Hungary'), 122269),
                                            (('France', 'Portugal', 'Germany', 'Hungary'), 118262),
                                            (('Portugal', 'Germany', 'France', 'Hungary'), 76505),
                                            (('Portugal', 'France', 'Germany', 'Hungary'), 74623),
                                            (('Germany', 'France', 'Hungary', 'Portugal'), 59926),
                                            (('France', 'Germany', 'Hungary', 'Portugal'), 58698),
                                            (('Germany', 'Portugal', 'Hungary', 'France'), 16393),
                                            (('France', 'Portugal', 'Hungary', 'Germany'), 16369),
                                            (('France', 'Hungary', 'Germany', 'Portugal'), 11286),
                                            (('Germany', 'Hungary', 'France', 'Portugal'), 10732),
                                            (('Portugal', 'Germany', 'Hungary', 'France'), 10475),
                                            (('Portugal', 'France', 'Hungary', 'Germany'), 10411),
                                            (('Germany', 'Hungary', 'Portugal', 'France'), 7917),
                                            (('France', 'Hungary', 'Portugal', 'Germany'), 7225),
                                            (('Hungary', 'Germany', 'France', 'Portugal'), 3126),
                                            (('Hungary', 'France', 'Germany', 'Portugal'), 2895),
                                            (('Portugal', 'Hungary', 'Germany', 'France'), 2668),
                                            (('Portugal', 'Hungary', 'France', 'Germany'), 2280),
                                            (('Hungary', 'Germany', 'Portugal', 'France'), 1996),
                                            (('Hungary', 'France', 'Portugal', 'Germany'), 1817),
                                            (('Hungary', 'Portugal', 'Germany', 'France'), 1453),
                                            (('Hungary', 'Portugal', 'France', 'Germany'), 1400)]}

def calc_text_dimension(text, font_size):
    x = 0
    y = 0
    o = all_char_dimensions['o'][1] * font_size
    i = all_char_dimensions['i'][1] * font_size
    O_ = all_char_dimensions['O'][1] * font_size
    sp = all_char_dimensions[' '][0] * font_size
    for ch in text:
        x += all_char_dimensions[ch][0] * font_size
        h_up = 0
        h_down = 0
        if ch in 'gpqy':
            h_down = all_char_dimensions[ch][1] * font_size - o
        elif ch == 'j':
            h_down = all_char_dimensions[ch][1] * font_size - i
            h_up = i - o
        elif ch == 'Q':
            h_down = all_char_dimensions[ch][1] * font_size - O_
            h_up = O_ - o
        elif ch in 'bdfhijkltABCDEFGHIJKLMNOPRSTUVWXYZ:0123456789':
            h_up = all_char_dimensions[ch][1] * font_size - o
        y = max(y, h_up + h_down + o)
    return x, y

def calc_font_size(text, width, height, bias=1, only_axis='XY'):
    font_size = 200
    if only_axis == 'XY':
        w, h = calc_text_dimension(text, font_size)
        counter = 0
        while (width - bias > w or w > width + bias):
            counter += 1
            if w > width + bias:
                font_size -= 1
                w, h = calc_text_dimension(text, font_size)
            elif w < width - bias:
                font_size += 1
                w, h = calc_text_dimension(text, font_size)
            if counter > 220:
                break
        counter = 0
        while h > height + bias / 10:
            counter += 1
            font_size -= 1
            w, h = calc_text_dimension(text, font_size)
            if counter > 220:
                break
        return font_size, (w, h)
    elif only_axis == 'X':
        w, h = calc_text_dimension(text, font_size)
        counter = 0
        while width - bias > w or w > width + bias:
            counter += 1
            if w > width + bias:
                font_size -= 1
                w, h = calc_text_dimension(text, font_size)
            elif w < width - bias:
                font_size += 1
                w, h = calc_text_dimension(text, font_size)
            if counter > 220:
                break
        return font_size, (w, h)
    elif only_axis == 'Y':
        counter = 0
        w, h = calc_text_dimension(text, font_size)
        while height + bias / 10 < h:
            counter += 1
            font_size -= 1
            w, h = calc_text_dimension(text, font_size)
            if counter > 220:
                break
        return font_size, (w, h)
    else:
        return 100, (500, 500)

def old_calc_font_size(text, width, heigth, bias, only_axis):
    font_id = 0
    blf.position(font_id, 10, 10, 0)
    font_size = 100
    blf.size(font_id, font_size, 72)
    if only_axis == 'XY':
        while (width - bias > blf.dimensions(font_id, text)[0] or blf.dimensions(font_id, text)[0] > width + bias):
            if blf.dimensions(font_id, text)[0] > width + bias:
                font_size -= 1
                blf.size(font_id, font_size, 72)
            elif blf.dimensions(font_id, text)[0] < width - bias:
                font_size += 1
                blf.size(font_id, font_size, 72)
        while blf.dimensions(font_id, text)[1] > heigth + bias / 10:
            font_size -= 1
            blf.size(font_id, font_size, 72)
        return font_size, blf.dimensions(font_id, text)
    elif only_axis == 'X':
        while width - bias > blf.dimensions(font_id, text)[0] or blf.dimensions(font_id, text)[0] > width + bias:
            if blf.dimensions(font_id, text)[0] > width + bias:
                font_size -= 1
                blf.size(font_id, font_size, 72)
            elif blf.dimensions(font_id, text)[0] < width - bias:
                font_size += 1
                blf.size(font_id, font_size, 72)
        return font_size, blf.dimensions(font_id, text)
    elif only_axis == 'Y':
        while heigth + bias < blf.dimensions(font_id, text)[1] or heigth - bias > blf.dimensions(font_id, text)[1]:
            if blf.dimensions(font_id, text)[1] > heigth + bias:
                font_size -= 1
                blf.size(font_id, font_size, 72)
            elif blf.dimensions(font_id, text)[1] > heigth + bias:
                font_size -= 1
                blf.size(font_id, font_size, 72)
        return font_size, blf.dimensions(font_id, text)
    else:
        return 100, (500, 500)

def old_calc_text_dimension(text, font_size):
    font_id = 0
    blf.position(font_id, 10, 10, 0)
    blf.size(font_id, int(font_size), 72)
    return blf.dimensions(font_id, text)

def calc_knockout_positions():
    window_width = 1000
    window_height = 700
    bg_border = 10
    for screen in bpy.data.screens:
        if screen.name == 'Layout':
            layout_screen = screen
            break
    for area in layout_screen.areas:
        if area.type == 'VIEW_3D':
            view_3d_area = area
            break
    for reg in view_3d_area.regions:
        if reg.type == 'WINDOW':
            window_width = reg.width - 1
            window_height = reg.height - 1
    title_text = 'Euro Soccer Blender 2020 LTS'
    font_size, title_text_dimansions = calc_font_size(title_text, window_width / 3, window_height / 20,
                                                      10, 'XY')
    font_size = max(font_size, 10)
    title_x = (window_width - calc_text_dimension(title_text, font_size)[0]) / 2
    title_y = window_height - font_size - 5

    buttons_spacing_rate = 0.2
    button_width = window_width / (3 + 4 * buttons_spacing_rate)
    # 'Group Phase' 'Generate Results' KnockOut Phase'
    butt_font_size = int(font_size * 0.95)
    row_buttons = title_y - 1.7 * calc_text_dimension('Generate RepZults', butt_font_size)[1]
    row_buttons_height = calc_text_dimension('Group Phase', butt_font_size)[1] * 1.4
    spacing_for_thirds = 0
    border = 20
    dist = window_height / 9
    final_coef = 1.3
    line_width = 10 / 2
    rect_width = (window_width - 2 * dist - 3 * border) / (3 + final_coef)
    rect_height = (row_buttons - spacing_for_thirds + 4 * line_width - 3 * dist - 2 * border) / 6
    text_x = rect_height * (0.5 + 0.05)
    score_font_size, score_font_dimension = calc_font_size('0', rect_height / 3, rect_height * 0.2, 5, 'XY')
    final_score_font_size, final_score_font_dimension = calc_font_size('0', final_coef * rect_height / 3, final_coef * rect_height * 0.2, 5, 'XY')
    team_font_size, team_font_dimention = calc_font_size('Czech Republic', (rect_width - rect_height) , 0.3 * rect_height, 6, 'XY')
    final_team_font_size, final_team_font_dimension = calc_font_size('Czech Republic', (rect_width - rect_height) * final_coef, 0.3 * rect_height * final_coef, 6, 'XY')
    final_text_x = final_coef * rect_height * (0.5 + 0.05)
    final_text_y = (rect_height * final_coef / 2 - final_team_font_dimension[1]) * 0.5
    text_y = (rect_height / 2 - team_font_dimention[1]) * 0.5
    score_y = (rect_height / 2 - score_font_dimension[1]) * 0.5
    final_score_y = (final_coef * rect_height / 2 - final_score_font_dimension[1]) * 0.5
    final_score_x = final_coef * (rect_width - (rect_height / 2.8 + final_score_font_dimension[0] / final_coef) * 0.5)
    score_x = rect_width - (rect_height / 3 + score_font_dimension[0]) * 0.5
    column_1_x = border
    column_1_1_y = row_buttons - spacing_for_thirds - border - rect_height
    column_1_4_y = border
    column_1_3_y = column_1_4_y + dist + 2 * rect_height - 2 * line_width
    column_1_2_y = column_1_3_y + dist + rect_height
    column_2_x = 2 * border + rect_width
    column_2_4_y = border + rect_height / 2 - line_width
    column_2_3_y = column_2_4_y + dist + rect_height
    column_2_1_y = row_buttons - spacing_for_thirds - border - 1.5 * rect_height + line_width
    column_2_2_y = column_2_1_y - dist - rect_height
    column_3_x = column_2_x + rect_width + dist
    column_4_x = column_3_x + rect_width / 2 + border
    column_4_1_y = (column_1_1_y + column_1_2_y) / 2
    column_4_2_y = (column_1_3_y + column_1_4_y) / 2
    column_5_x = window_width - border - final_coef * rect_width
    column_5_y = (row_buttons - spacing_for_thirds) / 2 - final_coef * rect_height / 2
    thirds_spacing = 0.15
    thirds_font_size, thirds_font_dimension = calc_font_size('SWE A', 100, rect_height * 0.2, 1, "Y")
    thirds_height = rect_height * 0.6
    thirds_width = thirds_font_dimension[0] + thirds_height + calc_text_dimension('ii', thirds_font_size)[0]
    thirds_text_x = thirds_height + (thirds_width - thirds_height - thirds_font_dimension[0]) * 0.5
    thirds_text_y = (thirds_height - thirds_font_dimension[1]) * 0.5
    thirds_title_font_size, thirds_title_font_dimension = calc_font_size('RANKING OF THIRDS', thirds_height * 6, 100, 5, 'X')
    limit_low_x = window_width - thirds_width - bg_border
    limit_low_y = bg_border
    limit_high_x = window_width - bg_border
    limit_high_y = bg_border + 6 * thirds_height
    limit_high_y = bg_border + 6 * thirds_height
    popup_team_rect_width = rect_width + 2 * border
    popup_team_focus_width = popup_team_rect_width / 4
    popup_team_font_size, popup_team_font_dimension = calc_font_size('WW SWE ', popup_team_focus_width * 1.2, popup_team_focus_width * 0.3, 5, 'X')
    popup_team_focus_height = popup_team_font_dimension[0] * 0.3
    popup_team_font_dimension = calc_text_dimension('SWE', popup_team_font_size)
    match_coef = {(0, 0): (2.7, 1.6, 2.7), (0, 1): (2.05, 3.1, 3.95), (0, 2): (1.65, 3.8, 6.1), (0, 3): (1.4, 4.2, 9.3),
     (0, 4): (1.1, 8, 15), (1, 1): (0, 1.9, 0), (1, 2): (1.9, 2.8, 4.6), (1, 3): (1.8, 3.5, 6.5), (1, 4): (1.6, 4, 6),
     (2, 2): (2.75, 2.2, 2.75), (2, 3): (2.5, 2.95, 3.6), (2, 4): (1.7, 3.65, 4.65), (3, 3): (2.75, 1.7, 2.75),
     (3, 4): (2.2, 2.75, 3.75), (4, 4): (2.7, 1.8, 2.7)}
    short_names = (
    'TUR', 'ITA', 'WLS', 'CHE', 'DNK', 'FIN', 'BEL', 'RUS', 'NLD', 'UKR', 'AUT', 'NMK', 'ENG', 'HRV', 'SCT', 'CZE',
    'ESP', 'SWE', 'POL', 'SVK', 'HUN', 'PRT', 'FRA', 'DEU')
    short_names_dict = {'Turkey': 'TUR', 'Italy': 'ITA', 'Wales': 'WLS', 'Switzerland': 'CHE', 'Denmark': 'DNK',
                        'Finland': 'FIN', 'Belgium': 'BEL', 'Russia': 'RUS', 'Netherlands': 'NLD', 'Ukraine': 'UKR',
                        'Austria': 'AUT', 'North Macedonia': 'NMK', 'England': 'ENG', 'Croatia': 'HRV',
                        'Scotland': 'SCT', 'Czech Republic': 'CZE', 'Spain': 'ESP', 'Sweden': 'SWE', 'Poland': 'POL',
                        'Slovakia': 'SVK', 'Hungary': 'HUN', 'Portugal': 'PRT', 'France': 'FRA', 'Germany': 'DEU'}
    popup_text = []
    for j in range(6):
        line = ''
        for i in range(6):
            line += (' ' + str(j) + ':' + str(i) + ' ')
        popup_text.append(line)
    coef = 0.7
    score_popup_dimensions = calc_text_dimension(popup_text[0], coef * score_font_size)
    EQ_ranking = {'Belgium': 1, 'Italy': 2, 'England': 3, 'Germany': 4, 'Spain': 5, 'Ukraine': 6, 'France': 7,
                  'Poland': 8, 'Switzerland': 9, 'Croatia': 10, 'Netherlands': 11, 'Russia': 12, 'Portugal': 13,
                  'Turkey': 14, 'Denmark': 15, 'Austria': 16, 'Sweden': 17, 'Czech Republic': 18, 'Wales': 19,
                  'Finland': 20, 'Slovakia': 22, 'Scotland': 29, 'North Macedonia': 30, 'Hungary': 31}
    match_win_draw_lose = {(4, 4): (0.2711864406779661, 0.4576271186440678, 0.2711864406779661),
                           (4, 3): (0.45865717763835573, 0.3033055529543965, 0.23803726940724784),
                           (4, 2): (0.5866126787295963, 0.2547133999746931, 0.1586739212957105),
                           (4, 1): (0.673913043478261, 0.22463768115942032, 0.10144927536231885),
                           (4, 0): (0.8258774948382656, 0.11355815554026152, 0.06056434962147281),
                           (3, 3): (0.29185867895545314, 0.41628264208909366, 0.29185867895545314),
                           (3, 2): (0.4780994803266518, 0.32442464736451376, 0.19747587230883445),
                           (3, 1): (0.558282208588957, 0.28711656441717787, 0.15460122699386503),
                           (3, 0): (0.6, 0.24, 0.15999999999999998),
                           (2, 2): (0.30769230769230776, 0.38461538461538464, 0.30769230769230776),
                           (2, 1): (0.393406186330802, 0.3333950731616966, 0.27319874050750137),
                           (2, 0): (0.5460468109064587, 0.25432317220300815, 0.19963001689053325),
                           (1, 1): (0.2764227642276423, 0.4471544715447155, 0.2764227642276423),
                           (1, 0): (0.4189944134078213, 0.335195530726257, 0.2458100558659218),
                           (0, 0): (0.2857142857142857, 0.4285714285714286, 0.2857142857142857)}


    d = {'rect_width': rect_width, 'rect_height': rect_height, 'text_x': text_x, 'text_y': text_y, 'score_x': score_x, \
         'score_y': score_y, 'score_font_size': score_font_size, 'score_font_dimension': score_font_dimension, \
         'team_font_size': team_font_size, 'team_font_dimention': team_font_dimention, 'final_coef': final_coef, \
         'column_1_x': column_1_x, 'column_1_1_y': column_1_1_y, 'column_1_2_y': column_1_2_y, 'column_1_3_y': column_1_3_y, \
         'column_1_4_y': column_1_4_y, 'column_2_x': column_2_x, 'column_2_1_y': column_2_1_y, 'column_2_2_y': column_2_2_y, \
         'column_2_3_y': column_2_3_y, 'column_2_4_y': column_2_4_y, 'column_3_x': column_3_x, 'column_4_x': column_4_x, \
         'column_4_1_y': column_4_1_y, 'column_4_2_y': column_4_2_y, 'column_5_x': column_5_x, 'column_5_y': column_5_y, \
         'line_width': line_width, 'score_popup_dimensions': score_popup_dimensions, 'popup_text': popup_text, \
         'title_x': title_x, 'title_y': title_y, 'title_button_width': button_width, 'window_width': window_width, \
         'knockout_window_height': row_buttons, 'title_font_size': font_size, 'title_butt_font_size': butt_font_size,
         'title_buttons_y': row_buttons, 'title_buttons_x1': buttons_spacing_rate * button_width, \
         'title_buttons_x2': (buttons_spacing_rate * 2 + 1) * button_width, \
         'title_buttons_x3': (buttons_spacing_rate * 3 + 2) * button_width, \
         'title_buttons_x4': (buttons_spacing_rate * 4 + 3) * button_width, \
         'title_buttons_height': row_buttons_height, 'border': border, 'EQ_ranking': EQ_ranking,
         'final_score_font_size': final_score_font_size, 'final_score_font_dimension': final_score_font_dimension,
         'final_team_font_size': final_team_font_size, 'final_team_font_dimension': final_team_font_dimension,
         'final_text_x': final_text_x, 'final_text_y': final_text_y, 'final_score_x': final_score_x,
         'final_score_y': final_score_y, 'limit_low_x': limit_low_x, 'limit_low_y': limit_low_y,
         'limit_high_x': limit_high_x, 'limit_high_y': limit_high_y, 'thirds_font_size': thirds_font_size,
         'thirds_font_dimension': thirds_font_dimension, 'thirds_width': thirds_width, 'thirds_height': thirds_height,
         'thirds_text_x': thirds_text_x, 'thirds_text_y': thirds_text_y, 'thirds_title_font_size': thirds_title_font_size,
         'thirds_title_font_dimension': thirds_title_font_dimension, 'bg_border': bg_border,
         'popup_team_rect_width': popup_team_rect_width, 'popup_team_focus_width': popup_team_focus_width,
         'popup_team_font_size': popup_team_font_size, 'popup_team_font_dimension': popup_team_font_dimension,
         'popup_team_focus_height': popup_team_focus_height, 'short_names_dict': short_names_dict,
         'match_win_draw_lose': match_win_draw_lose}
    return d

def calc_groups_positions():
    for screen in bpy.data.screens:
        if screen.name == 'Layout':
            layout_screen = screen
            break
    for area in layout_screen.areas:
        if area.type == 'VIEW_3D':
            view_3d_area = area
            break
    for reg in view_3d_area.regions:
        if reg.type == 'WINDOW':
            window_width = reg.width - 1
            window_height = reg.height - 1
    # Draw Title
    low_border_y = min(10, window_height / 100)
    title_text = 'Euro Soccer Predictor 2020 LTS'
    title_font_size, title_text_dimansions = calc_font_size(title_text, window_width / 3, window_height / 20)
    title_font_size = max(title_font_size, 10)
    title_x = (window_width - calc_text_dimension(title_text, title_font_size)[0]) / 2
    title_y = window_height - title_text_dimansions[1] - low_border_y * 2
    buttons_spacing_rate = 0.08
    button_width = window_width / (4 + 5 * buttons_spacing_rate)
    # 'Group Phase' 'Generate Results' Generate Knockout' KnockOut Phase'
    butt_font_size, butt_text_dimension = calc_font_size('Generate Knopkgut Results', button_width, 100)
    row_buttons = title_y - 2.5 * butt_text_dimension[1]
    but_row_y = row_buttons
    but_group_phase_x = button_width * buttons_spacing_rate
    but_group_gen_x =  button_width * (buttons_spacing_rate * 2 + 1)
    but_knockout_gen_x = button_width * (buttons_spacing_rate * 3 + 2)
    but_knockout_phase_x = button_width * (buttons_spacing_rate * 4 + 3)
    main_but_width = button_width
    main_but_height = butt_text_dimension[1] * 1.7
    cell = min(window_width / 40, (row_buttons - low_border_y * 2) / 24)
    row_group_height = 7.5 * cell
    groups_spacing_y = (row_buttons - 3 * row_group_height - low_border_y) / 3
    row_group_E = low_border_y * 2
    row_group_C = low_border_y * 2 + row_group_height + groups_spacing_y
    row_group_A = low_border_y * 2 + 2 * row_group_height + 2 * groups_spacing_y
    column_team_width_cells_count = 7
    column_group_width = (6.4 + column_team_width_cells_count + 3) * cell
    groups_spacing_x = (window_width - 2 * column_group_width) / 3
    column_group_A = groups_spacing_x
    column_group_B = groups_spacing_x * 2 + column_group_width
    score_font_size, score_font_dimensions = calc_font_size('0 : 0', 100, 0.5 * cell, 1, 'Y')
    team_font_size, team_font_dimension = calc_font_size('EnglandSpain', 4.5 * cell, 0.82 * 1.2 * cell, 5, 'XY')
    coef = 0.5
    stats_font_size, stats_font_dimension = calc_font_size('7', 100, coef * 0.82 * 1.2 * cell, 1, 'Y')
    coef = 0.9
    stats_font_size2, stats_font_dimension2 = calc_font_size('GD', coef * cell, 1.5 * cell, 1, 'X')
    score_names_font_size, score_names_dimention = calc_font_size('SWE', 50, 0.4 * cell, 1, 'Y')
    short_names = ('TUR', 'ITA', 'WLS', 'CHE', 'DNK', 'FIN', 'BEL', 'RUS', 'NLD', 'UKR', 'AUT', 'NMK', 'ENG', 'HRV', 'SCT', 'CZE',
     'ESP', 'SWE', 'POL', 'SVK', 'HUN', 'PRT', 'FRA', 'DEU')
    score_short_names_dimentions_x = {}
    for sn in short_names:
        score_short_names_dimentions_x[sn] = calc_text_dimension(sn, score_names_font_size)[0]
    popup_text = []
    for j in range(6):
        line = ''
        for i in range(6):
            line += (' ' + str(j) + ':' + str(i) + ' ')
        popup_text.append(line)
    coef = 0.7
    score_popup_dimensions = calc_text_dimension(popup_text[0], coef * score_font_size)
    EQ_ranking = {'Belgium': 1, 'Italy': 2, 'England': 3, 'Germany': 4, 'Spain': 5, 'Ukraine': 6, 'France': 7,
                  'Poland': 8, 'Switzerland': 9, 'Croatia': 10, 'Netherlands': 11, 'Russia': 12, 'Portugal': 13,
                  'Turkey': 14, 'Denmark': 15, 'Austria': 16, 'Sweden': 17, 'Czech Republic': 18, 'Wales': 19,
                  'Finland': 20, 'Slovakia': 22, 'Scotland': 29, 'North Macedonia': 30, 'Hungary': 31}
    short_names = (
    'BEL', 'ITA', 'ENG', 'DEU', 'ESP', 'UKR', 'FRA', 'POL', 'CHE', 'HRV', 'NLD', 'RUS', 'PRT', 'TUR', 'DNK', 'AUT',
    'SWE', 'CZE', 'WLS', 'FIN', 'SVK', 'SCT', 'NMK', 'HUN')
    level_stats = {'Belgium': (4, 9, 6), 'Italy': (3, 7, 6), 'England': (4, 8, 8), 'Germany': (4, 9, 5),
                   'Spain': (4, 8, 6), 'Ukraine': (2, 5, 4), 'France': (4, 8, 7), 'Poland': (2, 3, 3),
                   'Switzerland': (2, 4, 4), 'Croatia': (3, 6, 5), 'Netherlands': (4, 8, 7), 'Russia': (2, 4, 2),
                   'Portugal': (3, 7, 5), 'Turkey': (1, 3, 2), 'Denmark': (2, 6, 4), 'Austria': (2, 4, 3),
                   'Sweden': (1, 3, 1), 'Czech Republic': (1, 2, 1), 'Wales': (1, 2, 2), 'Finland': (0, 0, 0),
                   'Slovakia': (0, 1, 1), 'Scotland': (0, 1, 1), 'North Macedonia': (0, 0, 0), 'Hungary': (0, 1, 0)}
    match_win_draw_lose = {(4, 4): (0.2711864406779661, 0.4576271186440678, 0.2711864406779661),
                           (4, 3): (0.45865717763835573, 0.3033055529543965, 0.23803726940724784),
                           (4, 2): (0.5866126787295963, 0.2547133999746931, 0.1586739212957105),
                           (4, 1): (0.673913043478261, 0.22463768115942032, 0.10144927536231885),
                           (4, 0): (0.8258774948382656, 0.11355815554026152, 0.06056434962147281),
                           (3, 3): (0.29185867895545314, 0.41628264208909366, 0.29185867895545314),
                           (3, 2): (0.4780994803266518, 0.32442464736451376, 0.19747587230883445),
                           (3, 1): (0.558282208588957, 0.28711656441717787, 0.15460122699386503),
                           (3, 0): (0.6, 0.24, 0.15999999999999998),
                           (2, 2): (0.30769230769230776, 0.38461538461538464, 0.30769230769230776),
                           (2, 1): (0.393406186330802, 0.3333950731616966, 0.27319874050750137),
                           (2, 0): (0.5460468109064587, 0.25432317220300815, 0.19963001689053325),
                           (1, 1): (0.2764227642276423, 0.4471544715447155, 0.2764227642276423),
                           (1, 0): (0.4189944134078213, 0.335195530726257, 0.2458100558659218),
                           (0, 0): (0.2857142857142857, 0.4285714285714286, 0.2857142857142857)}
    match_over_under_stats = (
        (1, ((1.95, 1.8), (6.5, 1.1)), ((1.12, 5.8), (1.71, 2.07), (3.4, 1.29), (8, 1.08), (20, 1.01))),
        (2, ((1.45, 2.63), (3.45, 1.28), (10, 1.05)), ((1.28, 3.5), (2.5, 1.49), (6.4, 1.11), (19, 1.01))),
        (3, ((1.12, 6), (1.67, 2.12), (3.25, 1.3), (7.5, 1.09), (19, 1.01)), ((1.82, 1.93), (5.6, 1.13))),
        (4, ((1.16, 4.9), (1.89, 1.85), (4, 1.22), (10, 1.05), ((1.53, 2.43), (3.85, 1.24), (12, 1.04))),
         (5, ((1.15, 5.25), (1.8, 1.95), (3.7, 1.25), (9, 1.06)), ((1.72, 2.05), (5, 1.16), (17, 1.01))),
         (6, ((1.12, 6), (1.64, 2.17), (3.15, 1.33), (7, 1.1), (17, 1.01)), ((2.1, 1.68), (7.5, 1.09))),
         (7, ((1.18, 4.6), (1.98, 1.78), (4.35, 1.2), (11, 1.04)), ((1.54, 2.38), (3.9, 1.23), (12.5, 1.03))),
         (8, ((1.38, 2.88), (3, 1.36), (8, 1.08)), ((1.38, 2.88), (3, 1.36), (8, 1.07))),
         (9, ((1.09, 7.5), (1.5, 2.48), (2.7, 1.43), (5.6, 1.13), (13, 1.03)), ((1.83, 1.91), (5.75, 1.13))),
         (10, ((1.21, 4.1), (2.14, 1.66), (4.9, 1.16), (12.5, 1.03)), ((1.65, 2.15), (4.5, 1.18), (15, 1.02))),
         (11, ((1.67, 2.12), (4.7, 1.17), (16, 1.02)), ((1.14, 5.5), (1.76, 2), (3.55, 1.27), (8.5, 1.07))),
         (12, ((1.36, 2.95), (2.95, 1.37), (8, 1.07)), (1.27, 3.55), (2.48, 1.5), (6.25, 1.11), (18, 1.01))))
    team_points_wdls = {(9, 6, 3, 0): {'wwwwww'}, (9, 6, 1, 1): {'wwwwwd'}, (9, 4, 4, 0): {'wwwdww'},
                        (9, 4, 3, 1): {'wwwwdw'}, (9, 4, 2, 1): {'wwwdwd'}, (9, 3, 3, 3): {'wwwwlw', 'wwwlwl'},
                        (9, 2, 2, 2): {'wwwddd'}, (7, 7, 3, 0): {'dwwwww'}, (7, 7, 1, 1): {'dwwwwd'},
                        (7, 6, 4, 0): {'wdwwww'}, (7, 6, 3, 1): {'wwdwww'}, (7, 6, 2, 1): {'wdwwwd'},
                        (7, 5, 4, 0): {'dwwdww'}, (7, 5, 3, 1): {'dwwwdw'}, (7, 5, 2, 1): {'dwwdwd'},
                        (7, 4, 4, 1): {'dwwlwd', 'wdwwdw', 'wwddww'}, (7, 4, 3, 3): {'dwwlwl', 'dwwwlw'},
                        (7, 4, 3, 2): {'wwdwdw'}, (7, 4, 3, 1): {'wdwdwd'}, (7, 4, 2, 2): {'wwddwd', 'wdwwdd'},
                        (7, 3, 2, 2): {'dwwddd'}, (6, 6, 6, 0): {'lwwlww', 'wlwwww'},
                        (6, 6, 4, 1): {'lwwlwd', 'wlwwwd'}, (6, 6, 3, 3): {'wwlwww', 'lwwlwl', 'lwwwlw', 'wlwwwl'},
                        (6, 5, 4, 1): {'lwwddw'}, (6, 5, 2, 2): {'lwwddd'},
                        (6, 4, 4, 3): {'wlwdwl', 'wwldww', 'lwwdlw'}, (6, 4, 4, 2): {'wlwwdd', 'lwwldd'},
                        (5, 5, 5, 0): {'ddwdww'}, (5, 5, 4, 1): {'ddwwdw', 'dwddww'}, (5, 5, 3, 2): {'dwdwdw'},
                        (5, 5, 3, 1): {'ddwdwd'}, (5, 5, 2, 2): {'ddwwdd', 'dwddwd'},
                        (5, 4, 4, 3): {'ddwlwl', 'ddwwlw'}, (5, 4, 4, 2): {'dwdlwd', 'wddwdw'},
                        (5, 4, 3, 2): {'wdddwd'}, (5, 3, 3, 2): {'ddwddd'},
                        (4, 4, 4, 4): {'ldwldl', 'wlddwl', 'dlwwld', 'lwddlw', 'dwllwd', 'wdlwdw'},
                        (4, 4, 4, 3): {'lwdldd', 'wldwdd'}, (3, 3, 3, 3): {'dddddd'}}

    d = {'cell_width': cell, 'row_group_A': row_group_A, 'row_group_C': row_group_C, 'row_group_E': row_group_E, \
         'column_group_A': column_group_A, 'column_group_B': column_group_B, 'title_x': title_x, 'title_y': title_y, \
         'window_width': window_width, 'window_height': window_height, 'low_border': low_border_y,
         'title_font_size': title_font_size, 'main_butt_font_size': butt_font_size,
         'but_row_y': but_row_y, 'but_group_phase_x': but_group_phase_x, 'but_group_gen_x': but_group_gen_x,
         'but_knockout_gen_x': but_knockout_gen_x, 'but_knockout_phase_x': but_knockout_phase_x,
         'main_but_width': main_but_width, 'main_but_height': main_but_height,
         'groups_low_border_y': low_border_y, \
         'row_group_height': row_group_height, 'column_team_width_cells_count': column_team_width_cells_count, \
         'column_group_width': column_group_width, 'score_font_size': score_font_size, 'score_font_dimensions': score_font_dimensions, \
         'team_font_size': team_font_size, 'team_font_dimension': team_font_dimension, 'stats_font_size': stats_font_size, \
         'stats_font_dimension': stats_font_dimension, 'stats_font_size2': stats_font_size2, 'stats_font_dimension2': stats_font_dimension2, \
         'score_popup_dimensions': score_popup_dimensions, 'popup_text': popup_text, 'EQ_ranking': EQ_ranking,
         'score_names_font_size': score_names_font_size, 'score_names_dimention': score_names_dimention,
         'score_short_names_dimentions_x': score_short_names_dimentions_x, 'level_stats': level_stats,
         'match_over_under_stats': match_over_under_stats, 'match_win_draw_lose': match_win_draw_lose
         }
    return d