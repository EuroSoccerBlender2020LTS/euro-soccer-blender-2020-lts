import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
import random
from .knockout_phase import KnockOutPhase
from .calc_dimencsions import *


class KnockOutScorePopUpRect:

    def __init__(self, x, y, width, height, button):
        self.button = button
        self.dimensions_dict = calc_knockout_positions()
        self.x = x
        self.y = y
        self.text = self.dimensions_dict['popup_text']
        coef_width = 0.8
        coef1 = 0.7
        self.font_size = int(self.dimensions_dict['team_font_size'] * coef1)
        self.font_dimension = old_calc_text_dimension(' 0:0 ', self.font_size)
        self.width = old_calc_text_dimension(self.text[0], self.font_size)[0]
        self.d_x = 0
        coef2 = 1.8
        self.height = self.font_dimension[1] * 6 * coef2
        self.focus_x = x
        self.focus_y = y
        self.focus_width = self.font_dimension[0]
        self.focus_height = self.height / 6
        self.activated = False
        self.enabled = True
        self.deactivating = False
        self.set_color((0.288, 0.288, 0.288, 1.0))
        self.text_color = [0.855, 0.855, 0.855, 1.0]
        self.match_position = -1
        self.match_id = -1
        self.update(self.x, self.y)
        self.update_focus()
        self.neighbours = []
        self.left_down = 0

    def set_alpha(self, alpha):
        self.set_color([self.default_color[0], self.default_color[1], self.default_color[2], alpha])
        self.text_color[3] = alpha

    def set_color(self, color):
        self.color = color
        self.default_color = color
        self.hover_on_color = [c * 308 / 288 for c in color]
        self.hover_on_color[3] = 1
        self.pressed_color = self.hover_on_color
        self.focus_color = [0.976, 0.553, 0.39, 1.0]

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        self.shader_focus.bind()
        self.shader.uniform_float("color", self.focus_color)
        self.batch_focus.draw(self.shader_focus)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, 0.0, 0.0, 0.0, self.text_color[3] * 0.5)
        blf.shadow_offset(font_id, int(self.font_size / 10), -int(self.font_size/10))
        blf.size(font_id, self.font_size,  72)
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], self.text_color[3])
        for i in range(6):
            blf.position(font_id, self.x + self.d_x, self.y + self.focus_height * i + (self.focus_height - self.font_dimension[1])/2, 0)
            blf.draw(font_id, self.text[i])
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def update_focus(self):
        indices = ((0, 1, 2), (0, 2, 3))
        focus_vertices = (
            (self.focus_x, self.focus_y),
            (self.focus_x, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y))
        self.shader_focus = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_focus = batch_for_shader(self.shader_focus, 'TRIS', {"pos": focus_vertices}, indices=indices)

    def handle_event(self, event):
        if (event.type == 'LEFTMOUSE'):
            if (event.value == "PRESS"):
                return self.left_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.left_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

    def is_in_rect(self, x, y):
        return (self.x <= x <= (self.x + self.width)) and (self.y <= y <= (self.y + self.height))

    def left_mouse_down(self, x, y):
        if self.is_in_rect(x, y) and self.activated:
            self.color = self.pressed_color
            self.deactivating = True
            return True
        return False

    def mouse_move(self, x, y):
        if self.is_in_rect(x, y):
            self.index_x = int((x - self.x) / self.focus_width)
            self.index_y = int((y - self.y) / self.focus_height)
            self.focus_x = self.x + self.focus_width * self.index_x
            self.focus_y = self.y + self.focus_height * self.index_y
            self.color = self.hover_on_color
        else:
            self.focus_x = -100
            self.focus_y = -100
            self.color = self.default_color
        self.update_focus()

    def activate(self):
        self.activated = True
        self.set_alpha(1.0)

    def deactivate(self):
        self.activated = False
        self.deactivating = False
        self.set_alpha(0.0)
        self.match_id = -1
        self.match_position = -1

    def left_mouse_up(self, x, y):
        if self.deactivating:
            KnockOutPhase.popup_activated = False
            in_x = self.index_y
            in_y = self.index_x
            old_score_0 = KnockOutPhase.get_match_team_by_id_pos(self.match_id, 0).score
            old_score_1 = KnockOutPhase.get_match_team_by_id_pos(self.match_id, 1).score
            KnockOutPhase.get_match_team_by_id_pos(self.match_id, 0).score = in_x
            KnockOutPhase.get_match_team_by_id_pos(self.match_id, 1).score = in_y
            if in_x > in_y:
                KnockOutPhase.set_winner(KnockOutPhase.get_match_team_by_id_pos(self.match_id, 0).team, self.match_id)
                if str(old_score_0) < str(old_score_1):
                    KnockOutPhase.clear_score(self.match_id)
            elif in_y > in_x:
                KnockOutPhase.set_winner(KnockOutPhase.get_match_team_by_id_pos(self.match_id, 1).team, self.match_id)
                if str(old_score_0) > str(old_score_1):
                    KnockOutPhase.clear_score(self.match_id)
            else:
                rnd = random.randint(0, 1)
                if rnd == 0:
                    if str(old_score_0) < str(old_score_1):
                        KnockOutPhase.clear_score(self.match_id)
                    KnockOutPhase.set_winner(KnockOutPhase.get_match_team_by_id_pos(self.match_id, 0).team, self.match_id)
                    KnockOutPhase.get_match_team_by_id_pos(self.match_id, 0).score = str(in_x) + '*'
                else:
                    if str(old_score_0) > str(old_score_1):
                        KnockOutPhase.clear_score(self.match_id)
                    KnockOutPhase.set_winner(KnockOutPhase.get_match_team_by_id_pos(self.match_id, 1).team, self.match_id)
                    KnockOutPhase.get_match_team_by_id_pos(self.match_id, 1).score = str(in_y) + '*'
            self.focus_x = -500
            self.focus_y = -500
            self.update(-200, -200)
            self.button.pressed_function(self.button)
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color

