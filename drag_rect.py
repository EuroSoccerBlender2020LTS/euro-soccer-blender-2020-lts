import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
import bpy

class DragRect(Rect):

    def __init__(self, x, y, width, height, team):
        self.x = x
        self.y = y
        self.__textpos = [x, y]
        self.text_x = x
        self.text_y = y
        self.width = width
        self.height = height
        self.team = team
        self.font_size = self.team.group.team_font_size
        self.drag_offset_x = 0
        self.drag_offset_y = 0
        self.limit_low_x = self.team.group.drag_rect_limit_low_x
        self.limit_low_y = self.team.group.drag_rect_limit_low_y
        self.limit_high_x = self.team.group.drag_rect_limit_high_x
        self.limit_high_y = self.team.group.drag_rect_limit_high_y
        self.is_drag = False
        self.enabled = True
        self.already_dragging = False
        self.set_color((0.867, 0.325, 0.0, 1.0))
        self.text_color = (0.933, 0.933, 0.933, 1.0)
        self.text = 'None'
        if 'None.png' not in bpy.data.images:
            self.image = bpy.data.images.load(self.team.group.dirpath + '/flags/None.png')
        self.image = bpy.data.images['None.png']
        if self.image.gl_load():
            raise Exception()
        self.update(self.x, self.y)
        self.neighbours = []

    def load_img(self, filepath):
        if str(filepath).split('/')[-1] not in bpy.data.images:
            self.image = bpy.data.images.load(filepath)
        self.image = bpy.data.images[str(filepath).split('/')[-1]]
        if self.image.gl_load():
            raise Exception()

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        bgl.glActiveTexture(bgl.GL_TEXTURE0)
        bgl.glBindTexture(bgl.GL_TEXTURE_2D, self.image.bindcode)
        self.img_shader.bind()
        self.img_shader.uniform_int("image", 0)
        self.batch_img.draw(self.img_shader)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, 0.0, 0.0, 0.0, 0.5)
        blf.shadow_offset(font_id, int(self.font_size / 15), -int(self.font_size/15))
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], 1.0)
        if self.text != 'North Macedonia':
            blf.position(font_id, self.x + self.text_x, self.y + self.text_y, 0)
            blf.size(font_id, self.font_size, 72)
            blf.draw(font_id, self.text)
        else:
            blf.position(font_id, self.x + self.text_x - 5, self.y + self.text_y, 0)
            blf.size(font_id, int(self.font_size * 0.93), 72)
            blf.draw(font_id, self.text)
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x - self.drag_offset_x
        self.y = y - self.drag_offset_y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        border_img = 0
        vertices_img = (
            (self.x + border_img, self.y + border_img),
            (self.x + border_img, self.y + self.height - border_img),
            (self.x + self.height - border_img, self.y + self.height - border_img),
            (self.x + self.height - border_img, self.y + border_img))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)
        self.img_shader = gpu.shader.from_builtin('2D_IMAGE')
        self.batch_img = batch_for_shader(self.img_shader, 'TRI_FAN', {"pos": vertices_img, "texCoord": ((0, 0), (0, 1), (1, 1), (1, 0)), }, )

    def get_drag_place(self):
        return min(max(int((self.limit_high_y - self.y - 0.75 * self.team.group.group_pos_dict['cell_width']) / (
                    1.5 * self.team.group.group_pos_dict['cell_width'])), 0), 3)

    def set_place_position(self, place):
        self.team.place = place
        cell = self.team.group.group_pos_dict['cell_width']
        x = self.team.group.drag_rect_limit_low_x + self.team.drag_rect_x_spacing * cell
        y = self.team.group.drag_rect_limit_low_y + cell * (self.team.drag_rect_y_spacing + 4.5 - 1.5 * place)
        self.update(x, y)

    def left_mouse_down(self, x, y):
        if self.is_in_rect(x, y) and not self.team.group.popup_activated:
            self.is_drag = True
            self.drag_offset_x = x - self.x
            self.drag_offset_y = y - self.y
            self.color = self.pressed_color
            for drag_rect in self.neighbours:
                drag_rect.already_dragging = True
            return True
        return False

    def mouse_move(self, x, y):
        if self.is_drag:
            xx = min(max(x, self.limit_low_x + self.drag_offset_x), (self.limit_high_x + self.drag_offset_x - self.width))
            yy = min(max(y, self.limit_low_y + self.drag_offset_y), (self.limit_high_y + self.drag_offset_y - self.height))
            self.update(xx, yy)
            drag_place = self.get_drag_place()
            self.team.group.recalc_places_of_drag_teams(self, drag_place)
        else:
            if not self.team.group.popup_activated and not self.already_dragging and self.is_in_rect(x, y):
                self.color = self.hover_on_color
            else:
                self.color = self.default_color


    def left_mouse_up(self, x, y):
        self.drag_offset_x = 0
        self.drag_offset_y = 0
        # ==============
        if self.is_drag:
            drag_place = self.get_drag_place()
            self.set_place_position(drag_place)
            self.is_drag = False
            self.team.group.recalc_places_of_drag_teams(self, drag_place)
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color
        # ===============
        self.is_drag = False
        for drag_rect in self.neighbours:
            drag_rect.already_dragging = False

