bl_info = {
    "name": "Euro Soccer Blender 2020 LTS",
    "author": "First And Last Names",
    "version": (1, 0),
    "blender": (2, 90, 0),
    "category": "3D View",
    "location": "View3D > SideBar",
    "description": "Test GPU module in 3D View",
    "warning": "",
    "doc_url": "",
}

import bpy
import os
from .esb_op import ESB_OT_draw_operator
from .panels import ESBPanel

wm = bpy.types.WindowManager
wm.ESB_started = bpy.props.BoolProperty(default=False)
wm.ESB_dirpath = bpy.props.StringProperty(name='dirpath', default=os.path.dirname(os.path.realpath(__file__)))

addon_keymaps = []

blender_classes = [ESBPanel,
                   ESB_OT_draw_operator,
                   ]

def register():
    for blender_class in blender_classes:
        bpy.utils.register_class(blender_class)
    kcfg = bpy.context.window_manager.keyconfigs.addon
    if kcfg:
        km_v = kcfg.keymaps.new(name='3D View', space_type='VIEW_3D')
        kmi_v = km_v.keymap_items.new("object.esb_ot_draw_operator", 'F', 'PRESS', shift=True, ctrl=True)
        addon_keymaps.append((km_v, kmi_v))


def unregister():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    for blender_class in blender_classes:
        bpy.utils.unregister_class(blender_class)


if __name__ == "__main__":
    register()