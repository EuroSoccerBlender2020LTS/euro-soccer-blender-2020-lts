import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
from .button_rect import *
from .calc_dimencsions import *


class GroupGenerateButton(ButtonRect):

    def __init__(self, group):
        self.group_dimensions_dict = calc_groups_positions()
        self.group = group
        self.cell =  self.group_dimensions_dict['cell_width']
        self.font_size, dimension = calc_font_size('Generpte', self.cell * 7, self.cell * 0.6, 5, 'XY')
        self.width = dimension[0] + self.cell * 0.2
        self.height = dimension[1] + self.cell * 0.2
        self.x = self.group.x + self.group.cell * 13.4 - self.width - self.cell * 0.2
        self.y = self.group.y + self.group.cell * 6 + self.cell * 0.2
        self.text = 'Generate'
        coef = 0.4
        self.text_d_x = (self.width - old_calc_text_dimension(self.text, self.font_size)[0]) / 2
        self.text_d_y = coef * (self.height - old_calc_text_dimension('TPO', self.font_size)[1]) + (1 - coef) * (
                old_calc_text_dimension('qpg', self.font_size)[1] - old_calc_text_dimension('rnv', self.font_size)[1])
        self.group_target = 'NOT IN'
        self.group_position = -1
        self.pressed = False
        self.enabled = True
        self.activated = False
        self.inactive = False
        self.set_color([0.703, 0.703, 0.703, 1.0])
        self.text_color = (0.867, 0.325, 0.0, 1.0)
        self.update(self.x, self.y)
        self.neighbours = []






