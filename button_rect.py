import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
from .calc_dimencsions import *


class ButtonRect(Rect):

    def __init__(self, x, y, width, height, text, font_size):
        self.x = x
        self.y = y
        self.text = text
        self.font_size = font_size
        self.text_x = x + (width- calc_text_dimension(text, font_size)[0])/2
        self.text_y = y + (height - calc_text_dimension(text, font_size)[1]) * 0.2
        self.width = width
        self.height = height
        coef = 0.4
        self.text_d_x = (self.width - old_calc_text_dimension(self.text, self.font_size)[0]) / 2
        self.text_d_y = coef * (self.height - old_calc_text_dimension('TPO', self.font_size)[1]) + (1 - coef) * (
                    old_calc_text_dimension('qpg', self.font_size)[1] - old_calc_text_dimension('rnv', self.font_size)[1])
        self.enabled = True
        self.pressed = False
        self.activated = False
        self.set_color([0.867, 0.325, 0.0, 1.0])
        self.text_color = (0.933, 0.933, 0.933, 1.0)
        self.update(self.x, self.y)
        self.neighbours = []
        self.inactive = False

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, 0.0, 0.0, 0.0, 0.5)
        blf.shadow_offset(font_id, int(self.font_size / 10), -int(self.font_size/10))
        blf.position(font_id, self.x + self.text_d_x, self.y + self.text_d_y, 0)
        blf.size(font_id, self.font_size, 72)
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], 1.0)
        blf.draw(font_id, self.text)
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def set_pressed_function(self, some_function):
        self.pressed_function = some_function

    def left_mouse_down(self, x, y):
        if not self.inactive and self.enabled and self.is_in_rect(x, y):
            self.pressed = True
            self.color = self.pressed_color
            try:
                self.pressed_function(self)
            except:
                print("No Function for button")
            return True
        return False

    def mouse_move(self, x, y):
        if self.inactive:
            return
        if not self.pressed and self.enabled and self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color


    def left_mouse_up(self, x, y):
        self.pressed = False
        if not self.inactive:
            if self.is_in_rect(x, y):
                self.color = self.hover_on_color
            else:
                self.color = self.default_color
