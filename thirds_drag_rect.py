import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .rect import Rect
import bpy
from .calc_dimencsions import *

class ThirdsDragRect(Rect):

    def __init__(self, place, name_group_team, knockout_phase):
        self.knockout_phase = knockout_phase
        self.dimensions_dict = calc_knockout_positions()
        self.images = {}
        self.load_all_images()
        self.spacing = 0.1
        self.cell = self.dimensions_dict['thirds_height']
        self.width = self.dimensions_dict['thirds_width'] - 2 * self.spacing * self.cell
        self.height = self.cell - 2 * self.spacing * self.cell
        self.team_name, self.group_name, self.team = name_group_team
        self.set_text()
        self.font_size = self.dimensions_dict['thirds_font_size']
        self.font_dimension = self.dimensions_dict['thirds_font_dimension']
        self.text_x = self.height + (self.width - self.height - self.font_dimension[0]) * 0.5
        self.text_y = (self.height - self.font_dimension[1]) * 0.5
        self.drag_offset_x = 0
        self.drag_offset_y = 0
        self.limit_low_x = self.dimensions_dict['limit_low_x']
        self.limit_low_y = self.dimensions_dict['limit_low_y']
        self.limit_high_x = self.dimensions_dict['limit_high_x']
        self.limit_high_y = self.dimensions_dict['limit_high_y']
        self.place = place
        self.bckp_place = place
        self.x = self.limit_low_x + self.spacing * self.cell
        self.y = self.limit_low_y + self.cell * (self.spacing + 5 - self.place)
        if self.place < 4:
            self.y += self.dimensions_dict['line_width'] * 2
        self.is_drag = False
        self.enabled = True
        self.already_dragging = False
        self.set_color((0.135, 0.248, 0.16, 1.0))
        self.text_color = (0.855, 0.855, 0.855, 1.0)
        self.update(self.x, self.y)
        self.neighbours = []

    def set_text(self):
        if self.team_name == 'None':
            self.text = 'None'
        else:
            self.text = self.team.short_name + ' ' + self.group_name[-1]

    def load_img(self, filepath):
        if str(filepath).split('/')[-1] not in bpy.data.images:
            image = bpy.data.images.load(filepath)
        else:
            image = bpy.data.images[str(filepath).split('/')[-1]]
        if image.gl_load():
            raise Exception()
        return image

    def load_all_images(self):
        all_team_names = list(self.dimensions_dict['EQ_ranking'].keys())
        all_team_names.append('None')
        for name in all_team_names:
            filepath = self.knockout_phase.__class__.dirpath + '/flags/' + name.replace(' ', '_') + '.png'
            self.images[name] = self.load_img(filepath)

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader.bind()
        self.shader.uniform_float("color", self.color)
        self.batch_panel.draw(self.shader)
        image = self.images[self.team_name]
        bgl.glActiveTexture(bgl.GL_TEXTURE0)
        bgl.glBindTexture(bgl.GL_TEXTURE_2D, image.bindcode)
        self.img_shader.bind()
        self.img_shader.uniform_int("image", 0)
        self.batch_img.draw(self.img_shader)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, 0.0, 0.0, 0.0, 0.5)
        blf.shadow_offset(font_id, int(self.font_size / 15), -int(self.font_size/15))
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], 1.0)
        if self.text != 'North Macedonia':
            blf.position(font_id, self.x + self.text_x, self.y + self.text_y, 0)
            blf.size(font_id, self.font_size, 72)
            blf.draw(font_id, self.text)
        else:
            blf.position(font_id, self.x + self.text_x - 5, self.y + self.text_y, 0)
            blf.size(font_id, self.font_size - 2, 72)
            blf.draw(font_id, self.text)
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x - self.drag_offset_x
        self.y = y - self.drag_offset_y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        border_img = 0
        vertices_img = (
            (self.x + border_img, self.y + border_img),
            (self.x + border_img, self.y + self.height - border_img),
            (self.x + self.height - border_img, self.y + self.height - border_img),
            (self.x + self.height - border_img, self.y + border_img))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)
        self.img_shader = gpu.shader.from_builtin('2D_IMAGE')
        self.batch_img = batch_for_shader(self.img_shader, 'TRI_FAN', {"pos": vertices_img, "texCoord": ((0, 0), (0, 1), (1, 1), (1, 0)), }, )

    def get_drag_place(self):
        return min(max(int((self.limit_high_y - self.y - self.cell * 0.5) / self.cell), 0), 5)

    def set_place_position(self, place):
        self.place = place
        x = self.limit_low_x + self.spacing * self.cell
        y = self.limit_low_y + self.cell * (self.spacing + 5 - place)
        if self.place < 4:
            y += self.dimensions_dict['line_width'] * 2
        self.update(x, y)

    def left_mouse_down(self, x, y):
        if self.is_in_rect(x, y) and not self.knockout_phase.popup_activated:
            self.is_drag = True
            self.drag_offset_x = x - self.x
            self.drag_offset_y = y - self.y
            self.color = self.pressed_color
            for drag_rect in self.neighbours:
                drag_rect.already_dragging = True
            return True
        return False

    def mouse_move(self, x, y):
        if self.is_drag:
            xx = min(max(x, self.limit_low_x + self.drag_offset_x), (self.limit_high_x + self.drag_offset_x - self.width))
            yy = min(max(y, self.limit_low_y + self.drag_offset_y), (self.limit_high_y + self.drag_offset_y - self.height))
            drag_place = self.get_drag_place()
            self.knockout_phase.recalc_places_of_draging_thirds(self, drag_place)
            self.update(xx, yy)
        else:
            if not self.already_dragging and self.is_in_rect(x, y) and not self.knockout_phase.popup_activated:
                self.color = self.hover_on_color
            else:
                self.color = self.default_color

    def left_mouse_up(self, x, y):
        self.drag_offset_x = 0
        self.drag_offset_y = 0
        # ==============
        if self.is_drag:
            drag_place = self.get_drag_place()
            self.set_place_position(drag_place)
            self.is_drag = False
            self.knockout_phase.recalc_places_of_draging_thirds(self, drag_place)
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color
        # ===============
        self.is_drag = False
        for drag_rect in self.neighbours:
            drag_rect.already_dragging = False

