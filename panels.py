import bpy

class ESBPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_idname = "VIEW3D_PT_ESB_panel"
    bl_label = "Euro Soccer Blender"
    bl_category = "ESB"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout
        layout.label(text="Just Press This Big Button:")
        row = layout.row()
        row.scale_y = 3.0
        row.operator("object.esb_ot_draw_operator")

