import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import bpy


class GroupRect:

    def __init__(self, group, x, y, width, height):
        self.group = group
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.team_names = self.group.team_names
        self.team_width_rate = 10
        self.cell_width = self.group.cell
        self.set_color((0.833, 0.833, 0.833, 1.0))
        self.images = {}
        self.load_group_images()
        self.activated = False
        self.enabled = True
        self.update(self.x, self.y)

    def set_color(self, color):
        self.lines_color = (0.44, 0.44, 0.44, 1.0)
        self.thin_lines_color = (0.34, 0.34, 0.34, 1.0)
        self.color = color
        self.default_color = color
        self.hover_on_color = [c * 0.6 for c in self.default_color]
        self.hover_on_color[1] += 0.4 * self.hover_on_color[1] / 0.6
        self.hover_on_color[3] += 0.4 * self.hover_on_color[3] / 0.6
        self.pressed_color = self.hover_on_color[:]
        self.pressed_color[3] *= 0.3

    def load_img(self, filepath):
        if str(filepath).split('/')[-1] not in bpy.data.images:
            self.image = bpy.data.images.load(filepath)
        image = bpy.data.images[str(filepath).split('/')[-1]]
        if image.gl_load():
            raise Exception()
        return image

    def load_group_images(self):
        for name in self.group.team_names:
            filepath = self.group.__class__.dirpath + '/flags/' + name.replace(' ', '_') + '.png'
            self.images[name] = self.load_img(filepath)

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        self.shader_rect.bind()
        self.shader_rect.uniform_float("color", self.color)
        self.batch_rect.draw(self.shader_rect)
        bgl.glLineWidth(1)
        self.shader_rect.uniform_float("color", self.thin_lines_color)
        self.batch_thin_lines.draw(self.shader_rect)
        bgl.glLineWidth(3)
        self.shader_rect.uniform_float("color", self.lines_color)
        self.batch_lines.draw(self.shader_rect)
        for image, batch in self.batches_images:
            bgl.glActiveTexture(bgl.GL_TEXTURE0)
            bgl.glBindTexture(bgl.GL_TEXTURE_2D, image.bindcode)
            self.shader_images.bind()
            self.shader_images.uniform_int("image", 0)
            batch.draw(self.shader_images)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        line_vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height - 1.5 * self.cell_width),
            (self.x + self.width - 3 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + self.width - 3 * self.cell_width, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y),
            (self.x + 6.4 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y),
        )
        thin_line_vertices = (
            (self.x + 2.4 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + 2.4 * self.cell_width, self.y),
            (self.x + 4 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + 4 * self.cell_width, self.y),
            # (-1, -1), (-2, -2), (-3, -3), (-4, -4), # replace first two vertical lines in group
            (self.x + self.width - 3 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + self.width - 3 * self.cell_width, self.y),
            (self.x + self.width - 2 * self.cell_width, self.y + self.height ),
            (self.x + self.width - 2 * self.cell_width, self.y),
            (self.x + self.width - self.cell_width, self.y + self.height),
            (self.x + self.width - self.cell_width, self.y),
            (self.x + self.width - 3 * self.cell_width, self.y + self.height - 1.5 * self.cell_width),
            (self.x + self.width, self.y + self.height - 1.5 * self.cell_width),
            (self.x, self.y + self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + self.cell_width),
            (self.x, self.y + 2 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 2 * self.cell_width),
            (self.x, self.y + 3 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 3 * self.cell_width),
            (self.x, self.y + 4 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 4 * self.cell_width),
            (self.x, self.y + 5 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 5 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 1.5 * self.cell_width),
            (self.x + self.width, self.y + 1.5 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 3 * self.cell_width),
            (self.x + self.width, self.y + 3 * self.cell_width),
            (self.x + 6.4 * self.cell_width, self.y + 4.5 * self.cell_width),
            (self.x + self.width, self.y + 4.5 * self.cell_width),
        )
        line_indices = (
            (0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 0), (6, 7)
        )
        thin_line_indices = (
            (0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11), (12, 13), (14, 15), (16, 17), (18, 19), (20, 21), (22, 23),
            (24, 25), (26, 27),
        )
        line_color = [self.lines_color for i in range(len(line_vertices))]
        thin_line_color = [self.hover_on_color for i in range(len(line_vertices))]
        self.shader_rect = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.shader_images = gpu.shader.from_builtin("2D_IMAGE")
        self.batches_images = []
        for i, match in enumerate(self.group.match_scores):
            x_spacing = 1.4 * self.cell_width
            x_second_column = 4 * self.cell_width
            vertices_img = ((self.x + x_spacing, self.y + (5 - i) * self.cell_width),
                            (self.x + x_spacing, self.y + (6 - i) * self.cell_width),
                            (self.x + self.cell_width + x_spacing, self.y + (6 - i) * self.cell_width),
                            (self.x + self.cell_width + x_spacing, self.y + (5 - i) * self.cell_width),
                            )
            self.batches_images.append((self.images[self.team_names[match[1][0]]],
                                        batch_for_shader(self.shader_images, "TRI_FAN", {"pos": vertices_img,
                                                                                         "texCoord": (
                                                                                         (0, 0), (0, 1), (1, 1),
                                                                                         (1, 0)), })))
            vertices_img = ((self.x + x_second_column, self.y + (5 - i) * self.cell_width),
                            (self.x + x_second_column, self.y + (6 - i) * self.cell_width),
                            (self.x + x_second_column + self.cell_width, self.y + (6 - i) * self.cell_width),
                            (self.x + x_second_column + self.cell_width, self.y + (5 - i) * self.cell_width),
                            )
            self.batches_images.append((self.images[self.team_names[match[1][1]]],
                                        batch_for_shader(self.shader_images, "TRI_FAN", {"pos": vertices_img,
                                                                                         "texCoord": (
                                                                                         (0, 0), (0, 1), (1, 1),
                                                                                         (1, 0)), })))
        self.batch_rect = batch_for_shader(self.shader_rect, 'TRIS', {"pos": vertices}, indices=indices)
        self.batch_lines = batch_for_shader(self.shader_rect, 'LINES', {"pos": line_vertices }, indices=line_indices)
        self.batch_thin_lines = batch_for_shader(self.shader_rect, 'LINES', {"pos": thin_line_vertices}, indices=thin_line_indices)

    def handle_event(self, event):
        if (event.type == 'LEFTMOUSE'):
            if (event.value == "PRESS"):
                return self.left_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.left_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

    def is_in_rect(self, x, y):
        return (self.x <= x <= (self.x + self.width)) and (self.y <= y <= (self.y + self.height))

    def left_mouse_down(self, x, y):
        return True

    def left_mouse_up(self, x, y):
        pass

    def right_mouse_down(self, x, y):
        return True

    def right_mouse_up(self, x, y):
        pass

    def mouse_move(self, x, y):
        pass
