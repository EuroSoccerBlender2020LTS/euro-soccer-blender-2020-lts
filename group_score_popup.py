import gpu
import bgl
from gpu_extras.batch import batch_for_shader
import blf
from .group_phase import Group
from .calc_dimencsions import *


class PopUpRect:

    def __init__(self, x, y, width, height, button):
        self.button = button
        self.group_dimensions_dict = calc_groups_positions()
        self.x = -500
        self.y = -500
        self.__textpos = [x, y]
        self.text = self.group_dimensions_dict['popup_text']
        coef1 = 0.8
        self.font_size = int(self.group_dimensions_dict['score_font_size'] * coef1)
        self.font_dimension = old_calc_text_dimension(' 0:0 ', self.font_size)
        self.width = old_calc_text_dimension(self.text[0], self.font_size)[0]
        self.d_x = 0
        coef2 = 1.8
        self.height = self.font_dimension[1] * 6 * coef2
        self.focus_x = -100
        self.focus_y = -100
        self.focus_width = self.font_dimension[0]
        self.focus_height = self.height / 6
        self.activated = False
        self.enabled = True
        self.deactivating = False
        self.set_color([0.976, 0.553, 0.39, 1.0])
        self.text_color = [1.0, 1.0, 1.0, 1.0]
        self.match_position = -1
        self.group_name = 'NOT IN'
        self.update(self.x, self.y)
        self.update_focus()
        self.neighbours = []
        self.up_down = -1

    def set_alpha(self, alpha):
        self.set_color([self.default_color[0], self.default_color[1], self.default_color[2], alpha])
        self.text_color[3] = alpha

    def set_color(self, color):
        self.color = color
        self.default_color = color
        self.hover_on_color = [0.867, 0.325, 0.0, 1.0]
        self.pressed_color = self.hover_on_color[:]

    def draw(self):
        bgl.glEnable(bgl.GL_BLEND)
        color = self.color
        self.shader.bind()
        self.shader.uniform_float("color", color)
        self.batch_panel.draw(self.shader)
        self.shader_focus.bind()
        self.shader_focus.uniform_float("color", (1.0, 1.0, 1.0, 1.0))
        self.batch_focus.draw(self.shader_focus)
        font_id = 0
        blf.enable(font_id, blf.SHADOW)
        blf.shadow(font_id, 0, 0.0, 0.0, 0.0, self.text_color[3] * 0.5)
        blf.shadow_offset(font_id, int(self.font_size / 10), -int(self.font_size/10))
        blf.size(font_id, self.font_size,  72)
        blf.color(font_id, self.text_color[0], self.text_color[1], self.text_color[2], self.text_color[3])
        up_dowm = 0
        if self.up_down == -1:
            up_dowm = 5
        for i in range(6):
            blf.position(font_id, self.x + self.d_x, self.y + self.focus_height * (up_dowm + i * self.up_down) + (self.focus_height - self.font_dimension[1])/2, 0)
            blf.draw(font_id, self.text[i])
        blf.disable(font_id, blf.SHADOW)
        bgl.glDisable(bgl.GL_BLEND)

    def update(self, x, y):
        indices = ((0, 1, 2), (0, 2, 3))
        self.x = x
        self.y = y
        self.__textpos[0] = self.x
        self.__textpos[1] = self.y
        vertices = (
            (self.x, self.y),
            (self.x, self.y + self.height),
            (self.x + self.width, self.y + self.height),
            (self.x + self.width, self.y))
        self.shader = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_panel = batch_for_shader(self.shader, 'TRIS', {"pos": vertices}, indices=indices)

    def update_focus(self):
        indices = ((0, 1, 2), (0, 2, 3))
        focus_vertices = (
            (self.focus_x, self.focus_y),
            (self.focus_x, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y + self.focus_height),
            (self.focus_x + self.focus_width, self.focus_y))
        self.shader_focus = gpu.shader.from_builtin("2D_UNIFORM_COLOR")
        self.batch_focus = batch_for_shader(self.shader_focus, 'TRIS', {"pos": focus_vertices}, indices=indices)

    def handle_event(self, event):
        if (event.type == 'LEFTMOUSE'):
            if (event.value == "PRESS"):
                return self.left_mouse_down(event.mouse_region_x, event.mouse_region_y)
            else:
                self.left_mouse_up(event.mouse_region_x, event.mouse_region_y)
        elif (event.type == "MOUSEMOVE"):
            self.mouse_move(event.mouse_region_x, event.mouse_region_y)
            return True
        return False

    def is_in_rect(self, x, y):
        return (self.x <= x <= (self.x + self.width)) and (self.y <= y <= (self.y + self.height))

    def left_mouse_down(self, x, y):
        if self.is_in_rect(x, y) and self.activated:
            self.color = self.pressed_color
            self.deactivating = True
            return True
        return False

    def mouse_move(self, x, y):
        if self.is_in_rect(x, y):
            self.index_x = int((x - self.x) / self.focus_width)
            self.index_y = int((y - self.y) / self.focus_height)
            self.focus_x = self.x + self.focus_width * self.index_x
            self.focus_y = self.y + self.focus_height * self.index_y
            self.color = self.hover_on_color
        else:
            self.focus_x = -100
            self.focus_y = -100
            self.color = self.default_color
        self.update_focus()

    def activate(self):
        self.activated = True
        self.set_alpha(1.0)

    def deactivate(self):
        self.activated = False
        self.deactivating = False
        self.set_alpha(0.0)
        self.group_name = 'NOT IN'
        self.match_position = -1

    def left_mouse_up(self, x, y):
        if self.deactivating:
            Group.popup_activated = False
            if self.up_down == -1:
                in_x = 5 - self.index_y
            else:
                in_x = self.index_y
            in_y = self.index_x
            Group.all_groups[self.group_name].match_scores[self.match_position][2][0] = in_x
            Group.all_groups[self.group_name].match_scores[self.match_position][2][1] = in_y
            Group.all_groups[self.group_name].calc_table()
            Group.all_groups[self.group_name].zero_fill_penalties()
            Group.all_groups[self.group_name].calc_table()
            Group.all_groups[self.group_name].real_sort()
            self.button.pressed_function(self.button)
            self.focus_x = -500
            self.focus_y = -500
            self.update(-200, -200)
            self.update_focus()
            self.deactivate()
        if self.is_in_rect(x, y):
            self.color = self.hover_on_color
        else:
            self.color = self.default_color

