This is Blender add-on.

Installation instructions:
 1. Download [EuroSoccerBlender2020LTS.zip](https://gitlab.com/EuroSoccerBlender2020LTS/euro-soccer-blender-2020-lts/-/raw/master/EuroSoccerBlender2020LTS.zip?inline=false).
 2. Then open Blender.
 3. Open _Edit>Preferences>Add-ons_.
 4. Click install button, find and choose downloaded _EuroSoccerBlender2020LTS.zip_.

Usage:
 1. Click on score to choose yours.
 2. Click **"Generate"** to generate all match scores in group.
 3. Click **"Generate Groups Results"** to generate all match scores in all groups.
 4. Drag-n-drop team to place you choose.
 5. Click **"KnockOut Phase"** to go to Knock-Out phase.
 6. Click **"Group Phase"** to go to Group phase.
 7. Click on score to choose yours.
 8. Click **"Generate KnockOut Results"** button to generate all match scores in Knock-Out phase.
 9. Click **"Generate Groups Results"** button to generate all match scores in Group phase.
 10. Drag-n-drop third placed team to choose four for Knock-Out Phase.
 11. Click on Team to choose one in list of available teams.
 12. Click RIGHT MOUSEBUTTON on team to lock(unlock) it in this match.
 13. Press **ESC** to EXIT.

Have a fun!
